<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "profile_user".
 *
 * @property integer $profile_user_id
 * @property integer $user_id
 * @property string $profile_photo
 * @property integer $profile_title
 * @property string $profile_full_name
 * @property string $profile_nickname
 * @property string $profile_mobile
 * @property string $profile_address
 * @property string $profile_nationality
 * @property integer $profile_country
 * @property string $profile_ic_no
 * @property integer $profile_race
 * @property string $created_datetime
 * @property integer $created_by
 * @property string $updated_datetime
 * @property integer $updated_by
 */
class ProfileUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profile_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'created_datetime', 'created_by', 'updated_datetime', 'updated_by'], 'required'],
            [['user_id', 'profile_title', 'profile_country', 'profile_race', 'created_by', 'updated_by'], 'integer'],
            [['profile_address'], 'string'],
            [['created_datetime', 'updated_datetime'], 'safe'],
            [['profile_photo'], 'string', 'max' => 300],
            [['profile_full_name'], 'string', 'max' => 250],
            [['profile_nickname'], 'string', 'max' => 100],
            [['profile_mobile', 'profile_ic_no'], 'string', 'max' => 20],
            [['profile_nationality'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'profile_user_id' => 'Profile User ID',
            'user_id' => 'User ID',
            'profile_photo' => 'Profile Photo',
            'profile_title' => 'Profile Title',
            'profile_full_name' => 'Profile Full Name',
            'profile_nickname' => 'Profile Nickname',
            'profile_mobile' => 'Profile Mobile',
            'profile_address' => 'Profile Address',
            'profile_nationality' => 'Profile Nationality',
            'profile_country' => 'Profile Country',
            'profile_ic_no' => 'Profile Ic No',
            'profile_race' => 'Profile Race',
            'created_datetime' => 'Created Datetime',
            'created_by' => 'Created By',
            'updated_datetime' => 'Updated Datetime',
            'updated_by' => 'Updated By',
        ];
    }
    
    public function getUser() {
        return $this->hasOne(\common\models\User::className(), ['id' => 'user_id'])->
       andWhere(['status' =>! self::STATUS_DELETED]);
    }
    
    public function getProfileTitle()
    {
        return $this->hasOne(\common\models\TitleOptions::className(), ['id' => 'profile_title']);
    }
    
    public function getProfileCountry()
    {
        return $this->hasOne(\common\models\Country::className(), ['id' => 'profile_country']);
    }
    
    public function getProfileRace()
    {
        return $this->hasOne(\common\models\Race::className(), ['id' => 'profile_race']);
    }
    
    public function getProfileRegion()
    {
        return $this->hasOne(\common\models\Region::className(), ['region_id' => 'region_id']);
    }
    
    public function getProfileState()
    {
        return $this->hasOne(\common\models\State::className(), ['state_id' => 'state_id']);
    }
}
