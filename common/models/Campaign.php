<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "campaign".
 *
 * @property integer $campaign_id
 * @property string $campaign_description
 * @property string $campaign_short_code
 * @property string $campaign_rm_per_point
 * @property string $campaign_start_date
 * @property string $campaign_end_date
 * @property string $campaign_status
 */
class Campaign extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'campaign';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['campaign_description', 'campaign_rm_per_point', 'campaign_start_date', 'campaign_end_date', 'campaign_status','campaign_short_code'], 'required'],
            [['campaign_description'], 'string', 'max' => 200],
            [['campaign_short_code'], 'string', 'max' => 10],
            [['campaign_rm_per_point'], 'number'],
            [['campaign_start_date', 'campaign_end_date'], 'safe'],
            [['campaign_status'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'campaign_id' => 'Campaign ID',
            'campaign_description' => 'Title',
            'campaign_short_code' => 'Short Code',
            'campaign_rm_per_point' => 'RM Per Point',
            'campaign_start_date' => 'Start Date',
            'campaign_end_date' => 'End Date',
            'campaign_status' => 'Status',
        ];
    }
    
    public function getCampaignStatus() {
        
        //A=Active, D=Deactive, X=Delete, E=Expiry
        $paymentDate = date('Y-m-d');
        //$paymentDate=date('Y-m-d', strtotime($paymentDate));
        //$contractDateBegin = date('Y-m-d', strtotime($this->campaign_start_date));
        //$contractDateEnd = date('Y-m-d', strtotime($this->campaign_end_date));
        $returnValue = "";
        
        //$paymentDate=strtotime($paymentDate);
        $contractDateBegin=$this->campaign_start_date;
        $contractDateEnd = $this->campaign_end_date;
        
        if ($this->campaign_status == "A") {
            $returnValue = "<span class='label label-primary'>Active</span>";
        } else if ($this->campaign_status == "D") {
            $returnValue = "<span class='label label-danger'>Deactive</span>";
        } else if ($this->campaign_status == "X") {
            $returnValue = "<span class='label label-danger'>Deleted</span>";
        }

        /*if ($paymentDate <= $contractDateEnd){
            if ($this->campaign_status == "A") {
                $returnValue = "<span class='label label-primary'>Active</span>";
            } else if ($this->campaign_status == "D") {
                $returnValue = "<span class='label label-danger'>Deactive</span>";
            } else if ($this->campaign_status == "X") {
                $returnValue = "<span class='label label-danger'>Deleted</span>";
            }
        }else{
            $returnValue = "<span class='label label-danger'>Expired</span>";
        }*/
        
        return $returnValue;
    }
}
