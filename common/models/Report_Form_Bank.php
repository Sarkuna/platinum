<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class Report_Form_Bank extends Model
{
    public $reportdate;
    public $status;
    //public $membershipno;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['reportdate','status'], 'required'],
            //[['membershipno'], 'safe']

        ];
    }
    
    public function attributeLabels()
    {
        return [
            'reportdate' => 'Redemption Payment Month',
            'status' => 'Account Status'
        ];
    }

    
}
