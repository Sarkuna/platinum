<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class Payout_Form extends Model
{
    public $date_month;
    public $description;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['date_month','description'], 'required'],
            //[['membershipno'], 'safe']

        ];
    }
    
    public function attributeLabels()
    {
        return [
            'date_month' => 'Redemption Payment Month',
            'description' => 'Description'
        ];
    }

    
}
