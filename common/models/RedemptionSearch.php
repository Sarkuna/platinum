<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Redemption;

/**
 * RedemptionSearch represents the model behind the search form about `common\models\Redemption`.
 */
class RedemptionSearch extends Redemption
{
    /**
     * @inheritdoc
     */
    public $card_id;
    public $ic_no;
    public $full_name;
    public $account_no_verification;
    public $account_number;
    public $bank_name;
    public $account_name;


    public function rules()
    {
        return [
            [['redemptionID', 'painterID', 'req_points', 'redemption_status', 'redemption_remarks', 'created_by', 'updated_by'], 'integer'],
            [['redemption_status_ray','full_name','ic_no','card_id','redemption_invoice_no', 'orderID', 'redemption_IP', 'redemption_created_datetime', 'redemption_updated_datetime', 'account_no_verification', 'account_number', 'bank_name', 'internel_status', 'account_name'], 'safe'],
            [['req_amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Redemption::find();
        $query->joinWith(['profile']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['redemption_status'=>SORT_ASC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'redemptionID' => $this->redemptionID,
            'painterID' => $this->painterID,
            'card_id' => $this->card_id,
            'ic_no' => $this->ic_no,
            //'full_name' => $this->full_name,
            'req_points' => $this->req_points,
            'req_amount' => $this->req_amount,
            'redemption_status' => $this->redemption_status,
            'redemption_status_ray' => $this->redemption_status_ray,
            'redemption_remarks' => $this->redemption_remarks,
            //'redemption_created_datetime' => $this->redemption_created_datetime,
            'redemption_created_by' => $this->created_by,
            'redemption_updated_datetime' => $this->redemption_updated_datetime,
            'redemption_updated_by' => $this->updated_by,
        ]);
        $query->andFilterWhere(['like', 'redemption_created_datetime', $this->redemption_created_datetime]);
        $query->andFilterWhere(['like', 'redemption_invoice_no', $this->redemption_invoice_no])
            ->andFilterWhere(['like', 'orderID', $this->orderID])
            ->andFilterWhere(['like', 'card_id', $this->card_id])
            ->andFilterWhere(['like', 'full_name', $this->full_name])      
            ->andFilterWhere(['like', 'redemption_IP', $this->redemption_IP]);
        $query->andFilterWhere(['like', 'card_id', $this->card_id]);
        $query->andFilterWhere(['IN', 'redemption_status', ['1','7','17']]);
            //->andFilterWhere(['like', 'roles', $this->roles]);
        //echo $this->redemption_created_datetime;
        //die();
        return $dataProvider;
    }
    
    public function searchray($params)
    {
        //$query = Redemption::find();
        //$query->joinWith(['profile']);
        
        $query = Redemption::find()->joinWith([
            'profile' => function($query) {
                $query->select(['painter_profile.id','painter_profile.user_id','painter_profile.card_id','painter_profile.full_name','painter_profile.ic_no','painter_profile.mobile']);
                //$query->select(['profile.id','profile.username'])->where(['user.status' => 'A']);
            },            
            'bank',
            ])
            ->select(['*'])
            /*->where(['point_order.order_status'=> 17])
            ->andWhere(['BETWEEN', 'point_order.created_datetime',
                new \yii\db\Expression('(NOW() - INTERVAL 1 MONTH)'),
                new \yii\db\Expression('NOW()')])*/
            ->limit(20);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['redemption_status_ray'=>SORT_ASC]],
            //'pagination' => ['pageSize' => 50,],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'redemptionID' => $this->redemptionID,
            'painterID' => $this->painterID,
            'card_id' => $this->card_id,
            'ic_no' => $this->ic_no,
            'req_points' => $this->req_points,
            'req_amount' => $this->req_amount,
            'redemption_status' => $this->redemption_status,
            'redemption_status_ray' => $this->redemption_status_ray,
            'redemption_remarks' => $this->redemption_remarks,
            //'redemption_created_datetime' => $this->redemption_created_datetime,
            'redemption_created_by' => $this->created_by,
            'redemption_updated_datetime' => $this->redemption_updated_datetime,
            'redemption_updated_by' => $this->updated_by,
        ]);
        $query->andFilterWhere(['like', 'redemption_created_datetime', $this->redemption_created_datetime]);
        $query->andFilterWhere(['like', 'redemption_invoice_no', $this->redemption_invoice_no])
            ->andFilterWhere(['like', 'orderID', $this->orderID])
            ->andFilterWhere(['like', 'card_id', $this->card_id]) 
            ->andFilterWhere(['like', 'full_name', $this->full_name]) 
            ->andFilterWhere(['like', 'internel_status', $this->internel_status])
            ->andFilterWhere(['like', 'redemption_IP', $this->redemption_IP]);
        $query->andFilterWhere(['like', 'card_id', $this->card_id]);
        
        $query->andFilterWhere(['like', 'banking_information.account_no_verification', $this->account_no_verification]);
        $query->andFilterWhere(['like', 'banking_information.account_number', $this->account_number]);
        $query->andFilterWhere(['like', 'banking_information.bank_name', $this->bank_name]);
        $query->andFilterWhere(['like', 'banking_information.account_name', $this->account_name]);
        
        $query->andFilterWhere(['IN', 'redemption_status_ray', ['2','8','10','19']]);
            //->andFilterWhere(['like', 'roles', $this->roles]);
        //echo $this->redemption_created_datetime;
        //die();
        return $dataProvider;
    }
}
