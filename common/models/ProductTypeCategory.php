<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product_type_category".
 *
 * @property integer $product_type_id
 * @property string $product_type_name
 * @property string $product_type_status
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property integer $created_by
 * @property integer $updated_by
 */
class ProductTypeCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_type_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_type_name'], 'required'],
            [['created_datetime', 'updated_datetime'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['product_type_name'], 'string', 'max' => 100],
            [['product_type_status'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_type_id' => 'Product Type ID',
            'product_type_name' => 'Product Type Name',
            'product_type_status' => 'Product Type Status',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
}
