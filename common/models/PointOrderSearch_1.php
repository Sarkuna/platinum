<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PointOrder;

/**
 * PointOrderSearch represents the model behind the search form about `common\models\PointOrder`.
 */
class PointOrderSearch extends PointOrder
{
    /**
     * @inheritdoc
     */
    public $card_id;
    public $full_name;
    public $customer_name;
    public $item_id;
    public function rules()
    {
        return [
            [['order_id', 'order_qty', 'order_total_point', 'created_by', 'updated_by'], 'integer'],
            [['item_id','customer_name','full_name','card_id','painter_login_id', 'order_num','painter_ic', 'order_status', 'redemption','order_remarks', 'order_IP', 'created_datetime', 'updated_datetime'], 'safe'],
            [['order_total_amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PointOrder::find();
        $query->joinWith(['dealerOutlet','profile','orderStatus']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['order_status'=>SORT_ASC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'order_id' => $this->order_id,
            'order_num' => $this->order_num,
            'order_dealer_id' => $this->order_dealer_id,
            'card_id' => $this->card_id,
            'full_name' => $this->full_name,
            //'customer_name' => $this->customer_name, 
            'order_status_id' => $this->order_status,
            'painter_login_id' => $this->painter_login_id,
            'order_qty' => $this->order_qty,
            'order_total_point' => $this->order_total_point,
            'order_total_amount' => $this->order_total_amount,
            'redemption' => $this->redemption,
            //'created_datetime' => $this->created_datetime,
            'created_by' => $this->created_by,
            'updated_datetime' => $this->updated_datetime,
            'updated_by' => $this->updated_by,
        ]);
        $query->andFilterWhere(['like', 'point_order.created_datetime', $this->created_datetime]);
        $query->andFilterWhere(['like', 'painter_ic', $this->painter_ic])
            ->andFilterWhere(['like', 'order_status_id', $this->order_status])
            ->andFilterWhere(['like', 'order_remarks', $this->order_remarks])
            ->andFilterWhere(['like', 'card_id', $this->card_id])
            ->andFilterWhere(['like', 'full_name', $this->full_name])    
            ->andFilterWhere(['like', 'customer_name', $this->customer_name])            
            ->andFilterWhere(['like', 'order_IP', $this->order_IP]);

       /* echo '<pre>';
        print_r($query);
        die();*/
        return $dataProvider;
    }
    
    public function searchtra($params)
    {
        $query = PointOrder::find();
        $query->joinWith(['dealerOutlet','profile','orderStatus','orderItems']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['order_status'=>SORT_ASC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'order_id' => $this->order_id,
            'order_num' => $this->order_num,
            'order_dealer_id' => $this->order_dealer_id,
            'card_id' => $this->card_id,
            'full_name' => $this->full_name,
            //'customer_name' => $this->customer_name, 
            'order_status_id' => $this->order_status,
            'painter_login_id' => $this->painter_login_id,
            'order_qty' => $this->order_qty,
            'order_total_point' => $this->order_total_point,
            'order_total_amount' => $this->order_total_amount,
            'redemption' => $this->redemption,
            //'created_datetime' => $this->created_datetime,
            'created_by' => $this->created_by,
            'updated_datetime' => $this->updated_datetime,
            'updated_by' => $this->updated_by,
        ]);
        $query->andFilterWhere(['like', 'point_order.created_datetime', $this->created_datetime]);
        $query->andFilterWhere(['like', 'painter_ic', $this->painter_ic])
            ->andFilterWhere(['like', 'order_status_id', $this->order_status])
            ->andFilterWhere(['like', 'order_remarks', $this->order_remarks])
            ->andFilterWhere(['like', 'card_id', $this->card_id])
            ->andFilterWhere(['like', 'full_name', $this->full_name])    
            ->andFilterWhere(['like', 'customer_name', $this->customer_name])            
            ->andFilterWhere(['like', 'order_IP', $this->order_IP]);

       /* echo '<pre>';
        print_r($query);
        die();*/
        return $dataProvider;
    }
    
    public function searchbyuser($params)
    {
        //$query = PointOrder::find()->select('painter_login_id')->distinct();
        $query = PointOrder::find()->groupBy('painter_login_id');
        $query->joinWith(['dealerOutlet','profile','orderStatus']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['order_status'=>SORT_ASC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'order_id' => $this->order_id,
            'order_num' => $this->order_num,
            'order_dealer_id' => $this->order_dealer_id,
            'card_id' => $this->card_id,
            'full_name' => $this->full_name,
            //'customer_name' => $this->customer_name, 
            'order_status_id' => $this->order_status,
            'painter_login_id' => $this->painter_login_id,
            'order_qty' => $this->order_qty,
            'order_total_point' => $this->order_total_point,
            'order_total_amount' => $this->order_total_amount,
            'redemption' => $this->redemption,
            //'created_datetime' => $this->created_datetime,
            'created_by' => $this->created_by,
            'updated_datetime' => $this->updated_datetime,
            'updated_by' => $this->updated_by,
        ]);
        $query->andFilterWhere(['like', 'point_order.created_datetime', $this->created_datetime]);
        $query->andFilterWhere(['like', 'painter_ic', $this->painter_ic])
            ->andFilterWhere(['like', 'order_status_id', $this->order_status])
            ->andFilterWhere(['like', 'order_remarks', $this->order_remarks])
            ->andFilterWhere(['like', 'card_id', $this->card_id])
            ->andFilterWhere(['like', 'full_name', $this->full_name])    
            ->andFilterWhere(['like', 'customer_name', $this->customer_name])            
            ->andFilterWhere(['like', 'order_IP', $this->order_IP]);

       /* echo '<pre>';
        print_r($query);
        die();*/
        return $dataProvider;
    }
    
}
