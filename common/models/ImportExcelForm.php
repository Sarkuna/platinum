<?php
namespace common\models;

use yii\base\Model;
use yii\web\UploadedFile;
use Yii;


class ImportExcelForm extends Model
{
    public $excel;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['excel', 'required'],
            //['excel', 'file'],
            [['excel'], 'file', 'extensions'=>['xls', 'xlsx'], 'checkExtensionByMimeType'=>false],
            //[['csv'], 'file', 'skipOnEmpty' => false, 'extensions'=>['xls', 'csv'], 'checkExtensionByMimeType'=>false, 'maxSize'=>1024 * 1024 * 2],
        ];
    }

    
}