<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "redemption_history".
 *
 * @property integer $redemption_history_id
 * @property integer $redemptionID
 * @property integer $redemption_status
 * @property integer $notify
 * @property string $comment
 * @property string $date_added
 */
class RedemptionHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'redemption_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['redemptionID', 'redemption_status', 'date_added'], 'required'],
            [['redemptionID', 'redemption_status', 'notify'], 'integer'],
            [['comment'], 'string'],
            [['date_added'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'redemption_history_id' => 'Redemption History ID',
            'redemptionID' => 'Redemption ID',
            'redemption_status' => 'Redemption Status',
            'notify' => 'Notify',
            'comment' => 'Comment',
            'date_added' => 'Date Added',
        ];
    }
}
