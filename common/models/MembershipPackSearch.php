<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MembershipPack;
use common\models\User;

/**
 * MembershipPackSearch represents the model behind the search form about `common\models\MembershipPack`.
 */
class MembershipPackSearch extends MembershipPack
{
    public $user;
    public $mpack;
    public $userPainter;
    public $full_name;
    public $ic_no;
    public $email;
    public $mobile;
    public $del;
    public $status;
    //public $mp_updated_datetime;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'created_by', 'updated_by'], 'integer'],
            [['del','mobile','email','ic_no','userPainter','remark', 'pack_status', 'IP', 'created_datetime', 'mp_updated_datetime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MembershipPack::find();
       // $query->joinWith(['user']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'created_datetime' => $this->created_datetime,
            'created_by' => $this->created_by,
            'mp_updated_datetime' => $this->updated_datetime,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'remark', $this->remark])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'IP', $this->IP]);

        if ($this->user != null) {
            $query->andFilterWhere(['like', 'user.id', $this->user]);
        }
        return $dataProvider;
    }
    
    public function searchuser($params)
    {
        $query = User::find();
        $query->joinWith(['userPainter','membershipPack']);
        //$query->joinWith(['user']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'created_datetime' => $this->created_datetime,
            'mp_updated_datetime' => $this->mp_updated_datetime,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'user_type' => 'P',
            'full_name' => $this->userPainter,
            //'ic_no' => $this->ic_no,
        ]);

        
        $query->andWhere(['=','status', 'A']);
        $query->andWhere(['!=','del', 'X']);
        

        $vmp_updated_datetime = date('Y-m-d H:i:s', strtotime($this->mp_updated_datetime));
        $query->andFilterWhere(['like', 'remark', $this->remark])
                ->andFilterWhere(['like', 'pack_status', $this->pack_status])
                ->andFilterWhere(['like', 'IP', $this->IP]);
        
        $query->andFilterWhere(['like', 'full_name', $this->userPainter])
                ->andFilterWhere(['like', 'ic_no', $this->ic_no])
                ->andFilterWhere(['like', 'mp_updated_datetime', $this->mp_updated_datetime])
                ->andFilterWhere(['like', 'email', $this->email])
                ->andFilterWhere(['like', 'mobile', $this->mobile]);

        //echo date('Y-m-d H:i:s', strtotime($this->mp_updated_datetime));
        //die();
        return $dataProvider;
    }
}
