<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * This is the model class for table "point_order".
 *
 * @property integer $order_id
 * @property integer $order_dealer_id
 * @property integer $painter_login_id
 * @property string $painter_ic
 * @property integer $order_qty
 * @property integer $order_total_point
 * @property string $order_total_amount
 * @property integer $order_status
 * @property string $order_remarks
 * @property string $order_IP
 * @property string $created_datetime
 * @property integer $created_by
 * @property string $updated_datetime
 * @property integer $updated_by
 */
class PointOrder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $d_area;
    public $product_name;
    public $product_description;
    public $vqty;
    public $total_items;
    public $listing_campaign;
    public $brand;
    
    public static function tableName()
    {
        return 'point_order';
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_dealer_id', 'dealer_invoice_price', 'dealer_invoice_date', 'pay_out_percentage'], 'required'],
            //[['order_dealer_id',], 'required'],
            ['dealer_invoice_no', 'required'],
            //['dealer_invoice_no', 'unique'],
            ['dealer_invoice_no', 'unique', 'targetAttribute' => ['dealer_invoice_no', 'order_dealer_id'], 'message' => 'This {attribute} has already been taken.'],
            [['order_dealer_id', 'painter_login_id', 'order_qty', 'order_total_point', 'order_total_point_mines','created_by', 'updated_by', 'order_status'], 'integer'],
            [['order_total_amount','order_total_amount_mines'], 'number'],
            [['order_remarks'], 'string'],
            [['total_items','order_num','created_datetime', 'updated_datetime','dealer_invoice_no', 'dealer_invoice_price', 'd_area','product_name','product_description','vqty','brand'], 'safe'],
            [['painter_ic'], 'string', 'max' => 20],
            //[['order_status'], 'string', 'max' => 2],
            [['order_IP'], 'string', 'max' => 30],
            //[['dealer_invoice_no'], 'unique', 'attribute' => ['dealer_invoice_no', 'order_dealer_id']],

            
            //['dealer_invoice_no', 'message' => 'This {attribute} has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id' => 'Order ID',
            'order_num' => 'Order#',
            'total_items' => 'Total Items',
            'brand' => 'Brand',
            'd_area' => 'Area',
            'order_dealer_id' => 'Order Dealer ID',
            'dealer_invoice_no' => 'Dealer Invoice No.',
            'dealer_invoice_date' => 'Dealer Invoice Date',
            'campaign_status' => 'Is this part of a Campaign?',
            'dealer_invoice_price' => 'Dealer Invoice (RM)',
            'product_name' => 'Product Name',
            'product_description' => 'Product Description',
            'pay_out_percentage' => 'RM pay-out %',
            'vqty' => 'Qty',
            'painter_login_id' => 'Painter Login ID',
            'painter_ic' => 'Painter Ic',
            'order_qty' => 'Order Qty',
            'order_total_point' => 'Order Total Point',
            'order_total_amount' => 'Order Total Amount',
            'order_status' => 'Order Status',
            'order_remarks' => 'Order Remarks',
            'order_IP' => 'Order  Ip',
            'created_datetime' => 'Created Datetime',
            'created_by' => 'Created By',
            'updated_datetime' => 'Updated Datetime',
            'updated_by' => 'Updated By',
        ];
    }
    
    public function getTotalItems(){
        //return Comment::find()->where(['post' => $this->id])->count();
        return PointOrderItem::find()->where(['point_order_id' => $this->order_id])->count();
    }
    
    public function getOrderItems()
    {
        return $this->hasMany(PointOrderItem::className(), ['point_order_id' => 'order_id']);
    }
    
    public function getProfile()
    {
        return $this->hasOne(\app\modules\painter\models\PainterProfile::className(), ['user_id' => 'painter_login_id']);
    }
    
    public function getCompany()
    {
        return $this->hasOne(\app\modules\painter\models\CompanyInformation::className(), ['user_id' => 'painter_login_id']);
    }
    
    public function getBank()
    {
        return $this->hasOne(\app\modules\painter\models\BankingInformation::className(), ['user_id' => 'painter_login_id']);
    }
    
    public function getOrderStatus() {
        return $this->hasOne(\common\models\OrderStatus::className(), ['order_status_id' => 'order_status']);
    }
    
    public function getDealerOutlet()
    {
        return $this->hasOne(\common\models\DealerList::className(), ['id' => 'order_dealer_id']);
    }
    
    public function getOtotalItems()
    {
        return $this->hasMany(\common\models\PointOrder::className(), ['painter_login_id' => 'painter_login_id',])->where(['order_status' => '17','redemption' => 'N'])->count();
    }
    
    public function getTotalPointValue()
    {
        return $this->hasMany(\common\models\PointOrder::className(), ['painter_login_id' => 'painter_login_id',])->where(['order_status' => '17','redemption' => 'N'])->sum('order_total_point');
    }
    
    public function getTotalAmount()
    {
        return $this->hasMany(\common\models\PointOrder::className(), ['painter_login_id' => 'painter_login_id',])->where(['order_status' => '17','redemption' => 'N'])->sum('order_total_amount');
    }
    
    public function getRealIp() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {   //check ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {   //to check ip is pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
}
