<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class Findpainter extends Model
{
    public $email;
    public $ic_no;
    public $type;
    public $membership_id;
    public $showpopup;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['type'], 'required'],
            [['ic_no'], 'required',
                'when' => function($model) {
                    return $model->type == 'ic';
                },
                'whenClient' => "function (attribute, value) {
                 return $('#findpainter-type').val() == 'ic';
                 }",
            ],
            [['membership_id'], 'required',
                'when' => function($model) {
                    return $model->type == 'mid';
                },
                'whenClient' => "function (attribute, value) {
                 return $('#findpainter-type').val() == 'mid';
                 }",
            ],
            [['ic_no'], 'string', 'length' => [6, 12]],
            //[['membership_id'], 'integer'],
            [['membership_id'], 'string', 'min' => 13, 'max' => 13],                  
            /*['email', 'email'],
            ['email', 'required'],
            ['email', 'unique', 'message' => 'This email has already been taken.'],*/
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'type' => 'Type',
            'ic_no' => 'NRIC/Passport eg:A75852/610408091234',
            'membership_id' => 'Membership #', 
        ];
    }

    
}
