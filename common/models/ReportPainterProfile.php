<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use yii\helpers\Url;

/**
 * This is the model class for table "painter_profile".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $card_id
 * @property string $photo
 * @property integer $title
 * @property string $full_name
 * @property string $profile_name
 * @property string $mobile
 * @property integer $nationality
 * @property string $ic_no
 * @property integer $race
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property integer $created_by
 * @property integer $updated_by
 */
class ReportPainterProfile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 'X';
    const STATUS_ACTIVE = 'A';
    const STATUS_PENDING = 'P';
    
    public $total_no_transactions;
    public $total_items;
    public $total_points_awarded;
    public $total_rm_awarded;
    public $total_no_of_redemptions;
    public $total_points_redeemed;
    public $total_rm_paid;
    public $balance_total_points;
    public $Balance_rm_to_be_paid;
    
    //[Total # of Transactions][Total Items][Total Points Awarded][Total RM awarded]
    //[Total # of Redemptions][Total Points Redeemed][Total RM paid][Balance Total Points][Balance RM to be paid]

    
    public static function tableName()
    {
        return 'painter_profile';
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'full_name', 'mobile', 'nationality', 'country', 'race'], 'required'],
            [['user_id', 'card_id', 'title', 'country', 'race', 'created_by', 'updated_by'], 'integer'],
            ['ic_no', 'required'],
            ['ic_no', 'unique', 'message' => 'This {attribute} has already been taken.'],
            [['email'], 'email'],
            ['email', 'required'],
            ['email', 'unique', 'message' => 'This {attribute} has already been taken.'],
            [['total_no_transactions','total_items','total_rm_awarded','total_no_of_redemptions','total_points_redeemed','total_rm_paid','balance_total_points','Balance_rm_to_be_paid','created_datetime', 'updated_datetime'], 'safe'],
            [['photo'], 'string', 'max' => 200],
            [['nationality'], 'string', 'max' => 2],
            [['full_name', 'email'], 'string', 'max' => 255],            
            [['profile_name', 'mobile', 'ic_no'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'card_id' => 'Membership No',
            'photo' => 'Photo',
            'title' => 'Title',
            'full_name' => 'Full Name',
            'profile_name' => 'Profile Name',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'nationality' => 'Nationality',
            'country' => 'Country',
            'ic_no' => 'NRIC/Passport Number',
            'race' => 'Race',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'total_no_transactions' => 'Total # of Transactions',
            'total_items' => 'Total Items',
            'total_points_awarded' => 'Total Points Awarded',
            'total_rm_awarded' => 'Total RM Awarded',
            'total_no_of_redemptions' => 'Total # of Redemptions',
            'total_points_redeemed' => 'Total Points Redeemed',
            'total_rm_paid' => 'Total RM paid',
            'balance_total_points' => 'Balance Total Points',
            'Balance_rm_to_be_paid' => 'Balance RM to be paid',
        ];
    }
    
    public function getUser() {
        return $this->hasOne(\common\models\User::className(), ['id' => 'user_id'])->
       andWhere(['status' =>! self::STATUS_DELETED]);
    }
    
    public function getProfileTitle()
    {
        return $this->hasOne(\common\models\TitleOptions::className(), ['id' => 'title']);
    }
    
    public function getProfileCountry()
    {
        return $this->hasOne(\common\models\Country::className(), ['id' => 'country']);
    }
    
    public function getProfileRace()
    {
        return $this->hasOne(\common\models\Race::className(), ['id' => 'race']);
    }
        
    public function getStatusDescription() {
        $returnValue = "";
        if ($this->status == "X") {
            $returnValue = "Deleted";
        } else if ($this->status == "P") {
            $returnValue = "Pending";
        } else if ($this->status == "A") {
            $returnValue = "Active";
        }
        return $returnValue;
    }
    
    public function getTotalnotransactions(){
        $totalnotransactions = \common\models\PointOrder::find()->where(['painter_login_id' => $this->user_id, 'order_status' => '17'])->count();
        $returnValue = $totalnotransactions;
        return $returnValue;
    }
    
    public function getTotalitems(){
        $totalitems = \common\models\PointOrder::find()->where(['painter_login_id' => $this->user_id, 'order_status' => '17'])->sum('order_qty');
        $returnValue = $totalitems;
        return $returnValue;
    }
    
    public function getTotalpointsawarded(){
        $totalpointsawarded = \common\models\PointOrder::find()->where(['painter_login_id' => $this->user_id, 'order_status' => '17'])->sum('order_total_point');
        $returnValue = $totalpointsawarded;
        return $returnValue;
    }
    
    public function getTotalrmawarded(){
        $totalrmawarded = \common\models\PointOrder::find()->where(['painter_login_id' => $this->user_id, 'order_status' => '17'])->sum('order_total_amount');
        $returnValue = $totalrmawarded;
        return $returnValue;
    }
    
    public function getTotal_no_of_redemptions(){
        $total_no_of_redemptions = \common\models\Redemption::find()->where(['painterID' => $this->user_id, 'redemption_status_ray' => '19'])->count();
        $returnValue = $total_no_of_redemptions;
        return $returnValue;
    }
    
    public function getTotal_points_redeemed(){
        $total_points_redeemed = \common\models\Redemption::find()->where(['painterID' => $this->user_id, 'redemption_status_ray' => '19'])->sum('req_points');
        $returnValue = $total_points_redeemed;
        return $returnValue;
    }
    public function getTotal_rm_paid(){
        $total_rm_paid = \common\models\Redemption::find()->where(['painterID' => $this->user_id, 'redemption_status_ray' => '19'])->sum('req_amount');
        $returnValue = $total_rm_paid;
        return $returnValue;
    }
    
    public function getTotal($records, $columns)
        {
            if (!is_array($columns)) { 
                $columns=array($columns);
            }
          
            $total = array();
            foreach ($records as $record) {
                    foreach ($columns as $column) {
                              if(!isset($total[$column]))$total[$column]=0;
                              $total[$column] += $record->$column;
                    }
            }
            return $total;
        }
    
}
