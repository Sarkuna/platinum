<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "actionlog".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $user_id
 * @property string $user_remote
 * @property string $time
 * @property string $action
 * @property string $category
 * @property string $status
 * @property string $message
 * @property string $user_agent
 */
class Actionlog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const LOG_STATUS_SUCCESS = 'success';
    const LOG_STATUS_INFO = 'info';
    const LOG_STATUS_WARNING = 'warning';
    const LOG_STATUS_ERROR = 'error';
    
    
    public static function tableName()
    {
        return 'actionlog';
    }
    
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'time',
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function add($status = null, $message = null, $uID = 0)
    {
        
        $session = Yii::$app->session;
        $model = Yii::createObject(__CLASS__);
        $model->user_id = ((int)$uID !== 0) ? (int)$uID : (int)$model->getUserID();
        $model->user_remote = $_SERVER['REMOTE_ADDR'];
        $model->action = Yii::$app->requestedAction->id;
        $model->category = Yii::$app->requestedAction->controller->id;
        $model->status = $status;
        $model->message = ($message !== null) ? serialize($message) : null;
        $model->user_agent = $model->exactBrowserName();
        return $model->save();
    }
    
    public static function getUserID()
    {
        $user = Yii::$app->getUser();
        return $user && !$user->getIsGuest() ? $user->getId() : 0;
    }
    
    public static function ExactBrowserName()
    {

    $ExactBrowserNameUA= Yii::$app->request->userAgent;

    if (strpos(strtolower($ExactBrowserNameUA), "safari/") and strpos(strtolower($ExactBrowserNameUA), "opr/")) {
        // OPERA
        $ExactBrowserNameBR="Opera";
    } elseIf (strpos(strtolower($ExactBrowserNameUA), "safari/") and strpos(strtolower($ExactBrowserNameUA), "chrome/")) {
        // CHROME
        $ExactBrowserNameBR="Chrome";
    } elseIf (strpos(strtolower($ExactBrowserNameUA), "msie")) {
        // INTERNET EXPLORER
        $ExactBrowserNameBR="Internet Explorer";
    } elseIf (strpos(strtolower($ExactBrowserNameUA), "firefox/")) {
        // FIREFOX
        $ExactBrowserNameBR="Firefox";
    } elseIf (strpos(strtolower($ExactBrowserNameUA), "safari/") and strpos(strtolower($ExactBrowserNameUA), "opr/")==false and strpos(strtolower($ExactBrowserNameUA), "chrome/")==false) {
        // SAFARI
        $ExactBrowserNameBR="Safari";
    } else {
        // OUT OF DATA
        $ExactBrowserNameBR="OUT OF DATA";
    };

    return $ExactBrowserNameBR;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'user_id' => 'User ID',
            'user_remote' => 'User Remote',
            'time' => 'Time',
            'action' => 'Action',
            'category' => 'Category',
            'status' => 'Status',
            'message' => 'Message',
            'user_agent' => 'User Agent',
        ];
    }
}
