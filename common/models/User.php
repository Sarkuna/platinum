<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

use yii\behaviors\BlameableBehavior;
use yii\db\Expression;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    public $rememberMe = true;
    public $current_pass,$new_pass,$retype_pass,$mp_updated_datetime;
    public $create_password, $confirm_password, $admin_user;
    //const STATUS_DELETED = 0;
    //const STATUS_ACTIVE = 10;
    const STATUS_DELETED = 'X';
    const STATUS_ACTIVE = 'A';
    const STATUS_PENDING = 'P';


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            [['auth_key', 'password_hash', 'created_datetime', 'updated_datetime'], 'required'],
            [['new_pass', 'retype_pass'], 'required','on'=>'change','message'=>''],
            ['retype_pass', 'compare','compareAttribute'=>'new_pass'],
            [['del','email'], 'safe'],
            ['confirm_password', 'compare','compareAttribute'=>'create_password', 'on'=>'firstTime'],
	    [['create_password', 'confirm_password', 'admin_user'], 'required', 'on'=>'firstTime'],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
        //throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        $returnValue = false;

        //return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
        if (strpos($username, '@') !== false) {
            $user = static::findOne(['email' => $username, 'status' => self::STATUS_ACTIVE]);
        } else {
            //Otherwise we search using the username
            $user = static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
        }

        if ($user) {
            $returnValue = $user;
            $session = Yii::$app->session;            
            $session['currentRole'] = $user->user_type;
        }
        return $returnValue;
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
    
    public function getStatusDescription() {
        $returnValue = "";
        if ($this->status == "P") {
            $returnValue = "<span class='label label-warning'>Pending</span>";
        } else if ($this->status == "A") {
            $returnValue = "<span class='label label-success'>Active</span>";
        } else if ($this->status == "X") {
            $returnValue = "<span class='label label-danger'>Deleted</span>";
        }
        return $returnValue;
    }
    
    public function getUserPainter() {
        return $this->hasOne(\common\models\PainterProfile::className(), ['user_id' => 'id']);
    }
    
    public function getMembershipPack() {
        return $this->hasOne(\common\models\MembershipPack::className(), ['user_id' => 'id']);
    }
    
    public function login()
    {
         if ($this->validate()) {
             return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0);
         }
         return false;
    }
}