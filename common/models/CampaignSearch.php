<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Campaign;

/**
 * CampaignSearch represents the model behind the search form about `common\models\Campaign`.
 */
class CampaignSearch extends Campaign
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['campaign_id'], 'integer'],
            [['campaign_description', 'campaign_start_date', 'campaign_end_date', 'campaign_status'], 'safe'],
            [['campaign_rm_per_point'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Campaign::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'campaign_id' => $this->campaign_id,
            'campaign_rm_per_point' => $this->campaign_rm_per_point,
            'campaign_start_date' => $this->campaign_start_date,
            'campaign_end_date' => $this->campaign_end_date,
        ]);

        $query->andFilterWhere(['like', 'campaign_description', $this->campaign_description])
            ->andFilterWhere(['like', 'campaign_status', $this->campaign_status]);

        return $dataProvider;
    }
}
