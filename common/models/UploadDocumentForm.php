<?php
namespace common\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Login form
 */
class UploadDocumentForm extends Model
{
    public $files;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['files'], 'required'],
            [['files'], 'file', 'maxFiles' => 5, 'skipOnEmpty' => true, 'extensions'=>['pdf','doc','docx','jpg','jpeg','png','bmp'], 'checkExtensionByMimeType'=>false],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'files' => 'Proof Documents',
        ];
    }

    
}
