<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class Report_Form extends Model
{
    public $reportdate;
    public $reportdateto;
    public $membershipno;
    public $status;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['reportdate','reportdateto'], 'required'],
            [['membershipno', 'status'], 'safe']

        ];
    }
    
    public function attributeLabels()
    {
        return [
            'reportdate' => 'Date',
            'reportdateto' => 'Date To',
            'membershipno' => 'Membership No',
            'status' => 'Status'
        ];
    }

    
}
