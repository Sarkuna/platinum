<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product_category".
 *
 * @property integer $product_category_id
 * @property string $product_category_name
 * @property string $product_category_status
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property integer $created_by
 * @property integer $updated_by
 */
class ProductCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_category_name'], 'required'],
            [['created_datetime', 'updated_datetime'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['product_category_name'], 'string', 'max' => 200],
            [['product_category_status'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_category_id' => 'Product Category ID',
            'product_category_name' => 'Product Category Name',
            'product_category_status' => 'Product Category Status',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
}
