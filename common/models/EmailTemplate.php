<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * This is the model class for table "email_template".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property string $subject
 * @property string $template
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property integer $created_by
 * @property integer $updated_by
 */
class EmailTemplate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'email_template';
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'name', 'subject', 'template'], 'required'],
            [['template'], 'string'],
            [['email','sms','created_datetime', 'updated_datetime'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['code'], 'string', 'max' => 15],
            [['name'], 'string', 'max' => 150],
            [['sms_text'], 'string', 'max' => 500],
            [['subject'], 'string', 'max' => 300],
            [['code'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'name' => 'Name',
            'subject' => 'Subject',
            'template' => 'Email Description',
            'sms_text' => 'SMS Text',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
}
