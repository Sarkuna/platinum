<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "state".
 *
 * @property integer $state_id
 * @property integer $region_id
 * @property string $state_name
 * @property string $state_code
 */
class State extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'state';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['region_id', 'state_name', 'state_code'], 'required'],
            [['region_id'], 'integer'],
            [['state_name'], 'string', 'max' => 75],
            [['state_code'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'state_id' => 'State ID',
            'region_id' => 'Region ID',
            'state_name' => 'State Name',
            'state_code' => 'State Code',
        ];
    }
}
