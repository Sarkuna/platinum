<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class PayoutForm extends Model
{
    public $account_verification;
    public $status;
    public $comment;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'required'],
            [['account_verification', 'comment'], 'safe'],
        ];
    }

    
}
