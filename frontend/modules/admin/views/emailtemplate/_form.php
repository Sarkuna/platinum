<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
//use dosamigos\ckeditor\CKEditor;
use app\widgets\CKEditor;
use iutbay\yii2kcfinder\KCFinderInputWidget;
/* @var $this yii\web\View */
/* @var $model common\models\EmailTemplate */
/* @var $form yii\widgets\ActiveForm */
$session = Yii::$app->session;
$_SESSION['KCFINDER']['uploadURL'] = '/upload';
$_SESSION['KCFINDER'] = array(
'disabled' => false
);
?>
<style>
@charset "UTF-8";
.material-switch > input[type="checkbox"] {
    display: none;   
}

.material-switch > label {
    cursor: pointer;
    height: 0px;
    position: relative; 
    width: 40px;  
}

.material-switch > label::before {
    background: rgb(0, 0, 0);
    box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
    border-radius: 8px;
    content: '';
    height: 16px;
    margin-top: -8px;
    position:absolute;
    opacity: 0.3;
    transition: all 0.4s ease-in-out;
    width: 40px;
}
.material-switch > label::after {
    background: rgb(255, 255, 255);
    border-radius: 16px;
    box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
    content: '';
    height: 24px;
    left: -4px;
    margin-top: -8px;
    position: absolute;
    top: -4px;
    transition: all 0.3s ease-in-out;
    width: 24px;
}
.material-switch > input[type="checkbox"]:checked + label::before {
    background: inherit;
    opacity: 0.5;
}
.material-switch > input[type="checkbox"]:checked + label::after {
    background: inherit;
    left: 20px;
}/* CSS Document */




</style>
<div class="email-template-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body">
        <div class="row">
            <div class="col-lg-2">
                <?= $form->field($model, 'code')->textInput(['readonly' => true,'maxlength' => true]) ?>
            </div>
            <div class="col-lg-4">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-lg-6">
                <?= $form->field($model, 'subject')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-7">
                <?= $form->field($model, 'sms_text')->textarea(['rows' => 5, 'cols' => 5]); ?>
            </div>
            <div class="col-lg-5">
                <label class="text-danger">SMS Notes:</label>
                <ol>
                    <li>ONLY a-z, A-Z, 0-9, !@#$%&*()-_+=;:"'<>,.?/ characters are supported.</li>
                    <li>Concatenated messages will be limited to 760 characters for ASCII or 310 for Unicode.</li>
                    <li>Use semicolon <;> to separate phone numbers for mass sms.</li>
                    <li>Delivery time may vary depending on gateway traffic.</li>
                    <li>SMS credit will be charged regardless of delivery status and number of SMS.</li>

                </ol>
            </div>
        </div>
        <div class="row">
            
            <div class="col-lg-6">
                <ul class="list-group">
                    <?=
                    $form->field($model, 'email', [
                        'template' => "<li class=\"list-group-item\">Are you sure enable Email?<div class=\"material-switch pull-right\">{input}<label for=\"emailtemplate-email\" class=\"label-primary\"></label></div></li>",
                    ])->checkbox([], false)
                    ?>
                    <?=
                    $form->field($model, 'sms', [
                        'template' => "<li class=\"list-group-item\">Are you sure enable SMS?<div class=\"material-switch pull-right\">{input}<label for=\"emailtemplate-sms\" class=\"label-primary\"></label></div></li>",
                    ])->checkbox([], false)
                    ?>
                </ul>
            </div>
        </div>
        <?=
        $form->field($model, 'template')->widget(CKEditor::className(), [
            'options' => ['rows' => 6],
            'preset' => 'full',
            'clientOptions' => [
                'allowedContent' => true
            ],
        ]);
    
        ?>
    </div>
    <div class="box-footer">
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
