<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionExcelimport(){        
        $model = new \common\models\ImportExcelForm();
        $msg = '';


        if ($model->load(Yii::$app->request->post())) {
            $model->excel = UploadedFile::getInstance($model, 'excel');
            if ($model->excel && $model->validate()) {
                $rpath = realpath(Yii::$app->basePath);
                $model->excel->saveAs($rpath.'/web/upload/excelfiles/' . $model->excel->baseName . '.' . $model->excel->extension);

                $inputFile = $rpath.'/web/upload/excelfiles/' . $model->excel->baseName . '.' . $model->excel->extension;
                try{
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
                } catch (Exception $e) {
                    die('Error');
                }

                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                
                //$objPHPExcel = \PHPExcel_IOFactory::load('./test.xlsx');
                $sheetData = $objPHPExcel->getActiveSheet(0)->toArray(null, true, true, true);
                for ($row = 1; $row <= $highestRow; ++ $row) {
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                    if ($row == 1) {
                        continue;
                    }
                    $product_name = $rowData[0][0];
                    $code = $rowData[0][1];
                    $status = $rowData[0][2];

                    $productLists = \common\models\DealerList::find()->where(['address' => $product_name, 'code' => null])->one();
                    $count = count($productLists);
                    if($count > 0){
                        $productLists->code = $code;
                        $productLists->status = $status;
                        $productLists->save(false);
                        $msg .= '<li class="list-group-item"><span class="badge pull-right bg-green"><i class="fa fa-check" aria-hidden="true"></i></span>  Product add <b>('.$product_name.')</b> successfully.</li>';
                    }else{
                        $msg .= '<li class="list-group-item"><span class="badge pull-right bg-red"><i class="fa fa-ban" aria-hidden="true"></i></span>  Product Not Found this -> <b>('.$product_name.')</b></li>';
                    }
                    
                }
                unlink($inputFile);
                return $this->render('summary', [
                    'msg' => $msg,
                ]);
            }else{
                print_r($model->getErrors());
            }
        } else {
            //$session['referrer'] = Yii::$app->request->referrer;
            return $this->render('uploadexcel', [
                        'model' => $model,
            ]);
        }
    }
}
