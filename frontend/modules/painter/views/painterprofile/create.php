<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\painter\models\PainterProfile */

$this->title = 'Register New Painter';
$this->params['breadcrumbs'][] = ['label' => 'Painter Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-xs-12">
  <div class="col-lg-4 col-sm-4 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-plus"></i> <?= Html::encode($this->title) ?></h3></div>
</div>
<div class="painter-profile-create">
    <?= $this->render('_form', [
        'model' => $model,
        'companyInformation' => $companyInformation,
        'bankingInformation' => $bankingInformation,
        'useremail' => $useremail,
]) ?>

</div>