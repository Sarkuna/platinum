<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use common\models\TitleOptions;
use common\models\Race;
use common\models\Banks;
use common\models\Country;
use common\models\DealerList;

/* @var $this yii\web\View */
/* @var $model app\modules\painter\models\PainterProfile */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Update Painter Profile: ' . $model->full_name;
$this->params['breadcrumbs'][] = ['label' => 'Painter Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->full_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<style>
.box .box-solid {
     background-color: #F8F8F8;
}
</style>

<div class="col-xs-12">
    <div class="col-lg-10 col-sm-10 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-edit"></i> <?= Html::encode($this->title) ?></h3></div>
    <div class="col-lg-2 col-sm-2 col-xs-12 no-padding" style="padding-top: 20px !important;">
        <div class="col-xs-12 no-padding">
            <?php //if(Yii::$app->user->can("/painter/painterprofile/create")) { ?>
            <?= Html::a('Back', ['view', 'id' => $model->id], ['class' => 'btn btn-block btn btn-default']) ?>
	<?php //} ?>
        </div>
    </div>
</div>

<div class="col-xs-12 col-lg-12">
    <div class="painter-profile-form">
      <?php $form = ActiveForm::begin(); ?>

        <div class="box-success box view-item col-xs-12 col-lg-12">            

            <div class="box box-solid box-info col-xs-12 col-lg-12 no-padding">
                <div class="box-header with-border">
                    <h4 class="box-title"><i class="fa fa-info-circle"></i> <?php echo Yii::t('app', 'Banking Information'); ?></h4>
                </div>
                
                <div class="box-body">
                    <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                        <div class="col-xs-12 col-sm-4 col-lg-4">
                            <?=
                            $form->field($bankingInformation, 'bank_name')->dropDownList(
                                    ArrayHelper::map(Banks::find()
                                            ->where(['status' => 'A'])
                                            ->all(), 'id', 'bank_name'), ['prompt' => '-- Select --']
                            )
                            ?>                
                        </div>
                        <div class="col-xs-12 col-sm-4 col-lg-4">
                            <?= $form->field($bankingInformation, 'account_name')->textInput(['maxlength' => true]) ?>               
                        </div>
                        <div class="col-xs-12 col-sm-4 col-lg-4">
                            <?= $form->field($bankingInformation, 'account_number')->textInput(['maxlength' => true]) ?>               
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-lg-3">
                            <?= $form->field($bankingInformation, 'account_ic_no')->textInput(['maxlength' => true]) ?>               
                        </div>
                    <div class="col-xs-12 col-sm-4 col-lg-3">
                            <label class="control-label" for="account_ic_no"></label>
                            <p><input type="checkbox"> Copy Profile NRIC <span id="painterprofile-ic_no"><?php echo $profile->ic_no ?></span></p>
                        </div>
                    
                    <div class="col-xs-12 col-sm-4 col-lg-3">
                        <?php
                        echo $form->field($bankingInformation, 'account_no_verification')->radioList(
			['Y' => 'Yes', 'N' => 'No']
   );
                        if($bankingInformation->account_no_verification == 'N'){
                            
                            $countredemption = \common\models\Redemption::find()->where(['painterID' => $bankingInformation->user_id])->count();
                            if($countredemption > 0){
                                $countredemption = \common\models\Redemption::find()->where(['painterID' => $bankingInformation->user_id])->one();
                                echo '<p class="text-danger" style="margin-top: 20px;">'.$countredemption->review_comment.'</p>';
                            }
                        }
                        ?>
                    </div>
                    
                </div>
            </div> 
            <div class="form-group">
                <?= Html::submitButton($bankingInformation->isNewRecord ? 'Create' : 'Update', ['class' => $bankingInformation->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div> 
        </div>
        
      <?php ActiveForm::end(); ?>  
    </div>    
</div>

<script language="JavaScript">    
    $('input[type="checkbox"]').click(function(){
            if($(this).prop("checked") == true){
                var acIC = $('#painterprofile-ic_no').text();
                $('#bankinginformation-account_ic_no').val(acIC);
                $('#bankinginformation-account_ic_no').prop('disabled',true);
            }
            else if($(this).prop("checked") == false){
                $('#bankinginformation-account_ic_no').val('');
                $('#bankinginformation-account_ic_no').prop('disabled',false);
            }
        });
</script>