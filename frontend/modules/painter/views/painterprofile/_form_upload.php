<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use common\models\TitleOptions;
use common\models\Race;
use common\models\Banks;
use common\models\Country;
use common\models\DealerList;

/* @var $this yii\web\View */
/* @var $model app\modules\painter\models\PainterProfile */
/* @var $form yii\widgets\ActiveForm */

?>
<style>
.box .box-solid {
     background-color: #F8F8F8;
}
</style>


<div class="col-xs-12 col-lg-6">
    <div class="painter-profile-form">

       <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

        <div class="box-success box view-item col-xs-12 col-lg-12">            


                <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                    <?= $form->field($fileuploadform, 'files[]')->fileInput(['multiple' => true]) ?>
                </div>
            
            <div class="form-group">
                <?= Html::submitButton('Upload', ['class' => 'btn btn-primary']) ?>
            </div> 
        </div>
        
      <?php ActiveForm::end(); ?>  
    </div>    
</div>