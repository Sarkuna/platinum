<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use common\models\TitleOptions;
use common\models\Race;
use common\models\Banks;
use common\models\Country;
use common\models\DealerList;

/* @var $this yii\web\View */
/* @var $model app\modules\painter\models\PainterProfile */
/* @var $form yii\widgets\ActiveForm */

?>
<style>
.box .box-solid {
     background-color: #F8F8F8;
}
</style>


<div class="row">
<div class="col-xs-12 col-lg-12">
    <div class="painter-profile-form">

        <?php
        if($formapprove->isNewRecord){
            $action = 'approvel';
        }else {
            $action = 'approvel-update';
        }
                $form = ActiveForm::begin([
                    'method' => 'POST',
                    'action' => ['/painter/painterprofile/'.$action, 'id' => $model->id],
                ]);
                ?>

        <div class="box-success box view-item col-xs-12 col-lg-12">            

            <div class="row">
                <div class="col-xs-12 col-sm-4 col-lg-4">
                    <?php echo $form->field($model, 'profile_status')->dropDownList(["P" => "Pending", "A" => "Approve", "R" => "Review", "D" => "Decline"]); ?>
                </div>
            </div>
            
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-lg-8"> 
                    <?= $form->field($formapprove, 'pa_remark')->textarea(['rows'=>5,'cols'=>5]); ?>
                    <?= $form->field($formapprove, 'pa_profileid')->hiddeninput(['value' => $model->id])->label(false); ?>
                </div>

            </div>
            <div class="form-group">
                <?= Html::submitButton($formapprove->isNewRecord ? 'Save' : 'Update', ['class' => $formapprove->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div> 
        </div>
        
      <?php ActiveForm::end(); ?>  
    </div>    
</div>
</div>