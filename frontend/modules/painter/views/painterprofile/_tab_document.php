<?php
use yii\helpers\Html;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="row">
    <div class="col-xs-12">
        <h4 class="edusec-border-bottom-warning page-header edusec-profile-title-1">	
            <i class="fa fa-files-o"></i> Uploaded Document	</h4>
    </div><!-- /.col -->
</div>

<?php
if (!empty($prrofdocument)) {
    echo '<ul class="mailbox-attachments clearfix">';
    foreach ($prrofdocument as $pdocument) {
        $extensionfile = explode(".", $pdocument->file_name);
        $extension = $extensionfile[1];
        //'pdf','doc','docs','jpg','jpeg','png','bmp'
        if($extension == 'pdf' || $extension == 'PDF'){
            $icon = 'fa-file-pdf-o';
        }elseif($extension == 'doc' || $extension == 'docx'){
            $icon = 'fa-file-word-o';
        }elseif($extension == 'jpg' || $extension == 'jpeg' || $extension == 'png' || $extension == 'bmp'){
            $icon = 'fa-file-photo-o';
        }else{
            $icon = '';
        }
        
        //%20
        $dbfilename = str_replace(" ", "%20", $pdocument->file_name);
        //$filename = Yii::$app->request->hostInfo.'/upload/documents/' . $dbfilename;
        //$filename = 'http://duluxpaintersclub.com.my/upload/documents/68_GohLai%20Hua%20-%200122021236%20-%20Tat%20Chong.pdf';
        //echo urlencode($filename);
        //echo str_replace(" ", "%20", $filename);
        //print_r($filename);
        //die();
        /*$head = array_change_key_case(get_headers($filename, TRUE));
        $file = $head['content-length'];
        echo $filename;
        print_r($file);
        die();*/
        if(!empty($pdocument->file_name)){
           $filename = Yii::$app->request->hostInfo.'/upload/documents/' . $dbfilename; 
           $size = $pdocument->getFilesize($filename);
        }else{
           $filename = '' ;
           $size = '';
        }
        //echo $filename;
        //die();
        //$head = array_change_key_case(get_headers($filename, TRUE));
        //$file = $head['content-length'];
        //$fsize  = filesize($filename) . ' bytes';
        //$fsize    = $headers['Content-Length'];
        
        echo '<li>';
        echo '<span class="mailbox-attachment-icon"><i class="fa '.$icon.'"></i></span>';
        echo '<div class="mailbox-attachment-info">
            <a href="/upload/documents/' . $pdocument->file_name . '" target="_blank" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> '.$pdocument->file_name.'</a>
            <span class="mailbox-attachment-size">
                '.$size.'
                <a href="/upload/documents/' . $pdocument->file_name . '" target="_blank" class="btn btn-default btn-xs pull-right"><i class="fa fa-cloud-download"></i></a>
            </span>
        </div>';
        echo '</li>';
    }
    echo '</ul>';
    
}


?>

<?= $this->render('_form_approve', ['model' => $model, 'formapprove' => $formapprove,]) ?>

