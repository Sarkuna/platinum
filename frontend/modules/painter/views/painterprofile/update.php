<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\painter\models\PainterProfile */

$this->title = 'Update Painter Profile: ' . $model->full_name;
$this->params['breadcrumbs'][] = ['label' => 'Painter Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->full_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="col-xs-12">
    <div class="col-lg-10 col-sm-10 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-edit"></i> <?= Html::encode($this->title) ?></h3></div>
    <div class="col-lg-2 col-sm-2 col-xs-12 no-padding" style="padding-top: 20px !important;">
        <div class="col-xs-12 no-padding">
            <?php //if(Yii::$app->user->can("/painter/painterprofile/create")) { ?>
            <?= Html::a('Back', ['index'], ['class' => 'btn btn-block btn btn-default']) ?>
	<?php //} ?>
        </div>
    </div>
</div>


<div class="painter-profile-update">
    <?= $this->render('_form', [
        'model' => $model,
        'companyInformation' => $companyInformation,
        'bankingInformation' => $bankingInformation,
    ]) ?>

</div>
