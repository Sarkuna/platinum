<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use common\models\TitleOptions;
use common\models\Race;
use common\models\Banks;
use common\models\Country;
use common\models\DealerList;

/* @var $this yii\web\View */
/* @var $model app\modules\painter\models\PainterProfile */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Update Company';
$this->params['breadcrumbs'][] = ['label' => $model->full_name, 'url' => ['index', '#' => 'company']];
//$this->params['breadcrumbs'][] = ['label' => $model->full_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<style>
.box .box-solid {
     background-color: #F8F8F8;
}
</style>

<div class="col-xs-12">
    <div class="col-lg-10 col-sm-10 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-edit"></i> <?= Html::encode($this->title) ?></h3></div>
    <div class="col-lg-2 col-sm-2 col-xs-12 no-padding" style="padding-top: 20px !important;">
        <div class="col-xs-12 no-padding">
            <?php //if(Yii::$app->user->can("/painter/painterprofile/create")) { ?>
            <?= Html::a('Back', ['index', '#' => 'company'], ['class' => 'btn btn-block btn btn-default']) ?>
	<?php //} ?>
        </div>
    </div>
</div>

<div class="col-xs-12 col-lg-12">
    <div class="painter-profile-form">
      <?php $form = ActiveForm::begin(); ?>

        <div class="box-success box view-item col-xs-12 col-lg-12">                      
          
            <div class="box box-solid box-info col-xs-12 col-lg-12 no-padding">
                <div class="box-header with-border">
                    <h4 class="box-title"><i class="fa fa-info-circle"></i> <?php echo Yii::t('app', 'Company Information'); ?></h4>
                </div>
                
                <div class="box-body">
                    <div class="col-xs-12 col-sm-12 col-lg-6 no-padding">                        
                        <div class="col-xs-12 col-sm-6 col-lg-6">
                            <?= $form->field($companyInformation, 'company_name')->textInput(['maxlength' => true]) ?>               
                        </div>
                        <div class="col-xs-12 col-sm-4 col-lg-12">
                            <?= $form->field($companyInformation, 'mailing_address')->textarea(array('rows' => 5, 'cols' => 5, 'style' => 'text-transform:uppercase')); ?>
                        </div>                        
                    </div>

                    <div class="col-xs-12 col-sm-12 col-lg-6 no-padding">
                        <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                            <div class="col-xs-12 col-sm-6 col-lg-6">
                                <?= $form->field($companyInformation, 'tel')->textInput(['maxlength' => true]) ?>               
                            </div>
                        </div>                       

                        <div class="col-xs-12 col-sm-4 col-lg-6">
                            <?= $form->field($companyInformation, 'no_painters')->textInput(['maxlength' => true]) ?>               
                        </div>
                        <div class="col-xs-12 col-sm-4 col-lg-6">
                            <?= $form->field($companyInformation, 'painter_sites')->textInput(['maxlength' => true]) ?>               
                        </div>
                        <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                            <div class="col-xs-12 col-sm-6 col-lg-6">
                                <?=
                                $form->field($companyInformation, 'dealer_outlet')->dropDownList(
                                        ArrayHelper::map(DealerList::find()
                                                        ->orderBy('customer_name ASC')
                                                        ->all(), 'id', 'customer_name'), ['prompt' => '-- Select --']
                                )
                                ?>               
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            

            <div class="form-group">
                <?= Html::submitButton($companyInformation->isNewRecord ? 'Create' : 'Update', ['class' => $companyInformation->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div> 
        </div>
        
      <?php ActiveForm::end(); ?>  
    </div>    
</div>