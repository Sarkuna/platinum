<?php
use yii\helpers\Html;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="row">
    <div class="col-xs-12">
        <h4 class="edusec-border-bottom-warning page-header edusec-profile-title-1">	
            <i class="fa fa-files-o"></i> Uploaded Document	</h4>
    </div><!-- /.col -->
</div>

<div class="table-responsive disp-doc">
    <table class="table table-bordered">
        <tbody>
            <tr>
                <th class="text-center"><label for="empdocs-emp_docs_details">Document</label></th>
                <th class="text-center">Created Date</th>
                <th class="text-center " style="width: 34%;">Created By</th>
            </tr>
            <tr>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center "></td>
            </tr>	
        </tbody>
    </table>
</div>
<?= $this->render('_form_approve', ['model' => $model, 'formapprove' => $formapprove,]) ?>

