<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use common\models\TitleOptions;
use common\models\Race;
use common\models\Banks;
use common\models\Country;
use common\models\DealerList;

/* @var $this yii\web\View */
/* @var $model app\modules\painter\models\PainterProfile */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Update';
$this->params['breadcrumbs'][] = ['label' => $model->full_name, 'url' => ['index', '#' => 'bank']];
//$this->params['breadcrumbs'][] = ['label' => $model->full_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<style>
.box .box-solid {
     background-color: #F8F8F8;
}
</style>

<div class="col-xs-12">
    <div class="col-lg-10 col-sm-10 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-edit"></i> <?= Html::encode($this->title) ?></h3></div>
    <div class="col-lg-2 col-sm-2 col-xs-12 no-padding" style="padding-top: 20px !important;">
        <div class="col-xs-12 no-padding">
            <?php //if(Yii::$app->user->can("/painter/painterprofile/create")) { ?>
            <?= Html::a('Back', ['index', '#' => 'bank'], ['class' => 'btn btn-block btn btn-default']) ?>
	<?php //} ?>
        </div>
    </div>
</div>

<div class="col-xs-12 col-lg-12">
    <div class="painter-profile-form">
      <?php $form = ActiveForm::begin(); ?>

        <div class="box-success box view-item col-xs-12 col-lg-12">            

            <div class="box box-solid box-info col-xs-12 col-lg-12 no-padding">
                <div class="box-header with-border">
                    <h4 class="box-title"><i class="fa fa-info-circle"></i> <?php echo Yii::t('app', 'Banking Information'); ?></h4>
                </div>
                
                <div class="box-body">
                    <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                        <div class="col-xs-12 col-sm-4 col-lg-4">
                            <?=
                            $form->field($bankingInformation, 'bank_name')->dropDownList(
                                    ArrayHelper::map(Banks::find()
                                            ->where(['status' => 'A'])
                                            ->all(), 'id', 'bank_name'), ['prompt' => '-- Select --']
                            )
                            ?>                
                        </div>
                        <div class="col-xs-12 col-sm-4 col-lg-4">
                            <?= $form->field($bankingInformation, 'account_name')->textInput(['maxlength' => true]) ?>               
                        </div>
                        <div class="col-xs-12 col-sm-4 col-lg-4">
                            <?= $form->field($bankingInformation, 'account_number')->textInput(['maxlength' => true]) ?>               
                        </div>
                    </div>
                </div>
            </div> 
            <div class="form-group">
                <?= Html::submitButton($bankingInformation->isNewRecord ? 'Create' : 'Update', ['class' => $bankingInformation->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div> 
        </div>
        
      <?php ActiveForm::end(); ?>  
    </div>    
</div>