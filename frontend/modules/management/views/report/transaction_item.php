<?php

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use kartik\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\painter\models\PainterProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Transaction Supporting Document';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-xs-12">
    <div class="box box-primary">
    <div class="box-header with-border">
        <div class="col-lg-12 text-left" style="padding-left: 0px;">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

        </div>


    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="vipcustomer-index">
    <?php                
    $gridColumns = [
        [
            'attribute' => 'card_id',
            'label' => 'Membership#',
            'format' => 'text',
            //'format' => 'html',
            'value' => function ($model) {
                return $model->profile->card_id;
            },
        ],
        [
            'attribute' => 'full_name',
            'label' => 'Full Name',
            'format' => 'html',
            'headerOptions' => ['width' => '150'],
            'value' => function ($model) {
                return $model->profile->full_name;
            },
        ],
        [
            'attribute' => 'ic_no',
            'label' => 'NRIC / Passport #',
            'format' => 'html',
            'headerOptions' => ['width' => '130'],
            'value' => function ($model) {
                return $model->profile->ic_no;
            },       
        ],
        [
            'attribute' => 'order_num',
            'label' => 'Transaction #',
            'format' => 'html',
            'value' => function ($model) {
                return $model->order->order_num;
            },
        ],
        [
            'attribute' => 'order_dealer_id',
            'label' => 'Dealer Name',
            'format' => 'html',
            'value' => function ($model) {
                return $model->order->dealerOutlet->customer_name;
            },
        ],
        [
            'attribute' => 'dealer_invoice_no',
            'label' => 'Dealer Invoice #',
            'format' => 'html',
            'value' => function ($model) {
                return $model->order->dealer_invoice_no;
            },
        ],
        [
            'attribute' => 'dealer_invoice_price',
            'label' => 'Dealer Invoice (RM)',
            //'format' => 'html',
            'format' => ['decimal', 2],
            'value' => function ($model) {
                return $model->order->dealer_invoice_price;
            },
        ],
        [
            'attribute' => 'dealer_invoice_date',
            'label' => 'Dealer Invoice Date',
            //'format' => 'html',
            //'format' => ['datetime', 'php:d-M-y'],
            'value' => function ($model) {
                if($model->order->dealer_invoice_date == '0000-00-00') {
                    return 'N/A';
                }else {
                   return date('d-m-Y', strtotime($model->order->dealer_invoice_date)); 
                }
                
            },
        ],
        [
            'attribute' => 'product_name',
            'label' => 'Product Name',
            'format' => 'html',
            'headerOptions' => ['width' => '180'],
            'value' => function ($model) {
                return $model->productListItem->product_name;
            },
        ],
        [
            'attribute' => 'product_description',
            'label' => 'Product Description',
            'format' => 'html',
            'headerOptions' => ['width' => '180'],
            'value' => function ($model) {
                return $model->productListItem->product_description;
            },
        ],
        [
            'attribute' => 'total_qty',
            'label' => 'Qty',
            'format' => 'html',
            'headerOptions' => ['width' => '180'],
            'value' => function ($model) {
                return $model->total_qty;
            },
            'pageSummary' => true,
        ],
        [
            'attribute' => 'total_qty_point',
            'label' => 'Total Points',
            'format' => 'integer',
            'value' => function ($model) {
                return $model->total_qty_point;
            },
            'contentOptions' => ['class' => 'text-right',],
            'options' => ['width' => '100'],
            'hAlign' => 'right',
            'pageSummary' => true,
        ],
        [
            'attribute' => 'total_qty_value',
            'label' => 'Total RM award',
            'format' => ['decimal', 2],
            'value' => function($model, $key, $index, $obj) {
                $obj->footer +=$model->total_qty_value;
                return $model->total_qty_value;
            },
            'contentOptions' => ['class' => 'text-right',],
            'options' => ['width' => '100'],
            'hAlign' => 'right',
            'pageSummary' => TRUE,
        ],
        [
            'attribute' => 'created_datetime',
            'label' => 'Transaction Date',
            'format' => ['datetime', 'php:d-M-y H:i:s'],
            'value' => function ($model) {
                return $model->order->created_datetime;
            },
        ],
        [
            'attribute' => '1created_datetime',
            'label' => 'Transaction Date',
            'format' => ['datetime', 'php:d-M-y H:i:s'],
            'value' => function ($model) {
                return $model->order->created_datetime;
            },
        ],
        [
            'attribute' => '1created_datetime',
            'label' => 'Transaction Date',
            'format' => ['datetime', 'php:d-M-y H:i:s'],
            'value' => function ($model) {
                return $model->order->created_datetime;
            },
        ],
    ];
    ?>
            
    <?=
                ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'exportConfig' => [
                        ExportMenu::FORMAT_TEXT => false,
                        ExportMenu::FORMAT_PDF => false,
                        ExportMenu::FORMAT_HTML => false,
                    ],
                    'filename' => Html::encode($this->title).Date('YmdGis'),
                    //'showColumnSelector'=> true,
                    //'fontAwesome' => true,
                    //'batchSize' => 50,
                    //'target' => '_blank',
                    //'folder' => '@webroot/tmp', // this is default save folder on server
                ]);
                            
                            
                echo GridView::widget([
                    'tableOptions' => ['id' => 'pointstbl'],
                    'dataProvider' => $dataProvider,
                    //'filterModel' => $searchModel,
                    //'pjax'=>true, 
                    'columns' => $gridColumns,
                    
                ]);
                ?>        
</div>
    </div>
</div>
</div>
<?php
    $script = <<<EOD
                
    $(function () {
        var chkall = $("input[name='export_columns_toggle']");   
        chkall.click(function () {
            if($(this).prop("checked") == true){
                var table = $("table tr");
                table.find("th, td").css('display', '');
            }
            else if($(this).prop("checked") == false){
                var table = $("table tr");
                table.find("th, td").toggle();
            }
        });    
        var chk = $("#w0-cols-list input:checkbox"); 
        var tbl = $("#pointstbl");
        var tblhead = $("#pointstbl th");

        chk.prop('checked', true);
        chk.click(function () {
            var cbox_val = $(this).data('key');
            $("table tr").find("th:eq("+cbox_val+")").toggle();  
            $("table tr").find("td:eq("+cbox_val+")").toggle();
        });   
    });      

EOD;
$this->registerJs($script);
    ?>