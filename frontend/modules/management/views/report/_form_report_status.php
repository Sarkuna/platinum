<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\PointOrder */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    input#findpainter-ic_no {font-size: 35px;}
    .field-findpainter-membership_id {display: none;}
    h3.box-title {
    margin-top: 0;
    font-size: 18px;
}
.datepicker{z-index: 2050 !important;}
</style>
<div class="login-box">
    <div class="login-box-body">
        <h3 class="box-title">Painter Transaction Management Report</h3>
        <div class="point-order-form">
            <p class="text-info"><em>Please select Month from drop-down list</em></p>
            <?php $form = ActiveForm::begin(); ?>

            <?=
        $form->field($model, 'reportdate')->widget(DatePicker::classname(), [
            'options' => ['readOnly' => true, 'placeholder' => 'From date ...'],
            'removeButton' => false,
            'pluginOptions' => [
                'autoclose' => true,
                'todayHighlight' => false,
                'todayBtn' => false,
                'format' => 'yyyy-mm-dd',
                //'viewMode' => "months", 
                //'minViewMode' => "months",
                //'minDate' => 'tenancy_period_from',
                'endDate' => '+0d', 
                //'startDate' => date('#newtenantform-tenancy_period_from'),
            ]
        ])->label(false);
        ?>
         
            <?=
        $form->field($model, 'reportdateto')->widget(DatePicker::classname(), [
            'options' => ['readOnly' => true, 'placeholder' => 'To date ...'],
            'removeButton' => false,
            'pluginOptions' => [
                'autoclose' => true,
                'todayHighlight' => false,
                'todayBtn' => false,
                'format' => 'yyyy-mm-dd',
                //'viewMode' => "months", 
                //'minViewMode' => "months",
                //'minDate' => 'tenancy_period_from',
                'endDate' => '+0d', 
                //'startDate' => date('#newtenantform-tenancy_period_from'),
            ]
        ])->label(false);
        ?>
            
         <?php
                        echo $form->field($model, 'status')->dropDownList(['0' => 'All', '1' => 'Pending', '17' => 'Approve', '7' => 'Cancel'], [
                            'prompt' => '-- Status --',
                        ])->label(false);
                    ?>   
            

        <button class="btn btn-info btn-flat btn-lg" type="submit">Go!</button>
        <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>

<?php
$script = <<< JS

$(document).ready(function () {
    $("#findpainter-type").change(function(){        
        var val = $('#findpainter-type').val();
        if(val == 'mid' ) {
            $('.field-findpainter-membership_id').show();
            $('.field-findpainter-ic_no').hide();
        } else {
            $('.field-findpainter-ic_no').show();
            $('.field-findpainter-membership_id').hide();
        }
    });
});

JS;
$this->registerJs($script);
?>