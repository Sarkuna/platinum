<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\painter\models\PainterProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Transaction Supporting Document';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php

$amount = 0;
$totbox6 = 0;
$qtypoint = 0;
$totalAmount = '0.00';

    ?>
<div class="col-xs-12">                
                <?php
                // Header and Footer options for PDF format
        $ourPdfHeader = [
            'L' => [
                'content'   => 'Summary View',
                'font-size' => 8,
                'color'     => '#333333'
            ],
            'C' => [
                'content'   => '',
                'font-size' => 16,
                'color'     => '#333333'
            ],
            'R' => [
                'content'   => 'Generated' . ': ' . date("D, d-M-Y g:i a T"),
                'font-size' => 8,
                'color'     => '#333333'
            ]
        ];
        $ourPdfFooter = [
            'L'    => [
                'content'    => '',
                'font-size'  => 8,
                'font-style' => 'B',
                'color'      => '#999999'
            ],
            'R'    => [
                'content'     => '[ {PAGENO} ]',
                'font-size'   => 10,
                'font-style'  => 'B',
                'font-family' => 'serif',
                'color'       => '#333333'
            ],
            'line' => TRUE,
        ];

        $exportFilename = date("Y-m-d_H-m-s").'_'.$this->title;

        $exportConfig = [
            GridView::EXCEL => [
                'label'           => 'Excel',
                'icon'            => ' fa fa-file-excel-o',
                'iconOptions'     => ['class' => 'text-success'],
                'showHeader'      => TRUE,
                'showPageSummary' => TRUE,
                'showFooter'      => false,
                'showCaption'     => TRUE,
                'filename'        => $exportFilename,
                'alertMsg'        => 'The EXCEL export file will be generated for download.',
                'options'         => ['title' => 'Microsoft Excel 95+'],
                'mime'            => 'application/vnd.ms-excel',
                'config'          => [
                    'worksheet' => 'Worksheet',
                    'cssFile'   => ''
                ]
            ],
        ];
                $gridColumns = [
                        [
                            'attribute' => 'card_id',
                            'label' => 'Membership#',
                            'format'=>'text', 
                            //'format' => 'html',
                            'value' => function ($model) {
                                return $model->profile->card_id;
                            },
                            //'group'=>true,  // enable grouping
                            //'subGroupOf'=>1        
                        ],            
                        [
                            'attribute' => 'full_name',
                            'label' => 'Full Name',
                            'format' => 'html',
                            'headerOptions' => ['width' => '150'],
                            'value' => function ($model) {
                                return $model->profile->full_name;
                            },
                            //'group'=>true,  // enable grouping
                            //'subGroupOf'=>1        
                        ],
                        [
                            'attribute' => 'ic_no',
                            'label' => 'NRIC / Passport #',
                            'format' => 'html',
                            'headerOptions' => ['width' => '130'],
                            'value' => function ($model) {
                                return $model->profile->ic_no;
                            },
                            //'group'=>true,  // enable grouping
                            //'subGroupOf'=>1        
                        ],
                        [
                            'attribute' => 'order_num',
                            'label' => 'Transaction #',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->order->order_num;
                            },
                            //'group'=>true,  // enable grouping
                            //'subGroupOf'=>1
                        ],
                        [
                            'attribute' => 'order_dealer_id',
                            'label' => 'Dealer Name',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->order->dealerOutlet->customer_name;
                            },
                            //'group'=>true,  // enable grouping
                            //'subGroupOf'=>1        
                        ],           
                        [
                            'attribute' => 'dealer_invoice_no',
                            'label' => 'Dealer Invoice #',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->order->dealer_invoice_no;
                            },
                            //'group'=>true,  // enable grouping
                            //'subGroupOf'=>1        
                        ],
                        [
                            'attribute' => 'dealer_invoice_price',
                            'label' => 'Dealer Invoice (RM)',
                            //'format' => 'html',
                            'format' => ['decimal', 2],
                            'value' => function ($model) {
                                return $model->order->dealer_invoice_price;
                            },
                                   
                            //'group'=>true,  // enable grouping
                            //'subGroupOf'=>1        
                        ],
                        [
                            'attribute' => 'dealer_invoice_price',
                            'label' => 'Dealer Invoice (RM)',
                            //'format' => 'html',
                            'format'=>['datetime', 'php:d-M-y'],
                            'value' => function ($model) {
                                return $model->order->dealer_invoice_date;
                            },
                                   
                            //'group'=>true,  // enable grouping
                            //'subGroupOf'=>1        
                        ],                    
                        [
                            'attribute' => 'product_name',
                            'label' => 'Product Name',
                            'format' => 'html',
                            'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->productListItem->product_name;
                            },
                            //'group'=>true,  // enable grouping
                            //'subGroupOf'=>1         
                        ],
                        [
                            'attribute' => 'product_description',
                            'label' => 'Product Description',
                            'format' => 'html',
                            'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->productListItem->product_description;
                            },
                        ],            
                        [
                            'attribute' => 'total_qty',
                            'label' => 'Qty',
                            'format' => 'html',
                            'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->total_qty;
                            },
                            'pageSummary' => true,        
                        ], 
                        [
                            'attribute' => 'total_qty_point',
                            'label' => 'Total Points',
                            'format'=>'integer',
                            'value' => function ($model) {
                                return $model->total_qty_point;
                            },
                            'contentOptions' =>['class' => 'text-right',],         
                            'options' => ['width' => '100'],
                            'hAlign'=>'right',        
                            'pageSummary' => true,         
                        ],
                        [
                            'attribute' => 'total_qty_value',
                            'label' => 'Total RM award',
                            'format' => ['decimal', 2],
                            'value' => function($model, $key, $index, $obj){
                                $obj->footer +=$model->total_qty_value;
                                return $model->total_qty_value;
                            },        
                            'contentOptions' =>['class' => 'text-right',],         
                            'options' => ['width' => '100'],
                            'hAlign'=>'right',
                            'pageSummary' => TRUE, 
                        ],             
                        [
                            'attribute' => 'created_datetime',
                            'label' => 'Transaction Date',
                            'format'=>['datetime', 'php:d-M-y H:i:s'],
                            'value' => function ($model) {
                                return $model->order->created_datetime;
                            },   
                        ],
                        [
                            'attribute' => 'order_id',
                            'label' => 'Redemption #',
                            //'format'=>['datetime', 'php:d-M-y H:i:s'],
                            'value' => function ($model) {
                                return $model->order->redemptionItems->redemption->redemption_invoice_no;
                                //return $model->Redemption;//.'++++++'.$model->order->order_id;
                            },    
                        ],
                        [
                            'attribute' => 'order_id',
                            'label' => 'Redemption Status',
                            //'format'=>['datetime', 'php:d-M-y H:i:s'],
                            'value' => function ($model) {
                                return $model->order->redemptionItems->redemption->orderStatus->name;
                                //return $model->Redemption;//.'++++++'.$model->order->order_id;
                            },    
                        ],
                    ];
                echo GridView::widget([
                    'dataProvider'=> $dataProvider,
                    //'filterModel' => $searchModel,
                    'autoXlFormat'=>true,
                    'columns' => $gridColumns,
                    'pjax' => true,
                    'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container']],
                    'bordered' => true,
                    'striped' => false,
                    'condensed' => false,
                    'responsive' => true,
                    'hover' => true,
                    //'exportConfig'     => $exportConfig,
                    'export' => false,
                    'toolbar' =>  [
                        ['content'=>
                            //Html::button('<i class="fa fa-file-excel-o"></i> Export to Excel', ['my-action', 'type'=>'button', 'title'=>'hhh', 'class'=>'btn btn-success', 'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . '&nbsp;&nbsp;&nbsp;'.
                            Html::a(Yii::t('app', '<i class="fa fa-file-excel-o"></i> Export to Excel'), ['transaction-supporting-document-excel'], ['class' => 'btn btn-primary',
                                'data' => [
                                    //'confirm' => Yii::t('app', 'Are you sure you want to download?'),
                                    'method' => 'post',
                                ],
                            ]).
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['/management/report/transactionitem'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>'ggg'])
                        ],
                        //'{export}',
                        //'{toggleData}'
                    ],
                    'floatHeader' => false,
                    //'floatHeaderOptions' => ['scrollingTop' => $scrollingTop],
                    'showPageSummary' => true,
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY,
                        'heading' => '<i class="fa fa-th-list"></i> '.Html::encode($this->title).''
                    ],
                    
                ]);
                ?>
                
<!--            </div>
        </div>
    </div>-->
</div>