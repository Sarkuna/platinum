<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\painter\models\PainterProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Painter Account Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-xs-12">
<!--    <div class="box">
        <div class="box-header with-border">
            <div class="col-lg-4 col-sm-4 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-th-list"></i></h3></div>
            <div class="col-xs-6"></div>
            <div class="col-lg-2 col-sm-2 col-xs-12 no-padding">
                <div class="col-xs-12 no-padding"></div>
            </div>
        </div>
        <div class="box-body table-responsive">
            <div class="painter-profile-index">-->
                
                <?php

 
                $gridColumns = [
                        //['class' => 'yii\grid\SerialColumn'],
                        //['class' => 'kartik\grid\SerialColumn'],
                        //'item_id',
                        //'point_order_id',
                    [
                        'attribute' => 'card_id',
                        'label' => 'Membership #',
                        'format' => 'html',
                        'value' => function ($model) {
                            //echo '<pre>';
                           // print_r($model->userPainter['card_id']);
                            return $model->userPainter['card_id'];
                        },
                    ],
                    [
                        'attribute' => 'full_name',
                        'label' => 'Full Name',
                        'format' => 'html',
                        'value' => function ($model) {
                            //return $model->full_name;
                            return $model->userPainter['full_name'];
                        },
                    ],
                    [
                        'attribute' => 'ic_no',
                        'label' => 'NRIC/Passport #',
                        'format' => 'html',
                        'value' => function ($model) {
                            return $model->userPainter['ic_no'];
                        },
                    ],
                    [
                        'attribute' => 'mobile',
                        'label' => 'Mobile No',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '150'],
                        'value' => function ($model) {
                            return $model->userPainter['mobile'];
                        },
                    ], 
                    
                    [
                        'attribute' => 'email',
                        'label' => 'Email ID',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '180'],
                        'value' => function ($model) {
                            return $model->email;
                        },
                    ],
                                
                    [
                        'attribute' => 'created_datetime',
                        'label' => 'Date Create',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '180'],
                        'format' => ['date', 'php:d-m-Y'],
                        'value' => function ($model) {
                            return $model->created_datetime;
                        },
                    ],  
                                
                    [
                        'attribute' => 'updated_datetime',
                        'label' => 'Last Login',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '180'],
                        'format' => ['date', 'php:d-m-Y'],
                        'value' => function ($model) {
                            return $model->updated_datetime;
                        },
                    ],
           
                                    
                                    
                    ];
                echo GridView::widget([
                    'dataProvider'=> $dataProvider,
                    //'filterModel' => $searchModel,
                    'autoXlFormat'=>true,
                    'columns' => $gridColumns,
                    'pjax' => true,
                    'bordered' => true,
                    'striped' => false,
                    'condensed' => false,
                    'responsive' => true,
                    'hover' => true,
                    'export' => false,
                    'toolbar' =>  [
                        ['content'=>
                            //Html::button('<i class="fa fa-file-excel-o"></i> Export to Excel', ['my-action', 'type'=>'button', 'title'=>'hhh', 'class'=>'btn btn-success', 'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . '&nbsp;&nbsp;&nbsp;'.
                            Html::a(Yii::t('app', '<i class="fa fa-file-excel-o"></i> Export to Excel'), ['painter-account-management-excel'], ['class' => 'btn btn-primary',
                                'data' => [
                                    'confirm' => Yii::t('app', 'Are you sure you want to download?'),
                                    'method' => 'post',
                                ],
                            ]).
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['/management/report/painterprofilesummery'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>'ggg'])
                        ],
                        //'{export}',
                        //'{toggleData}'
                    ],
                    //'exportConfig'     => $exportConfig,
                    'floatHeader' => false,
                    //'floatHeaderOptions' => ['scrollingTop' => $scrollingTop],
                    'showPageSummary' => false,
                    'resizableColumns'=>true,
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY,
                        'heading' => '<i class="fa fa-th-list"></i> '.Html::encode($this->title).''
                    ],
                    
                ]);
                ?>
                
<!--            </div>
        </div>
    </div>-->
</div>