<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;


/* @var $this yii\web\View */
/* @var $model common\models\ExcelTransaction */
/* @var $form yii\widgets\ActiveForm */

for ($i=0; $i<=1; $i++) {
    $months[date("Y-m-d", strtotime( date( 'Y-m-01' )." -$i months"))] = date("F Y", strtotime( date( 'Y-m-01' )." -$i months"));
}
$reverse_months = array_reverse($months);


?>

<div class="excel-transaction-form">
<div class="box-body">
            <div class="col-md-6 no-padding">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
<?= $form->field($model, 'approval_month')->dropDownList(
        $reverse_months,
        ['prompt'=>'Select ']) 
    ?>
    
    <?= $form->field($model, 'excel')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Upload', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
            </div>
</div>
</div>
