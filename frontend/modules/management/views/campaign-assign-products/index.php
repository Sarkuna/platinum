<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CampaignAssignProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Campaign Assign Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="campaign-assign-products-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Campaign Assign Products', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'cap_id',
            'campaign_id',
            'product_list_id',
            'new_points_per_l',
            'cap_datetime',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
