<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CampaignAssignProducts */

$this->title = 'Update Campaign Assign Products: ' . $model->cap_id;
$this->params['breadcrumbs'][] = ['label' => 'Campaign Assign Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cap_id, 'url' => ['view', 'id' => $model->cap_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="campaign-assign-products-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
