<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CampaignAssignProductsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="campaign-assign-products-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'cap_id') ?>

    <?= $form->field($model, 'campaign_id') ?>

    <?= $form->field($model, 'product_list_id') ?>

    <?= $form->field($model, 'new_points_per_l') ?>

    <?= $form->field($model, 'cap_datetime') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
