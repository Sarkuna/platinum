<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SMSQueueSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="smsqueue-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'sms_queue_id') ?>

    <?= $form->field($model, 'painterID') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'mobile') ?>

    <?= $form->field($model, 'redemption_invoice_no') ?>

    <?php // echo $form->field($model, 'r_points') ?>

    <?php // echo $form->field($model, 'r_amount') ?>

    <?php // echo $form->field($model, 'data') ?>

    <?php // echo $form->field($model, 'email_template_id') ?>

    <?php // echo $form->field($model, 'date_to_send') ?>

    <?php // echo $form->field($model, 'created_datetime') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
