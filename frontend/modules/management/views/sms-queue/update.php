<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SMSQueue */

$this->title = 'Update Smsqueue: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Smsqueues', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->sms_queue_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="smsqueue-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
