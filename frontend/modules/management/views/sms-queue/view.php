<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\SMSQueue */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Smsqueues', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="smsqueue-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->sms_queue_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->sms_queue_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'sms_queue_id',
            'painterID',
            'name',
            'mobile',
            'redemption_invoice_no',
            'r_points',
            'r_amount',
            'data:ntext',
            'email_template_id:email',
            'date_to_send',
            'created_datetime',
            'status',
        ],
    ]) ?>

</div>
