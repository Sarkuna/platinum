<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\SMSQueueSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'SMS Queues';
$this->params['breadcrumbs'][] = $this->title;
$result = Yii::$app->ici->ismscURL("https://www.isms.com.my/isms_balance.php?un=".urlencode(Yii::$app->params['sms.username'])."&pwd=".urlencode(Yii::$app->params['sms.password']));
?>
<div class="col-lg-12">
<div class="box">
    <div class="box-header with-border">
        <div class="col-lg-8 col-sm-10 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-th-list"></i> <?= Html::encode($this->title) ?></h3></div>
        <div class="col-lg-4 col-sm-2 col-xs-12 text-right no-padding"><span class="pull-right">SMS Credits Remaining <span class="badge bg-green" id="smsremain"><?php echo $result ?></span></span></div>
    </div>
    <div class="box-body table-responsive">
<div class="smsqueue-index">
    <?=Html::beginForm(['/management/sms-queue/bulksms'],'post');?>
                <div class="col-lg-12 actionbulk no-padding">

                    <div class="col-lg-4 no-padding">
                        <label class="control-label" for="redemptionbulk-comment"></label>
                        <div class="form-group">
                            <?php
                            if ($dataProvider->totalCount > 0) {
                                echo Html::submitButton('Send', ['class' => 'btn btn-success', 'id' => 'btnsubmit',]);
                            }
                            ?>
                            
                        </div>
                    </div>                      
                </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\CheckboxColumn','headerOptions' => ['width' => '10'],],
            //'sms_queue_id',
            //'painterID',
            'name',
            'mobile',
            
            [
                'attribute' => 'total_points',
                'label' => 'Total Points',
                'format' => 'html',
                'headerOptions' => ['width' => '100', 'class' => 'text-center'],
                'contentOptions' =>['class' => 'text-right'],
                'value' => function ($model) {
                    return $model->getTotalPoints();
                },
            ],
            [
                'attribute' => 'totalpoints',
                'label' => 'Total RM Amount',
                'format' => 'html',
                'headerOptions' => ['width' => '130', 'class' => 'text-center'],
                'contentOptions' =>['class' => 'text-right'],
                'value' => function ($model) {
                    return $model->getTotalAmounts();
                },
            ],
            [
                'attribute' => 'total_transactions',
                'label' => 'Total Transactions',
                'format' => 'html',
                'headerOptions' => ['width' => '140', 'class' => 'text-center'],
                'contentOptions' =>['class' => 'text-right'],
                'value' => function ($model) {
                    return $model->getTotalTransactions();
                },
            ],            
            //'redemption_invoice_no',
            // 'r_points',
            // 'r_amount',
            // 'data:ntext',
            // 'email_template_id:email',
            // 'date_to_send',
            // 'created_datetime',
            // 'status',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?= Html::endForm();?> 
</div>
    </div>
</div>
</div>