<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DealerListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Dealer Lists';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-12">
    <div class="box">
        <div class="box-header with-border">
            <div class="col-lg-8 col-sm-10 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-th-list"></i> <?= Html::encode($this->title) ?></h3></div>
            <div class="col-lg-4 col-sm-2 col-xs-12 text-right no-padding">
                <p>
                    <?= Html::a('Add New Dealer', ['create'], ['class' => 'btn btn-success']) ?>
                </p>
            </div>
        </div>
        <div class="box-body table-responsive">
            <div class="dealer-list-index">
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        //'id',
                        'customer_name',
                        //'account_type',
                        'region',
                        'state',
                        // 'area',
                        // 'address:ntext',
                        'contact_person',
                        'contact_no',                        
                        // 'status',
                        // 'created_datetime',
                        // 'created_by',
                        // 'updated_datetime',
                        // 'updated_by',
                        ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>

