<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DealerList */

$this->title = 'Edit Dealer : ' . $model->customer_name;
$this->params['breadcrumbs'][] = ['label' => 'Manage Dealer Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->customer_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="col-md-12">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div><!-- /.box-header -->
        <div class="dealer-list-update">
            <?=
            $this->render('_form', [
                'model' => $model,
            ])
            ?>
        </div>
    </div>
</div>
