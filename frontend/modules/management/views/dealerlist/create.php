<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DealerList */

$this->title = 'New Dealer';
$this->params['breadcrumbs'][] = ['label' => 'Dealer Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-12">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div><!-- /.box-header -->
        <div class="dealer-list-create">
            <?=
            $this->render('_form', [
                'model' => $model,
            ])
            ?>
        </div>
    </div>
</div>
