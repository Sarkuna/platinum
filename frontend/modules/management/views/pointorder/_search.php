<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PointOrderSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="point-order-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'order_id') ?>

    <?= $form->field($model, 'order_dealer_id') ?>

    <?= $form->field($model, 'painter_login_id') ?>

    <?= $form->field($model, 'painter_ic') ?>

    <?= $form->field($model, 'order_qty') ?>

    <?php // echo $form->field($model, 'order_total_point') ?>

    <?php // echo $form->field($model, 'order_total_amount') ?>

    <?php // echo $form->field($model, 'order_status') ?>

    <?php // echo $form->field($model, 'order_remarks') ?>

    <?php // echo $form->field($model, 'order_IP') ?>

    <?php // echo $form->field($model, 'created_datetime') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_datetime') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
