<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use common\models\DealerList;

/* @var $this yii\web\View */
/* @var $model common\models\PointOrder */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .point-order-form .dropdown-menu.open {
        z-index: 5000 !important;
    }
    
    .table > thead:first-child > tr:first-child > th, .table > thead:first-child > tr:first-child > td, .table-striped thead tr.primary:nth-child(odd) th {
    background-color: #428BCA;
    color: white;
    border-color: #357EBD;
    border-top: 1px solid #357EBD;
    text-align: center;
}
input#totalqty {
    width: 50px;
}
</style>
<div class="login-box" style="margin-top: 0px;">
<div class="login-box-body">
<div class="point-order-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($profile, 'ic_no', [
        'template' => '{label} <div class=""><div class="col-lg-12 input-group input-group">{input}
        <span class="input-group-btn"></span></div>{error}{hint}</div>',
    ])->textInput(['autocomplete' => 'off', 'disabled' => true]) ?>
    <?php
    $listData= ArrayHelper::map(DealerList::find()->orderBy('customer_name ASC')->all(), 'id', 'customer_name');

    ?>
    <?= $form->field($pointorder, 'order_dealer_id', [
        'template' => '{label} <div class=""><div class="col-lg-12 input-group input-group">{input}
        <span class="input-group-btn"><button class="btn btn-info btn-flat" type="button"><i class="fa fa-plus-circle" id="addIcon"></i></button></span></div>{error}{hint}</div>',
    ])->dropDownList($listData,['prompt'=>'Select...', 'class' => 'form-control selectpicker', 'data-live-search' => 'true']) ?>
    
    <div class="form-group" id="ui">
        <div class="input-group">
            <input type="text" name="add_item" value="" class="form-control pos-tip ui-autocomplete-input" id="add_item" data-placement="top" data-trigger="focus" placeholder="Scan/Search product by name/code" title="" autocomplete="off" data-original-title="Please start typing code/name for suggestions or just scan barcode" tabindex="1">
            <div class="input-group-addon" style="padding: 2px 8px;">
                <a href="#" id="addManually" tabindex="-1">
                    <i class="fa fa-barcode" id="addIcon" style="font-size: 1.5em;"></i>
                </a>
            </div>
        </div>
        <div style="clear:both;"></div>
    </div>
    

    <div class="direct-chat-messages no-padding">
        <table class="table fixed" id="retc"> 
            <thead>
                <tr>
                    <th class="hide">barcodename</th>
                    <th class="hide">barcodeid</th>
                    <th width="60%">Product</th>
                    <th width="15%">Point</th>
                    <th width="15%">Status</th>
                    <th style="width: 5%; text-align: center;"><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i></th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
    <table style="width:100%; float:right; padding:5px; color:#000; background: #FFF;" id="totalTable">
        <tbody>
            <tr>
                <td colspan="2" style="padding: 5px 10px; border-top: 1px solid #666; border-bottom: 1px solid #333; font-weight:bold; background:#333; color:#FFF;">
                    Total Items                                        </td>
                <td align="right" style="padding:5px 10px 5px 10px; font-size: 14px;border-top: 1px solid #666; border-bottom: 1px solid #333; font-weight:bold; background:#333; color:#FFF;" class="text-right">

                </td>
                <td align="right" style="padding:5px 10px 5px 10px; font-size: 14px;border-top: 1px solid #666; border-bottom: 1px solid #333; font-weight:bold; background:#333; color:#FFF;">
                    <input class="form-control" type="text" id="totalqty" name="totalqty" value="0" />
                </td>
            </tr>
        </tbody></table>
    <?php
    $script = <<<EOD
          
                var handledCount = 0;
                $('#add_item').keyup(function(){
                    var test = $("#add_item").val();
                    var flag = 0;
                    $("#retc").find("tr").each(function () {
                        var td1 = $(this).find("td:eq(0)").text();
                        if (test == td1) {
                            flag = 1;
                        }
                    });
            
                    if (handledCount == 0){
                        var itemID = $(this).val(),vNode  = $(this);
                        $.ajax({
                            url:'getitem?id='+itemID,
                            dataType: 'json',
                            success : function(data) {
                                if (flag == 1) {
                                    alert('not allowed');
                                    $("#add_item").val('');
                                    $("#add_item").focus();
                                } else {
                                    $('#retc tbody').append("<tr id="+ data.barcodeid + "><td class='hide'>" + data.barcodename+ "</td><td class='hide'><input class='form-control' type='text' name='barcodeid[]' value='"+ data.barcodeid + "'/></td><td>" + data.product + "</td><td>" + data.point+ "</td><td>" + data.status+ "</td><td><a href='javascript::;' class='btnDelete'><i class='fa fa-trash-o' style='opacity:0.5; filter:alpha(opacity=50);'></i></a></td></tr>");
                                    $("#add_item").val('');
                                    $("#add_item").focus();
                                    var count = $('#retc tbody').children('tr').length;
                                    $("#totalitem").html(count);
                                    $("#totalqty").val(count);
                                }
                            },
                            error : function() {
                                console.log('error');
                            }
                        });
                    }
                    handledCount++;
                    if (handledCount == 2)
                        handledCount = 0;
                    return false;
                    $("#add_item").val('');
                });
            
                $("#addManually").on('click',function(){
                    var test = $("#add_item").val();
                    //alert(test);
                    var flag = 0;
                    $("#retc").find("tr").each(function () {
                        var td1 = $(this).find("td:eq(0)").text();
                        if (test == td1) {
                            flag = 1;
                        }
                    });
            
                    if (handledCount == 0){
                        var itemID = $("#add_item").val(),vNode  = $(this);
                        $.ajax({
                            url:'getitem?id='+itemID,
                            dataType: 'json',
                            success : function(data) {
                                if (flag == 1) {
                                    alert('not allowed');
                                    $("#add_item").val('');
                                    $("#add_item").focus();
                                } else {
                                    $('#retc tbody').append("<tr id="+ data.barcodeid + "><td class='hide'>" + data.barcodename+ "</td><td class='hide'><input class='form-control' type='text' name='barcodeid[]' value='"+ data.barcodeid + "'/></td><td>" + data.product + "</td><td>" + data.point+ "</td><td>" + data.status+ "</td><td><a href='javascript::;' class='btnDelete'><i class='fa fa-trash-o' style='opacity:0.5; filter:alpha(opacity=50);'></i></a></td></tr>");
                                    $("#add_item").val('');
                                    $("#add_item").focus();
                                    var count = $('#retc tbody').children('tr').length;
                                    $("#totalitem").html(count);
                                    $("#totalqty").val(count);
                                }
                            },
                            error : function() {
                                console.log('error');
                            }
                        });
                    }
                    handledCount++;
                    if (handledCount == 2)
                        handledCount = 0;
                    return false;
                    $("#add_item").val('');
                });

            
            $("#retc").on('click','.btnDelete',function(){
                //$(this).closest('tr').remove();
                var tableRow = $(this).closest('tr');
                tableRow.find('td').fadeOut('fast', 
                    function(){ 
                        tableRow.remove();
                        var count = $('#retc tbody').children('tr').length;
                        $("#totalitem").html(count);
                        $("#totalqty").val(count);
                    }
                );
            });
EOD;
$this->registerJs($script);
    ?>
    <?= $form->field($pointorder, 'order_remarks')->textarea(['rows' => 6]) ?>
    <div class="row">
    <div class="col-xs-6">
        
        <?= Html::submitButton($pointorder->isNewRecord ? Yii::t('app', 'Submit') : Yii::t('app', 'Save'), ['class' => $pointorder->isNewRecord ? 'btn btn-block btn-info btgr' : 'btn btn-block btn-info btgr']) ?>
    </div>
    </div>
    <?php ActiveForm::end(); ?>
    
    

</div>
</div>
</div>
