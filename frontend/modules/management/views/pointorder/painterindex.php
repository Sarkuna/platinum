<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PointOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Transactions';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-xs-12">
    <div class="box">
        <div class="box-header with-border">
            <div class="col-lg-8 col-sm-10 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-th-list"></i> <?= Html::encode($this->title) ?></h3></div>
            <div class="col-lg-4 col-sm-2 col-xs-12 text-right no-padding">

            </div>
        </div>
        <div class="box-body table-responsive">
            <div class="point-order-index">
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    //'filterModel' => $searchModel,
                    'columns' => [
                        //['class' => 'yii\grid\SerialColumn'],
                        //'order_id',
                        //'order_num',
                        [
                            'attribute' => 'order_num',
                            'label' => 'Transaction #',
                            'format' => 'html',
                            'headerOptions' => ['width' => '120', 'class' => 'text-center'],
                            'value' => function ($model) {
                                return $model->order_num;
                            },
                        ],
                        //'order_dealer_id',
                        [
                            'attribute' => 'order_dealer_id',
                            'label' => 'Dealer Name',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '95'],
                            'value' => function ($model) {
                                return $model->dealerOutlet->customer_name;
                            },
                        ],
                        [
                            'attribute' => 'dealer_invoice_no',
                            'label' => 'Dealer Invoice',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '95'],
                            'value' => function ($model) {
                                return $model->dealer_invoice_no;
                            },
                        ], 
                        [
                            'attribute' => 'dealer_invoice_price',
                            'label' => 'Dealer Invoice Value',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '95'],
                            'value' => function ($model) {
                                return $model->dealer_invoice_price;
                            },
                        ],  
                        [
                            'attribute' => 'total_items',
                            //'label' => 'Total Transactions',
                            'format' => 'html',
                            'headerOptions' => ['width' => '60', 'class' => 'text-center'],
                            'contentOptions' =>['class' => 'text-right'],
                            'value' => function ($model) {
                                return $model->getTotalItems();
                            },
                        ],            
                        [
                            'attribute' => 'order_total_point',
                            'label' => 'Total Point',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '95'],
                            'headerOptions' => ['width' => '110', 'class' => 'text-center'],
                            'contentOptions' =>['class' => 'text-right'],
                            'value' => function ($model) {
                                return $model->order_total_point;
                            },
                        ],            
                        /*[
                            'attribute' => 'painter_ic',
                            'label' => 'NRIC/PP Number',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->painter_ic;
                            },
                        ],*/         
                        //'order_qty',
                        // 'order_total_point',
                        // 'order_total_amount',
                        // 'order_status',
                        [
                            'attribute' => 'order_total_amount',
                            'label' => 'Total RM',
                            'format' => 'html',
                            'headerOptions' => ['width' => '80', 'class' => 'text-center'],
                            'contentOptions' =>['class' => 'text-right'],
                            'value' => function ($model) {
                                return $model->order_total_amount;
                            },
                        ],  
                        [
                            'attribute' => 'order_status',
                            'label' => 'Status',
                            'format' => 'html',
                            'headerOptions' => ['width' => '95'],
                            'value' => function ($model) {
                                return $model->orderStatus->name;
                            },
                        ],            
                        // 'order_remarks:ntext',
                        // 'order_IP',
                        //'created_datetime',
                        [
                            'label' => 'Created Date',
                            'attribute' => 'created_datetime',
                            'headerOptions' => ['width' => '100'],
                            'format' => ['date', 'php:d-m-Y']
                        ],             
                        // 'created_by',
                        // 'updated_datetime',
                        // 'updated_by',
                        //['class' => 'yii\grid\ActionColumn'],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{view}', //{update} {delete}
                            'buttons' => [
                                'view' => function ($url, $model) {
                                    $url = 'myview?id=' . $model->order_id;
                                    return (Html::a('<span class="glyphicon glyphicon-search"></span>', $url, ['title' => Yii::t('app', 'Update'),]));
                                    }
                                ],
                                //'visible' => $visible,
                        ],         
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>

