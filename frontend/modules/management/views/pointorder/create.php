<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PointOrder */

$this->title = 'Create Point Order';
$this->params['breadcrumbs'][] = ['label' => 'Point Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="point-order-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
