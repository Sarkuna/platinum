<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\PointOrder */

$this->title = $model->profile->full_name;
$this->params['breadcrumbs'][] = ['label' => 'Painter Transaction Management', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

  <section class="invoice">
          <!-- title row -->
          <div class="row">
            <div class="col-xs-12">
              <h2 class="page-header">
                <i class="fa fa-globe"></i> <b>Transaction # <?= $model->order_num ?></b>
                <small class="pull-right">Date: <?php echo date('d-m-Y', strtotime($model->created_datetime))?></small>
              </h2>
            </div><!-- /.col -->
          </div>
          <!-- info row -->
          <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
              Dealer Info
              <address>
                  <strong><?= $model->dealerOutlet->customer_name ?></strong><br>
                  Dealer Invoice No. : <?= $model->dealer_invoice_no  ?><br>
                  Dealer Invoice (RM) : <?= $model->dealer_invoice_price  ?><br>
                  Dealer Invoice Date : <?= date('d-M-y', strtotime($model->dealer_invoice_date))  ?><br>
                  RM pay-out : <?= $model->pay_out_percentage  ?>%<br>
                <?= $model->dealerOutlet->address ?>
                <!--795 Folsom Ave, Suite 600<br>
                San Francisco, CA 94107<br>
                Phone: (804) 123-5432<br/>
                Email: info@almasaeedstudio.com-->
              </address>
            </div><!-- /.col -->
            <div class="col-sm-3 invoice-col">
              Painter Info
              <address>
                <strong><?= $model->profile->full_name ?></strong><br>  
                NRIC/Passport Number : <?= $model->profile->ic_no ?><br>
                Membership# : <?= $model->profile->card_id ?><br/>
                <br/>
              </address>
            </div><!-- /.col -->
            <div class="col-sm-3 invoice-col">
              <address>
                <br/>
              <?php
              $banks = common\models\Banks::find()->where(['id' => $model->bank->bank_name])->one();
              $bankname = $banks->bank_name;
              ?>
              <b>Bank Name:</b> <?= $bankname ?><br/>
              <b>Account Name:</b> <?= $model->bank->account_name ?><br/>
              <b>Account #:</b> <?= $model->bank->account_number ?>

              </address>
            </div><!-- /.col -->
            <div class="col-sm-2 invoice-col">
                <?php
                if(!empty($model->order_status)){
                    if($model->order_status == 1){
                        $sts = '<span class="label label-warning">Pending</span>';
                    }else if($model->order_status == 7){
                        $sts = '<span class="label label-danger">Canceled</span>';
                    }else if($model->order_status == 17){
                        $sts = '<span class="label label-success">Approved</span>';
                    }else{
                       $orderaction = common\models\OrderStatus::find()->where(['order_status_id' => $model->order_status])->one();
                       $sts = $orderaction->name;
                    }
                    echo '<h3 style="margin-top: 0px;">'.$sts.'</h3>';
                }
                ?>
                
                <b>Redemption Info</b><br>
              <?php
              $editchange = 0;
              $chkredemcount = \common\models\RedemptionItems::find()->where(['order_id' => $model->order_id])->count();
              if($chkredemcount > 0) {
                  $chkredem = \common\models\RedemptionItems::find()->where(['order_id' => $model->order_id])->one();
                  echo $chkredem->redemption->redemption_invoice_no.'<br>';
                  echo $chkredem->redemption->orderStatus2->name;
                  if($chkredem->redemption->orderStatus2->order_status_id == 1){
                      $editchange = 1;
                  }else {
                      $editchange = 0;
                  }
              }else {
                  echo 'N/A';
              }
              ?>
            </div><!-- /.col -->
          </div><!-- /.row -->

          <!-- Table row -->
          <div class="row">
            <div class="col-xs-12 table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Product Description</th>
                    <th class="text-center">Qty</th>
                    <th class="text-center">Points / can</th>
                    <th class="text-center">Total Points Awarded</th>
                    <th class="text-center">Value (RM) / can</th>
                    <th class="text-center">Total Value (RM)</th>
                    <th class="text-center">Status</th>
                  </tr>
                </thead>
                <tbody>
                    <?php
                    $orderitems = \common\models\PointOrderItem::find()->where(['point_order_id' => $model->order_id])->all();
                    $n = 1;
                    foreach($orderitems as $orderitem){
                        $perproduct = \common\models\ProductList::find()->where(['product_list_id' => $orderitem->product_list_id])->one();
                        echo '<tr>
                            <td>'.$n.'</td>
                            <td>'.$perproduct->product_description.'</td>    
                            <td class="text-center">'.$orderitem->total_qty.'</td>
                            <td class="text-center">'.$orderitem->item_bar_total_point.'</td>
                            <td class="text-center">'.$orderitem->total_qty_point.'</td>
                            <td class="text-center">'.$orderitem->item_bar_total_value.'</td>
                            <td class="text-center">'.$orderitem->total_qty_value.'</td>
                            <td class="text-center">'.$orderitem->statusDescription.'</td>     
                          </tr>';
                        $n++;
                    }
                    ?>
                
                </tbody>
              </table>
            </div><!-- /.col -->
          </div><!-- /.row -->

          <div class="row">
            <!-- accepted payments column -->
            <div class="col-xs-6">
              <p class="lead">Remark:</p>
              <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                <?= $model->order_remarks; ?>
              </p>
              
              
            </div><!-- /.col -->
            <div class="col-xs-6">
              <?php
              $total_points = \common\models\PointOrderItem::find()->where(['point_order_id' => $model->order_id,'Item_status' => 'G'])->sum('total_qty_point');
              $total_price = \common\models\PointOrderItem::find()->where(['point_order_id' => $model->order_id,'Item_status' => 'G'])->sum('total_qty_value');
              ?>
              <p class="lead">Awarded </p>
              <div class="table-responsive">
                <table class="table align-right">
                  <!--<tr>
                    <th style="width:50%">Subtotal:</th>
                    <td>$250.30</td>
                  </tr>
                  <tr>
                    <th>Tax (9.3%)</th>
                    <td>$10.34</td>
                  </tr>-->
                  <tr>
                    <th>Total Points:</th>
                    <td><?= $total_points ?></td>
                  </tr>
                  <tr>
                    <th>Total RM:</th>
                    <td><?= $total_price ?></td>
                  </tr>
                </table>
              </div>
            </div><!-- /.col -->
          </div><!-- /.row -->

          <!-- this row will not appear when printing -->
          <div class="row no-print">
            <div class="col-xs-12">
              <a href="invoice-print.html" target="_blank" class="hide btn btn-default"><i class="fa fa-print"></i> Print</a>
              <?php
                if($model->order_status == 1){
                    echo '<button class="btn btn-success pull-right" onclick="updateGuard('.$model->order_id.');return false;">Approval Action</button>';
                }else {
                    if($chkredemcount > 0) {
                        if($editchange == 1){
                            echo '<button class="btn btn-success pull-right" onclick="updateGuard('.$model->order_id.');return false;"> Approval Action</button>';
                        }else {
                            echo "<h3><span class='label label-danger pull-right'>Sorry, you can't change the status of this transaction.</span></h3>";
                        }
                    }else {
                        echo '<button class="btn btn-success pull-right" onclick="updateGuard('.$model->order_id.');return false;">Approval Action</button>';
                    }

                }
              ?>
              <?= Html::a('Back', ['index'], ['class' => 'btn btn-default']) ?>
              <button class="hide btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button>
            </div>
          </div>
        </section><!-- /.content -->
<?php
	yii\bootstrap\Modal::begin([
		'id' => 'guardModal',
		'header' => "<h4 class='modal-title'><i class='fa fa-edit'></i> Action </h4>",
	]);
 	yii\bootstrap\Modal::end(); 
?>

<script>
/***
  * Start Update Gardian Jquery
***/
function updateGuard(id) {
	$.ajax({
	  type:'GET',
	  url:'<?= Url::toRoute(["/management/pointorder/orderapprove"]) ?>',
	  data: { id : id},
	  success: function(data)
		   {
		       $(".modal-content").addClass("row");
		       $('.modal-body').html(data);
		       $('#guardModal').modal();

		   }
	});
}
</script>        
