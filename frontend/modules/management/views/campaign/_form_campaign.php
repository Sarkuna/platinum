<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use common\models\ProductList;

/* @var $this yii\web\View */
/* @var $model common\models\Campaign */
/* @var $form yii\widgets\ActiveForm */
$d_product_name = ArrayHelper::map(ProductList::find()->orderBy('product_name ASC')->groupBy(['product_name'])->all(), 'product_name', 'product_name');
$d_product_description = ArrayHelper::map(ProductList::find()->orderBy('product_description ASC')->all(), 'product_list_id', 'product_description');
?>
<div class="col-md-12">
    <div class="box box-primary campaign-form">

        <?php $form = ActiveForm::begin(); ?>
        <div class="box-body">

            <div class="row">
                <div class="col-md-4">
                    <?=
                    $form->field($campaign_assign, 'product_name')->dropDownList($d_product_name, [
                        'prompt' => '-- Select --',
                        'onchange' => '$.get( "' . Url::toRoute('/management/pointorder/pdescription') . '", {id: $(this).val() } )
                                 .done(function( data ) {
                                    $("#campaignassignproductsform-product_description").html(data);
                                 }
                             );'
                            ]
                    )
                    ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($campaign_assign, 'product_description')->dropDownList($d_product_description, ['prompt' => '-- Select --']) ?>
                </div>
                <div class="col-md-2">                    
                    
                    <?= $form->field($campaign_assign, 'new_points')->textInput(['maxlength' => true])->widget(\yii\widgets\MaskedInput::className(), [
                            //'mask' => '9999999999999',
                            'mask' => '99',
                            //'clientOptions' => ['alias' =>  'decimal','groupSeparator' => ',',],
                            'options' => ['placeholder' => 'XX', 'class' => 'form-control text-right',],
                        ]) ?> 
                </div>
                <div class="col-md-1" style="padding-left: 0px;">
                    <label class="control-label" for="pointorder-vqtybtn" style="height: 18px;display: block;"></label>
                    <input type="button" id="addButton" class="addButtonc btn btn-primary" value="SAVE" />
                </div>
                
            </div>            
        </div>
        <div class="box-footer">
            
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

    <?php
    $campaignid = $_GET['id'];
    $script = <<<EOD
                var handledCount = 0;
                $('.addButtonc').unbind('click').click(function() {
                    var test = $("#campaignassignproductsform-product_description").val();
                    //alert(test);
                    var flag = 0;
                    $("#retc").find("tr").each(function () {
                        var td1 = $(this).find("td:eq(0)").text();
                        if (test == td1) {
                            flag = 1;
                        }
                    });
            
                    if (handledCount == 0){
                        var itemID = $("#campaignassignproductsform-product_description").val(),vNode  = $(this);
                        var qty = $("#campaignassignproductsform-new_points").val();
                        var sum = 0.0;
                        $.ajax({
                            url:'getitem?id='+itemID+'&qty='+qty+'&campaignid='+$campaignid,
                            dataType: 'json',
                            success : function(data) {
                                //alert(data);
                                if (data == null) {
                                    alert("Item already exists");
                                }else if (data == 'conflicts') {
                                    alert("This product is already being used in another campaign");
                                }else{
                                    if (flag == 1) {
                                        alert('Item already exists');
                                        $("#add_item").val('');
                                        $("#add_item").focus();
                                        $("#campaignassignproductsform-new_points").val('');
                                    } else {
                                        $('#retc tbody').append("<tr id="+ data.barcodeid + "><td class='hide'>" + data.barcodeid+ "</td><td></td><td>" + data.product_name + "</td><td class='text-center'>" + data.product_description+ "</td><td class='text-center'>" + data.pack_size+ "</td><td class='text-center price'>" + data.points_per_liter+ "</td><td class='text-center'>" + data.new_points_per_liter+ "</td><td class='text-center'>" + data.total_points_awarded+ "</td><td class='text-center'>" + data.total_value_rm+ "</td><td class='text-center'><a data-confirm='Are you sure to delete this item?' href='javascript::;' class='btnDelete' id="+ data.barcodeid + "><i class='fa fa-trash-o' style='opacity:0.5; filter:alpha(opacity=50);'></i></a></td></tr>");
                                        $("#add_item").val('');
                                        $("#add_item").focus();
                                        var count = $('#retc tbody').children('tr').length;
                                        //update_amounts();
                                        //update_percentage();
                                        //$("#totalitem").html(count);
                                        //$("#totalqty").val(count);
                                        $("#campaignassignproductsform-new_points").val('');
                                    }
                                }
                            },
                            error : function(errorThrown) {
                                //console.log('error');
                                alert(errorThrown);
                                alert("There is an error with AJAX!");
                            }
                        });
                    }
                    handledCount++;
                    if (handledCount == 1)
                        handledCount = 0;
                    return false;
                    $("#add_item").val('');
                });

            function update_amounts()
            {
                var sum = 0.00;
                $('#retc > tbody  > tr').each(function() {
                    var price = $(this).find('.price').html();
                    sum = sum + Number(price);
                });
                $("#grandtotal").val(sum.toFixed(2));
            }
            function update_percentage()
            {
                var priceOne = parseFloat($('#pointorder-dealer_invoice_price').val());
                var priceTwo = parseFloat($('#grandtotal').val());
                var price3 = priceTwo / priceOne * 100;
                $('#pointorder-pay_out_percentage').val(price3.toFixed(2)); // Set the rate
            }
            
            
            $('#pointorder-dealer_invoice_price').on('input',function(e){
                update_percentage();
            });
            
                        
            $("#retc").on('click','.btnDelete',function(){
                var choice = confirm($(this).attr('data-confirm'));
                if (choice) {
                    //$(this).closest('tr').remove();
                    var tableRow = $(this).closest('tr');
                    var del_id = $(this).closest('tr').attr('id'); // table row ID 
                    tableRow.find('td').fadeOut('fast', 
                        function(){ 
                            tableRow.remove();
                            $.ajax({
                                type:'POST',
                                url:'ajax-delete',
                                data: {delete_id : del_id},
                                success:function(data) {
                                    if(data == "YES") {
                                        rowElement.fadeOut(500).remove();
                                    }else {

                                    } 
                                }
                            });                        
                        }
                    );
                }
                return false;
            });

            
            $('#btnsubmit').on('click', function() {
                if($("#product_item_list tr").length > 0){
                    return true;
                }else{
                    alert('Please ensure there is at least one row in product list table');
                    return false;
                }
            });

EOD;
$this->registerJs($script);
?>

