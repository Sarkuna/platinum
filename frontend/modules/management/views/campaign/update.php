<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Campaign */

$this->title = 'Update Campaign: ' . $model->campaign_description;
$this->params['breadcrumbs'][] = ['label' => 'Campaigns', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->campaign_description, 'url' => ['view', 'id' => $model->campaign_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="col-xs-12">
  <div class="col-lg-12 col-sm-4 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-plus"></i> <?= Html::encode($this->title) ?></h3></div>
</div>
<div class="campaign-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
