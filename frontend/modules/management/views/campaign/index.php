<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CampaignSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Campaigns';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-xs-12">
    <div class="box">
        <div class="box-header with-border">
            <div class="col-lg-8 col-sm-10 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-th-list"></i> <?= Html::encode($this->title) ?></h3></div>
            <div class="col-lg-4 col-sm-2 col-xs-12 text-right no-padding">
                <div class="col-xs-12 no-padding">
                    <?php if (Yii::$app->user->can('/management/campaign/create')) { ?>
                        <?= Html::a('New Campaign', ['create'], ['class' => 'btn btn-success']) ?>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="box-body table-responsive">
            <div class="campaign-index">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        //'campaign_id',
                        'campaign_description:ntext',
                        'campaign_rm_per_point',
                        'campaign_start_date',
                        'campaign_end_date',
                        // 'campaign_status',
                        [
                            'attribute' => 'campaign_status',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->getCampaignStatus();
                            },
                            'options' => ['width' => '60'],
                        ], 
                        //['class' => 'yii\grid\ActionColumn'],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{view} {update}', //{update} {delete}
                            'buttons' => [
                                'view' => function ($url, $model) {
                                    return (Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('app', 'View'),]));
                                },
                                'update' => function ($url, $model) {
                                    return (Html::a('<span class="fa fa-pencil-square-o"></span>', $url, ['title' => Yii::t('app', 'Edit'),]));
                                    
                                },        
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>

    </div>
</div>
