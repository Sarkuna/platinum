<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Campaign */

$this->title = $model->campaign_id;
$this->params['breadcrumbs'][] = ['label' => 'Campaigns', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
 <section class="invoice">
     <div class="row">
         <div class="col-xs-12">
             <h2 class="page-header">
                 <i class="fa fa-globe"></i> <b>Campaign - <?= $model->campaign_description?></b>
             </h2>
         </div><!-- /.col -->
     </div>
     <div class="row invoice-info">
         
     </div>
<div class="campaign-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->campaign_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->campaign_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'campaign_id',
            'campaign_description:ntext',
            'campaign_rm_per_point',
            'campaign_start_date',
            'campaign_end_date',
            'campaign_status',
        ],
    ]) ?>

</div>
</section>
