<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Campaign */
/* @var $form yii\widgets\ActiveForm */
$productlist = \common\models\ProductList::find()->orderBy(['per_value_price' => SORT_DESC ])->groupBy('per_value_price')->one();
if($model->isNewRecord){
    $cur_per_rm = $productlist->per_value_price;
}else{
    $cur_per_rm = '';
}
?>
<div class="col-md-12">
    <div class="box box-primary campaign-form">

        <?php $form = ActiveForm::begin(); ?>
        <div class="box-body">
            <div class="col-md-5">
                <?= $form->field($model, 'campaign_description')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'campaign_short_code')->textInput(['maxlength' => true]) ?>
                <div class="row">
                    <div class="col-md-6">
                        <?=
                        $form->field($model, 'campaign_start_date')->widget(DatePicker::classname(), [
                            'options' => ['readOnly' => true, 'placeholder' => 'From date ...'],
                            'removeButton' => false,
                            'pluginOptions' => [
                                'autoclose' => true,
                                'todayHighlight' => false,
                                'todayBtn' => false,
                                'format' => 'yyyy-mm-dd',
                                //'startDate' => $startDate,
                            ],
                            'pluginEvents' => [
                                'changeDate' => 'function(ev) {
                                    var sdat = $("#campaign-campaign_start_date").val();                    
                                    var bDate = sdat.toString("dd-MM-yy");
                                    $("#campaign-campaign_end_date-kvdate").kvDatepicker("setStartDate", bDate);
                                }'
                            ]
                        ]);

                        ?>
                    </div>
                    <div class="col-md-6">
                        <?=
                        $form->field($model, 'campaign_end_date')->widget(DatePicker::classname(), [
                            'options' => ['readOnly' => true, 'placeholder' => 'To date ...'],
                            'removeButton' => false,
                            'pluginOptions' => [
                                'autoclose' => true,
                                'todayHighlight' => false,
                                'todayBtn' => false,
                                'format' => 'yyyy-mm-dd',
                                //'minDate' => 'tenancy_period_from',
                                //'endDate' => date('#newtenantform-tenancy_period_from'), 
                                //'startDate' => date('#newtenantform-tenancy_period_from'),
                            ]
                        ]);
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">                        
                        <?= $form->field($model, 'campaign_rm_per_point')->textInput(['maxlength' => true])->widget(\yii\widgets\MaskedInput::className(), [
                            //'mask' => '9999999999999',
                            //'mask' => '99',
                            'clientOptions' => ['alias' =>  'decimal','groupSeparator' => ',',],
                            'options' => ['placeholder' => $cur_per_rm, 'class' => 'form-control text-right',],
                        ]) ?> 
                    </div>
                    <div class="col-md-6">
                        <?php echo $form->field($model, 'campaign_status')->dropDownList(['A' => 'Active', 'D' => 'Deactive']); ?>
                    </div>
                </div>
                
                
            </div>
            <div class="col-md-7">
                
                
            </div>            
        </div>
        <div class="box-footer">
            <?= Html::submitButton($model->isNewRecord ? 'Next' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

