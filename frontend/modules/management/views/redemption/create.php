<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Redemption */

$this->title = 'Create Redemption';
$this->params['breadcrumbs'][] = ['label' => 'Redemptions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="redemption-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
