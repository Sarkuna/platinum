<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel common\models\RedemptionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Points Redemption Management';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-12">
<div class="box">
    <div class="box-header with-border">
        <div class="col-lg-8 col-sm-10 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-th-list"></i> <?= Html::encode($this->title) ?></h3></div>
        <div class="col-lg-4 col-sm-2 col-xs-12 text-right no-padding">

        </div>
    </div>
    <div class="box-body table-responsive">
        <div class="redemption-index">
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    //'redemptionID',
                    'redemption_invoice_no',
                    //'profile.card_id',
                    
                    //'painterID',
                    [
                        'attribute' => 'card_id',
                        'label' => 'Membership No',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '95'],
                        'value' => function ($model) {
                            return $model->profile->card_id;
                        },
                    ],
                    [
                        'attribute' => 'full_name',
                        'label' => 'Painter Name',
                        'format' => 'html',
                        'headerOptions' => ['width' => '195'],
                        'value' => function ($model) {
                            return $model->profile->full_name;
                        },
                    ],
                    [
                        'attribute' => 'ic_no',
                        'label' => 'NRIC/Passport Number',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '180'],
                        'value' => function ($model) {
                            return $model->profile->ic_no;
                        },
                    ],            
                    //'profile.ic_no',            
                    /*[
                        'attribute' => 'painterID',
                        'label' => 'NRIC/PP',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '95'],
                        'value' => function ($model) {
                            return $model->profile->ic_no;
                        },
                    ], */           
                    //'orderID:ntext',
                    //'req_points',
                    [
                        'attribute' => 'total_transactions',
                        'label' => 'Total Transactions',
                        'format' => 'html',
                        'headerOptions' => ['width' => '50', 'class' => 'text-center'],
                        'contentOptions' =>['class' => 'text-right'],
                        'value' => function ($model) {
                            return $model->getTotalTransactions();
                        },
                    ],            
                    [
                        'attribute' => 'req_points',
                        'label' => 'Total Points',
                        'format' => 'html',
                        'headerOptions' => ['width' => '60', 'class' => 'text-center'],
                        'contentOptions' =>['class' => 'text-right'],
                        'value' => function ($model) {
                            return $model->req_points;
                        },
                    ],            
                    
                    [
                        'attribute' => 'req_amount',
                        'label' => 'Total RM Amount',
                        'format' => 'html',
                        'headerOptions' => ['width' => '100', 'class' => 'text-center'],
                        'contentOptions' =>['class' => 'text-right'],
                        'value' => function ($model) {
                            return $model->req_amount;
                        },
                    ],
                    [
                        'attribute' => 'redemption_status_ray',
                        'label' => 'Status',
                        'format' => 'html',
                        'headerOptions' => ['width' => '95'],
                        'value' => function ($model) {
                            //$ord =
                            return $model->orderStatus2->name;
                        },
                        'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],        
                        'filter' => ["2" => "Processing", "8" => "Denied", "10" => "Failed", "19" => "Paid"],        
                    ],             
                    //'req_amount',
                    //'redemption_status',
                    // 'redemption_remarks',
                    // 'redemption_IP',
                    //'redemption_created_datetime',
                    /*[
                        'attribute' => 'redemption_created_datetime',
                        //'format' => ['raw', 'Y-m-d H:i:s'],
                        'format' =>  ['date', 'php:d-m-Y h.i A'],
                        'options' => ['width' => '200']
                    ],*/
                    
                    [
                        'attribute' => 'redemption_created_datetime',
                        'value' => 'redemption_created_datetime',
                        //'format' => ['php:D, d-M-Y H:i:s A'],
                        //'format' => 'datetime',
                        'format' =>  ['date', 'php:d-m-Y h.i A'],
                        'filter' => DatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'redemption_created_datetime',
                            'convertFormat' => true,
                            'removeButton' => false,
                            //'displayFormat' => 'php:D, d-M-Y H:i:s A',
                            'pluginOptions' => [
                                'format' => 'yyyy-M-dd'
                                //'format' => 'dd-M-yyyy'
                            ],
                        ]),
                        'options' => ['width' => '150']
                    ],             
                    // 'redemption_created_by',
                    // 'redemption_updated_datetime',
                    // 'redemption_updated_by',
                    //['class' => 'yii\grid\ActionColumn'],
                    [
                        'attribute' => 'action',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->getActions2();
                        },
                        'options' => ['width' => '60'],
                    ],            
                ],
            ]);
            ?>
        </div>
    </div>
</div>
</div>

