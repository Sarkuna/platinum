<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use common\models\TitleOptions;
use common\models\Race;
use common\models\Banks;
use common\models\Country;
use common\models\DealerList;

/* @var $this yii\web\View */
/* @var $model app\modules\painter\models\PainterProfile */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    #redemption-req_points {font-size: 28px;}
    .login-box-body{padding: 5px 10px;}
</style>

<div class="Redemption-Request-form">
            
            <?php $form = ActiveForm::begin(); ?>
            <?= $form->field($model, 'orderID')->hiddenInput()->label(false) ?>
            <div class="table-responsive">
            <table class="table no-margin">
                <thead>
                    <tr>
                        <th class="hide"></th>
                        <th>Date</th>
                        <th>Membership No</th>
                        <th>Painter Name</th>
                        <th>Order ID</th>
                        <!--<th>Dealer</th>-->
                        <th class="text-right">RM Values</th>
                        <th class="text-right">Points</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $count = count($myorders);
                    $trmsg = '0';

                    if ($count > 0) {
                        foreach ($myorders as $myorder) {
                            $chkorderid = \common\models\RedemptionItems::find()
                                    ->where("order_id = ".$myorder->order_id."")
                                    ->count();
                            
                            $trTotal = \common\models\PointOrderItem::find()
                                ->where("point_order_id = ".$myorder->order_id." AND Item_status = 'G'")
                                ->sum('total_qty_point');

                                $trTotalsum = \common\models\PointOrderItem::find()
                                ->where("point_order_id = ".$myorder->order_id." AND Item_status = 'G'")
                                ->sum('total_qty_value');
                                
                                $mychk = '<input type="checkbox" class="check" value="' . $trTotal . '" rel="' . $trTotalsum . '">';
                                
                            //if($chkorderid == 0){
  
                                //$status = '<span class="label label-' . $lbg . '">' . $myorder->orderStatus->name . '</span>';
                                //$ahrf = '<a href="management/pointorder/myview?id=' . $myorder->order_id . '"><span class="glyphicon glyphicon-search"></span></a>';
                                echo '<tr>';
                                echo '<td class="hide">' . $myorder->order_id . '</td>';
                                echo '<td width="100">' . date('d-m-Y', strtotime($myorder->created_datetime)) . '</td>';
                                echo '<td>' . $myorder->profile->card_id . '</td>';
                                echo '<td>' . $myorder->profile->full_name . '</td>';
                                echo '<td>' . $myorder->order_num . '</td>';
                                //echo '<td>' . $productoverview->dealerOutlet->customer_name . '</td>';
                                echo '<td class="text-right">' . $myorder->order_total_amount. '</td>';
                                echo '<td class="text-right">' . $trTotal . '</td>';
                                echo '<td class="text-center">' . $mychk. '</td>';
                                //echo '<td>' . $ahrf . '</td>';
                                echo '</tr>';
                                //$trmsg = '1';
                            //}
                        }
                    }else{
                    echo '<tr><td colspan="4" class="text-center">No records found</td></tr>';
                    }
                    ?>
                </tbody>
            </table>
        </div><!-- /.table-responsive -->
        <?php
        if ($count > 0) {
        ?>
        <div class="row">
                <div class="col-lg-4 col-lg-offset-8">
                    <?= $form->field($model, 'req_points')->textInput(['readonly' => true,])->label(false) ?>
                    <?= $form->field($model, 'req_amount')->hiddenInput()->label(false) ?>
                </div>
            
        <div class="col-lg-12">
            <div class="form-group pull-right">
                <?= Html::submitButton('Request', ['class' => 'btn btn-success']) ?>
            </div>
        </div>
        </div>
        <?php
        }
        ?>
      <?php ActiveForm::end(); ?> 
        </div>

<?php
    $clientScript = '
        $("input[type=checkbox]").change(function () {
            recalculate();
        });
    ';
    $this->registerJs($clientScript, \yii\web\View::POS_END, 'booking-period');
    ?>
    <script language="javascript">  
    function recalculate() {
        
        var sum = 0;
        var sumtot = 0.00;
        var a = "";
        $('.check').each(function () {            
            if (this.checked) sum = sum + parseFloat($(this).val());
            if (this.checked) sumtot = sumtot + parseFloat($(this).attr('rel'));
            if ((this).checked) {
                if (a.length == 0) {
                    a = $(this).closest("tr").find("td:first").text();
                } else {
                    a = a + "," + $(this).closest("tr").find("td:first").text();
                }
            } else {
                if (a.length == 1) {
                    a = a.slice(0, 1);
                } else {
                    a = a.replace(("," + $(this).closest("tr").find("td:first").text()), "");
                }
            }
        });        
        $("#redemption-orderid").val(a); 
        $("#output").text(sum);
        $("#redemption-req_points").val(sum);
        $("#redemption-req_amount").val(sumtot.toFixed(2));
        $("#multiamt").val(sum);
        
    }
    function checkempty() {
        var minamt = $("#csvsendpayment-csv_amount").val(); 
        if(minamt == '0.00' || minamt == '0' || minamt == ''){
            alert('Minimum Ammount Required above 0.00');
            return false;
        }else{
            return true;
        }
        
    }
//});
    </script> 
