<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Redemption */

$this->title = 'Update Redemption: ' . $model->redemptionID;
$this->params['breadcrumbs'][] = ['label' => 'Redemptions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->redemptionID, 'url' => ['view', 'id' => $model->redemptionID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="redemption-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
