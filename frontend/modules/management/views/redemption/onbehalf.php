<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel common\models\RedemptionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Redemption on-behalf';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-12">
<div class="box">
    <div class="box-header with-border">
        <div class="col-lg-8 col-sm-10 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-th-list"></i> <?= Html::encode($this->title) ?></h3></div>
        <div class="col-lg-4 col-sm-2 col-xs-12 text-right no-padding">

        </div>
    </div>
    <div class="box-body table-responsive">
        <div class="redemption-index">
            <?= $this->render('_form_redemption_request', [
                    'model' => $model,
                    'myorders' => $myorders,
            ]) ?>
        </div>
    </div>
</div>
</div>

