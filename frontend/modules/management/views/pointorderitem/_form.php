<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PointOrderItem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="point-order-item-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'point_order_id')->textInput() ?>

    <?= $form->field($model, 'painter_login_id')->textInput() ?>

    <?= $form->field($model, 'item_bar_code')->textInput() ?>

    <?= $form->field($model, 'item_bar_point')->textInput() ?>

    <?= $form->field($model, 'item_bar_total_point')->textInput() ?>

    <?= $form->field($model, 'item_bar_total_value')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Item_status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Item_IP')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_datetime')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'updated_datetime')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
