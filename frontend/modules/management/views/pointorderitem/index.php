<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PointOrderItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Point Order Items';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="point-order-item-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Point Order Item', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'item_id',
            'point_order_id',
            'painter_login_id',
            'item_bar_code',
            'item_bar_point',
            // 'item_bar_total_point',
            // 'item_bar_total_value',
            // 'Item_status',
            // 'Item_IP',
            // 'created_datetime',
            // 'created_by',
            // 'updated_datetime',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
