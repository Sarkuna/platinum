<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PointOrderItem */

$this->title = 'Create Point Order Item';
$this->params['breadcrumbs'][] = ['label' => 'Point Order Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="point-order-item-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
