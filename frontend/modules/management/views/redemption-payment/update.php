<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PayOutSummary */

$this->title = 'Update Pay Out Summary: ' . $model->pay_out_summary_id;
$this->params['breadcrumbs'][] = ['label' => 'Pay Out Summaries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pay_out_summary_id, 'url' => ['view', 'id' => $model->pay_out_summary_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pay-out-summary-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
