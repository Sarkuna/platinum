<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\PayOutSummary */

$this->title = $model->pay_out_summary_id;
$this->params['breadcrumbs'][] = ['label' => 'Pay Out Summaries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pay-out-summary-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->pay_out_summary_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->pay_out_summary_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'pay_out_summary_id',
            'date_created',
            'description:ntext',
        ],
    ]) ?>

</div>

<div class="col-xs-12">
<!--    <div class="box">
        <div class="box-header with-border">
            <div class="col-lg-4 col-sm-4 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-th-list"></i></h3></div>
            <div class="col-xs-6"></div>
            <div class="col-lg-2 col-sm-2 col-xs-12 no-padding">
                <div class="col-xs-12 no-padding"></div>
            </div>
        </div>
        <div class="box-body table-responsive">
            <div class="painter-profile-index">-->
                
                <?php
                // Header and Footer options for PDF format
        $ourPdfHeader = [
            'L' => [
                'content'   => 'Summary View',
                'font-size' => 8,
                'color'     => '#333333'
            ],
            'C' => [
                'content'   => '',
                'font-size' => 16,
                'color'     => '#333333'
            ],
            'R' => [
                'content'   => 'Generated' . ': ' . date("D, d-M-Y g:i a T"),
                'font-size' => 8,
                'color'     => '#333333'
            ]
        ];
        $ourPdfFooter = [
            'L'    => [
                'content'    => '',
                'font-size'  => 8,
                'font-style' => 'B',
                'color'      => '#999999'
            ],
            'R'    => [
                'content'     => '[ {PAGENO} ]',
                'font-size'   => 10,
                'font-style'  => 'B',
                'font-family' => 'serif',
                'color'       => '#333333'
            ],
            'line' => TRUE,
        ];

        $exportFilename = date("Y-m-d_H-m-s").'_'.$this->title;

        $exportConfig = [

            /*GridView::CSV   => [
                'label'           => 'CSV',
                'icon'            => ' fa fa-file-code-o',
                'iconOptions'     => ['class' => 'text-primary'],
                'showHeader'      => TRUE,
                'showPageSummary' => TRUE,
                'showFooter'      => TRUE,
                'showCaption'     => TRUE,
                'filename'        => $exportFilename,
                'alertMsg'        => 'The CSV export file will be generated for download.',
                'options'         => ['title' => 'Comma Separated Values'],
                'mime'            => 'application/csv',
                'config'          => [
                    'colDelimiter' => ",",
                    'rowDelimiter' => "\r\n",
                ]
            ],*/
            GridView::EXCEL => [
                'label'           => 'Excel',
                'icon'            => ' fa fa-file-excel-o',
                'iconOptions'     => ['class' => 'text-success'],
                'showHeader'      => TRUE,
                'showPageSummary' => TRUE,
                'showFooter'      => TRUE,
                'showCaption'     => TRUE,
                'filename'        => $exportFilename,
                'alertMsg'        => 'The EXCEL export file will be generated for download.',
                'options'         => ['title' => 'Microsoft Excel 95+'],
                'mime'            => 'application/vnd.ms-excel',
                'config'          => [
                    'worksheet' => 'Worksheet',
                    'cssFile'   => ''
                ]
            ],
            /*GridView::PDF   => [
                'label'           => 'PDF',
                'icon'            => ' fa fa-file-pdf-o',
                'iconOptions'     => ['class' => 'text-danger'],
                'showHeader'      => TRUE,
                'showPageSummary' => TRUE,
                'showFooter'      => TRUE,
                'showCaption'     => TRUE,
                'filename'        => $exportFilename,
                'alertMsg'        => 'The PDF export file will be generated for download.',
                'options'         => ['title' => 'Portable Document Format'],
                'mime'            => 'application/pdf',
                'config'          => [
                    'mode'          => 'c',
                    'format'        => 'A4-L',
                    'destination'   => 'D',
                    'marginTop'     => 20,
                    'marginBottom'  => 20,
                    'cssInline'     => '.kv-wrap{padding:20px;}' .
                        '.kv-align-center{text-align:center;}' .
                        '.kv-align-left{text-align:left;}' .
                        '.kv-align-right{text-align:right;}' .
                        '.kv-align-top{vertical-align:top!important;}' .
                        '.kv-align-bottom{vertical-align:bottom!important;}' .
                        '.kv-align-middle{vertical-align:middle!important;}' .
                        '.kv-page-summary{border-top:4px double #ddd;font-weight: bold;}' .
                        '.kv-table-footer{border-top:4px double #ddd;font-weight: bold;}' .
                        '.kv-table-caption{font-size:1.5em;padding:8px;border:1px solid #ddd;border-bottom:none;}',
                    'methods'       => [
                        'SetHeader' => [
                            ['odd' => $ourPdfHeader, 'even' => $ourPdfHeader]
                        ],
                        'SetFooter' => [
                            ['odd' => $ourPdfFooter, 'even' => $ourPdfFooter]
                        ],
                    ],
                    'options'       => [
                        'title'    => 'Summary View',
                        'subject'  => 'PDF export',
                        'keywords' => 'pdf'
                    ],
                    'contentBefore' => '',
                    'contentAfter'  => ''
                ]
            ]*/
        ];
                $gridColumns = [
                        //['class' => 'yii\grid\SerialColumn'],
                        ['class' => 'kartik\grid\SerialColumn'],
                        //'item_id',
                        //'point_order_id',
                    [
                        'attribute' => 'card_id',
                        'label' => 'Membership #',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '95'],
                        'value' => function ($model) {
                            return $model->profile->card_id;
                        },
                    ],
                    /*[
                        'attribute' => 'full_name',
                        'label' => 'Full Name',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '250'],
                        'value' => function ($model) {
                            return $model->profile->full_name;
                        },
                    ],*/
                    [
                        'attribute' => 'account_name',
                        'label' => 'Full Name',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '50'],
                        'value' => function ($model) {
                            return $model->bank->account_name;
                        },
                    ],            
                    [
                        'attribute' => 'bank_name',
                        'label' => 'Bank Name',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '380'],
                        'value' => function ($model) {
                            $banks = common\models\Banks::find()->where(['id' => $model->bank->bank_name])->one();
                            $bankname = $banks->bank_name;
                            return $bankname;
                        },
                    ],
                                
                    [
                        'attribute' => 'account_number',
                        'label' => 'Bank Acc #',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '180'],
                        'value' => function ($model) {
                            return $model->bank->account_number;
                        },
                    ],            
                                
                    [
                        'attribute' => 'redemption_invoice_no',
                        'label' => 'Redemption',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '95'],
                        'value' => function ($model) {
                            return $model->redemption_invoice_no;
                        },
                    ],
                    
                    
                    /*[
                        'attribute' => 'ic_no',
                        'label' => 'NRIC/Passport #',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '150'],
                        'value' => function ($model) {
                            return $model->profile->ic_no;
                        },
                    ],
                    [
                        'attribute' => 'total_transactions',
                        'label' => 'Total Transactions',
                        'format' => 'html',
                        'headerOptions' => ['class' => 'text-center'],
                        'contentOptions' =>['class' => 'text-right'],
                        'value' => function ($model) {
                            return $model->getTotalTransactions();
                        },
                    ],*/          
                    /*[
                        'attribute' => 'req_points',
                        'label' => 'Total',
                        'format'=>'integer',
                        'value' => function ($model) {
                            return $model->req_points;
                        },
                        'contentOptions' =>['class' => 'text-right',],         
                        //'options' => ['width' => '80'],
                        'hAlign'=>'right',        
                        //'pageSummary' => $qtypoint,
                        'pageSummary' => TRUE,        
                    ],*/
                    [
                        'attribute' => 'req_amount',
                        'label' => 'Total',
                        'format' => ['decimal', 2],
                        'value' => function ($model) {
                            return $model->req_amount;
                        },
                        'contentOptions' =>['class' => 'text-right',],         
                        //'options' => ['width' => '80'],
                        'hAlign'=>'right',        
                        //'pageSummary' => 'RM '.Yii::$app->formatter->asDecimal($totbox6),
                        'pageSummary' => TRUE,
                    ],
                    [
                        'attribute' => 'mobile',
                        'label' => 'Mobile',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '180'],
                        'value' => function ($model) {
                            return $model->profile->mobile;
                        },
                    ],
                    [
                        'attribute' => 'account_no_verification',
                        'label' => 'Account Verified',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '180'],
                        'value' => function ($model) {
                            //return $model->bank->account_no_verification;
                            return $model->bank->account_no_verification == 'N' ? 'No':'Yes'; 
                        },
                    ],            
                    /*[
                        'attribute' => 'email',
                        'label' => 'Email',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '180'],
                        'value' => function ($model) {
                            return $model->user->email;
                        },
                    ],*/
                    
                    [
                        'attribute' => 'redemption_status_ray',
                        'label' => 'Status',
                        'format' => 'html',
                        'headerOptions' => ['width' => '95'],
                        'value' => function ($model) {
                            //$ord =
                            return $model->orderStatus2->name;
                        },      
                    ],            
                    
                    [
                        'attribute' => 'redemption_created_datetime',
                        'label' => 'Request Date',
                        'format'=>['date', 'php:d-m-Y'],//h:i:s
                        //'headerOptions' => ['width' => '180'],
                        'value' => function ($model) {
                            return $model->redemption_created_datetime;
                        },   
                    ],                                                       
                                    
                ];
                echo GridView::widget([
                    'dataProvider'=> $dataProvider,
                    //'filterModel' => $searchModel,
                    'autoXlFormat'=>true,
                    'columns' => $gridColumns,
                    'pjax' => true,
                    'bordered' => true,
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'hover' => true,
                    //'exportConfig'     => $exportConfig,
                    'export' => false,
                    'toolbar' =>  [
                        ['content'=>
                            //Html::button('<i class="fa fa-file-excel-o"></i> Export to Excel', ['my-action', 'type'=>'button', 'title'=>'hhh', 'class'=>'btn btn-success', 'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . '&nbsp;&nbsp;&nbsp;'.
                            Html::a(Yii::t('app', '<i class="fa fa-file-excel-o"></i> Export to Excel'), ['redemption-summary-excel'], ['class' => 'btn btn-primary',
                                'data' => [
                                    'confirm' => Yii::t('app', 'Are you sure you want to download?'),
                                    'method' => 'post',
                                ],
                            ]).
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['/management/report/redemptionsummery'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>'ggg'])
                        ],
                        //'{export}',
                        //'{toggleData}'
                    ],
                    'floatHeader' => false,
                    //'floatHeaderOptions' => ['scrollingTop' => $scrollingTop],
                    'showPageSummary' => true,
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY,
                        'heading' => '<i class="fa fa-th-list"></i> '.Html::encode($this->title).''
                    ],
                    
                ]);
                ?>
                
<!--            </div>
        </div>
    </div>-->
</div>
