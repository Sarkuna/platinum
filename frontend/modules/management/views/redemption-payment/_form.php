<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */
/* @var $model common\models\PayOutSummary */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .kv-drp-dropdown .kv-clear{font-size: 1.8rem;right: 1.8rem;line-height: 1.6rem;}
    .kv-drp-dropdown .left-ind, .kv-drp-dropdown .right-ind {top: 0.9rem;}
    .kv-drp-dropdown .left-ind {left: 6px;}
</style>
<div class="pay-out-summary-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'date_created')->widget(
        DateRangePicker::class, 
        [
            'options' => ['placeholder' => 'Select Redemption Created Datetime'],
            'presetDropdown'=>true,
    'convertFormat'=>true,
    'includeMonthsFilter'=>true,
            'convertFormat' => true,
            'pluginOptions' => [
                'locale' => ['format' => 'd-M-y'],
                'format' => 'd-M-Y g:i A',
                'startDate' => '01-Mar-2014 12:00 AM',
                'todayHighlight' => true
            ]
        ]
        );
    ?>



    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>