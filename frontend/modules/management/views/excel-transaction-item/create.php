<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ExcelTransactionItem */

$this->title = 'Create Excel Transaction Item';
$this->params['breadcrumbs'][] = ['label' => 'Excel Transaction Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="excel-transaction-item-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
