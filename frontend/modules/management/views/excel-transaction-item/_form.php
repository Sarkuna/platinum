<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ExcelTransactionItem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="excel-transaction-item-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'auto_transaction_id')->textInput() ?>

    <?= $form->field($model, 'transaction_num')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'transaction_status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList([ 'pending' => 'Pending', 'success' => 'Success', 'fail' => 'Fail', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
