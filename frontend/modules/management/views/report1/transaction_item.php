<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\painter\models\PainterProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Transaction Supporting Document';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php

$amount = 0;
$totbox6 = 0;
$qtypoint = 0;
if (!empty($dataProvider->getModels())) {
        foreach ($dataProvider->getModels() as $key => $val) {
            //$amount2 = $val->getTotalrmawarded() - $val->getTotal_rm_paid();
           // $amount += $amount2;
            //$totbox6 += $val->getTotalrmawarded();
            $totbox6 += $val->total_qty_value;
            $qtypoint += $val->total_qty_point;
        }
    }

    ?>
<div class="col-xs-12">
<!--    <div class="box">
        <div class="box-header with-border">
            <div class="col-lg-4 col-sm-4 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-th-list"></i></h3></div>
            <div class="col-xs-6"></div>
            <div class="col-lg-2 col-sm-2 col-xs-12 no-padding">
                <div class="col-xs-12 no-padding"></div>
            </div>
        </div>
        <div class="box-body table-responsive">
            <div class="painter-profile-index">-->
                
                <?php
                // Header and Footer options for PDF format
        $ourPdfHeader = [
            'L' => [
                'content'   => 'Summary View',
                'font-size' => 8,
                'color'     => '#333333'
            ],
            'C' => [
                'content'   => '',
                'font-size' => 16,
                'color'     => '#333333'
            ],
            'R' => [
                'content'   => 'Generated' . ': ' . date("D, d-M-Y g:i a T"),
                'font-size' => 8,
                'color'     => '#333333'
            ]
        ];
        $ourPdfFooter = [
            'L'    => [
                'content'    => '',
                'font-size'  => 8,
                'font-style' => 'B',
                'color'      => '#999999'
            ],
            'R'    => [
                'content'     => '[ {PAGENO} ]',
                'font-size'   => 10,
                'font-style'  => 'B',
                'font-family' => 'serif',
                'color'       => '#333333'
            ],
            'line' => TRUE,
        ];

        $exportFilename = date("Y-m-d_H-m-s").'_'.$this->title;

        $exportConfig = [

            /*GridView::CSV   => [
                'label'           => 'CSV',
                'icon'            => ' fa fa-file-code-o',
                'iconOptions'     => ['class' => 'text-primary'],
                'showHeader'      => TRUE,
                'showPageSummary' => TRUE,
                'showFooter'      => TRUE,
                'showCaption'     => TRUE,
                'filename'        => $exportFilename,
                'alertMsg'        => 'The CSV export file will be generated for download.',
                'options'         => ['title' => 'Comma Separated Values'],
                'mime'            => 'application/csv',
                'config'          => [
                    'colDelimiter' => ",",
                    'rowDelimiter' => "\r\n",
                ]
            ],            
            GridView::PDF   => [
                'label'           => 'PDF',
                'icon'            => ' fa fa-file-pdf-o',
                'iconOptions'     => ['class' => 'text-danger'],
                'showHeader'      => TRUE,
                'showPageSummary' => TRUE,
                'showFooter'      => TRUE,
                'showCaption'     => TRUE,
                'filename'        => $exportFilename,
                'alertMsg'        => 'The PDF export file will be generated for download.',
                'options'         => ['title' => 'Portable Document Format'],
                'mime'            => 'application/pdf',
                'config'          => [
                    'mode'          => 'c',
                    'format'        => 'A4-L',
                    'destination'   => 'D',
                    'marginTop'     => 20,
                    'marginBottom'  => 20,
                    'cssInline'     => '.kv-wrap{padding:20px;}' .
                        '.kv-align-center{text-align:center;}' .
                        '.kv-align-left{text-align:left;}' .
                        '.kv-align-right{text-align:right;}' .
                        '.kv-align-top{vertical-align:top!important;}' .
                        '.kv-align-bottom{vertical-align:bottom!important;}' .
                        '.kv-align-middle{vertical-align:middle!important;}' .
                        '.kv-page-summary{border-top:4px double #ddd;font-weight: bold;}' .
                        '.kv-table-footer{border-top:4px double #ddd;font-weight: bold;}' .
                        '.kv-table-caption{font-size:1.5em;padding:8px;border:1px solid #ddd;border-bottom:none;}',
                    'methods'       => [
                        'SetHeader' => [
                            ['odd' => $ourPdfHeader, 'even' => $ourPdfHeader]
                        ],
                        'SetFooter' => [
                            ['odd' => $ourPdfFooter, 'even' => $ourPdfFooter]
                        ],
                    ],
                    'options'       => [
                        'title'    => 'Summary View',
                        'subject'  => 'PDF export',
                        'keywords' => 'pdf'
                    ],
                    'contentBefore' => '',
                    'contentAfter'  => ''
                ]
            ],*/
            GridView::EXCEL => [
                'label'           => 'Excel',
                'icon'            => ' fa fa-file-excel-o',
                'iconOptions'     => ['class' => 'text-success'],
                'showHeader'      => TRUE,
                'showPageSummary' => TRUE,
                'showFooter'      => TRUE,
                'showCaption'     => TRUE,
                'filename'        => $exportFilename,
                'alertMsg'        => 'The EXCEL export file will be generated for download.',
                'options'         => ['title' => 'Microsoft Excel 95+'],
                'mime'            => 'application/vnd.ms-excel',
                'config'          => [
                    'worksheet' => 'Worksheet',
                    'cssFile'   => ''
                ]
            ],
        ];
                $gridColumns = [
                        //['class' => 'yii\grid\SerialColumn'],
                        //['class' => 'kartik\grid\SerialColumn'],
                        //'item_id',
                        //'point_order_id',
                        [
                            'attribute' => 'card_id',
                            'label' => 'Membership#',
                            'format'=>'text', 
                            //'format' => 'html',
                            'value' => function ($model) {
                                return $model->profile->card_id;
                            },
                            //'group'=>true,  // enable grouping
                            //'subGroupOf'=>1        
                        ],            
                        [
                            'attribute' => 'full_name',
                            'label' => 'Full Name',
                            'format' => 'html',
                            'headerOptions' => ['width' => '150'],
                            'value' => function ($model) {
                                return $model->profile->full_name;
                            },
                            //'group'=>true,  // enable grouping
                            //'subGroupOf'=>1        
                        ],
                        [
                            'attribute' => 'ic_no',
                            'label' => 'NRIC / Passport #',
                            'format' => 'html',
                            'headerOptions' => ['width' => '130'],
                            'value' => function ($model) {
                                return $model->profile->ic_no;
                            },
                            //'group'=>true,  // enable grouping
                            //'subGroupOf'=>1        
                        ],
                        [
                            'attribute' => 'order_num',
                            'label' => 'Transaction #',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->order->order_num;
                            },
                            //'group'=>true,  // enable grouping
                            //'subGroupOf'=>1
                        ],
                        /*[
                            'attribute' => 'painter_login_id',
                            'label' => 'Login ID #',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->order->painter_login_id;
                            },
                            'group'=>true,  // enable grouping
                            'subGroupOf'=>1
                        ],*/            
                        //'painter_login_id
                        [
                            'attribute' => 'order_dealer_id',
                            'label' => 'Dealer Name',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->order->dealerOutlet->customer_name;
                            },
                            //'group'=>true,  // enable grouping
                            //'subGroupOf'=>1        
                        ],
                        [
                            'attribute' => 'address',
                            'label' => 'Dealer Address',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->order->dealerOutlet->address;
                            },
                            ///'group'=>true,  // enable grouping
                            //'subGroupOf'=>1        
                        ],            
                        [
                            'attribute' => 'dealer_invoice_no',
                            'label' => 'Dealer Invoice #',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->order->dealer_invoice_no;
                            },
                            //'group'=>true,  // enable grouping
                            //'subGroupOf'=>1        
                        ],
                        [
                            'attribute' => 'dealer_invoice_price',
                            'label' => 'Dealer Invoice (RM)',
                            //'format' => 'html',
                            'format' => ['decimal', 2],
                            'value' => function ($model) {
                                return $model->order->dealer_invoice_price;
                            },
                                   
                            //'group'=>true,  // enable grouping
                            //'subGroupOf'=>1        
                        ],            
                        //'product_list_id',
                          //ProductListItem          
                        [
                            'attribute' => 'product_name',
                            'label' => 'Product Name',
                            'format' => 'html',
                            'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->productListItem->product_name;
                            },
                            //'group'=>true,  // enable grouping
                            //'subGroupOf'=>1         
                        ],
                        [
                            'attribute' => 'product_description',
                            'label' => 'Product Description',
                            'format' => 'html',
                            'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->productListItem->product_description;
                            },
                        ],            
                        [
                            'attribute' => 'total_qty',
                            'label' => 'Qty',
                            'format' => 'html',
                            'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->total_qty;
                            },
                        ], 
                        //'total_qty',            
                        //'total_qty_point',
                        //'total_qty_value',
                        [
                            'attribute' => 'total_qty_point',
                            'label' => 'Total Points',
                            'format'=>'integer',
                            'value' => function ($model) {
                                return $model->total_qty_point;
                            },
                            'contentOptions' =>['class' => 'text-right',],         
                            'options' => ['width' => '100'],
                            'hAlign'=>'right',        
                            'pageSummary' => $qtypoint,         
                        ],
                        [
                            'attribute' => 'total_qty_value',
                            'label' => 'Total RM award',
                            'format' => ['decimal', 2],
                            'value' => function ($model) {
                                return $model->total_qty_value;
                            },
                            'contentOptions' =>['class' => 'text-right',],         
                            'options' => ['width' => '100'],
                            'hAlign'=>'right',        
                            'pageSummary' => 'RM '.Yii::$app->formatter->asDecimal($totbox6),         
                        ],             
                        [
                            'attribute' => 'created_datetime',
                            'label' => 'Transaction Date',
                            'format'=>['datetime', 'php:d-M-y H:i:s'],
                            'value' => function ($model) {
                                return $model->order->created_datetime;
                            },
                            //'group'=>true,  // enable grouping
                            //'subGroupOf'=>1        
                        ],
                        [
                            'attribute' => 'order_id',
                            'label' => 'Redemption #',
                            //'format'=>['datetime', 'php:d-M-y H:i:s'],
                            'value' => function ($model) {
                                return $model->Redemption;//.'++++++'.$model->order->order_id;
                                //$oid=[$model->order->order_id];
                                //$rd = common\models\Redemption::find()->where('orderID IN('.$oid.')');
                                //print_r($rd);
                                //return $rd->redemption_invoice_no;
                            },
                            //'group'=>true,  // enable grouping
                            //'subGroupOf'=>1        
                        ],            
                        // 'order_status',
                        //'total_items',
          
           
                                    
                                   
                        // 'order_remarks:ntext',
                        // 'order_IP',
                        //'created_datetime',
                        /*[
                            'label' => 'Created Date',
                            'attribute' => 'created_datetime',
                            'headerOptions' => ['width' => '100'],
                            'format' => ['date', 'php:d-m-Y']
                        ],*/
                                    

 

                        //['class' => 'kartik\grid\CheckboxColumn'],            
                    ];
                echo GridView::widget([
                    'dataProvider'=> $dataProvider,
                    //'filterModel' => $searchModel,
                    'autoXlFormat'=>true,
                    'columns' => $gridColumns,
                    'pjax' => true,
                    'bordered' => true,
                    'striped' => false,
                    'condensed' => false,
                    'responsive' => true,
                    'hover' => true,
                    'exportConfig'     => $exportConfig,
                    'floatHeader' => false,
                    //'floatHeaderOptions' => ['scrollingTop' => $scrollingTop],
                    'showPageSummary' => true,
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY,
                        'heading' => '<i class="fa fa-th-list"></i> '.Html::encode($this->title).''
                    ],
                    
                ]);
                ?>
                
<!--            </div>
        </div>
    </div>-->
</div>