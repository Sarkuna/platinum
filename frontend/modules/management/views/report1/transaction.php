<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\painter\models\PainterProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Transaction Supporting Document';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php

$amount = 0;
$totbox6 = 0;

    ?>
<div class="col-xs-12">
<!--    <div class="box">
        <div class="box-header with-border">
            <div class="col-lg-4 col-sm-4 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-th-list"></i></h3></div>
            <div class="col-xs-6"></div>
            <div class="col-lg-2 col-sm-2 col-xs-12 no-padding">
                <div class="col-xs-12 no-padding"></div>
            </div>
        </div>
        <div class="box-body table-responsive">
            <div class="painter-profile-index">-->
                
                <?php
                // Header and Footer options for PDF format
        $ourPdfHeader = [
            'L' => [
                'content'   => 'Summary View',
                'font-size' => 8,
                'color'     => '#333333'
            ],
            'C' => [
                'content'   => '',
                'font-size' => 16,
                'color'     => '#333333'
            ],
            'R' => [
                'content'   => 'Generated' . ': ' . date("D, d-M-Y g:i a T"),
                'font-size' => 8,
                'color'     => '#333333'
            ]
        ];
        $ourPdfFooter = [
            'L'    => [
                'content'    => '',
                'font-size'  => 8,
                'font-style' => 'B',
                'color'      => '#999999'
            ],
            'R'    => [
                'content'     => '[ {PAGENO} ]',
                'font-size'   => 10,
                'font-style'  => 'B',
                'font-family' => 'serif',
                'color'       => '#333333'
            ],
            'line' => TRUE,
        ];

        $exportFilename = date("Y-m-d_H-m-s").'_'.$this->title;

        $exportConfig = [

            GridView::CSV   => [
                'label'           => 'CSV',
                'icon'            => ' fa fa-file-code-o',
                'iconOptions'     => ['class' => 'text-primary'],
                'showHeader'      => TRUE,
                'showPageSummary' => TRUE,
                'showFooter'      => TRUE,
                'showCaption'     => TRUE,
                'filename'        => $exportFilename,
                'alertMsg'        => 'The CSV export file will be generated for download.',
                'options'         => ['title' => 'Comma Separated Values'],
                'mime'            => 'application/csv',
                'config'          => [
                    'colDelimiter' => ",",
                    'rowDelimiter' => "\r\n",
                ]
            ],
            GridView::EXCEL => [
                'label'           => 'Excel',
                'icon'            => ' fa fa-file-excel-o',
                'iconOptions'     => ['class' => 'text-success'],
                'showHeader'      => TRUE,
                'showPageSummary' => TRUE,
                'showFooter'      => TRUE,
                'showCaption'     => TRUE,
                'filename'        => $exportFilename,
                'alertMsg'        => 'The EXCEL export file will be generated for download.',
                'options'         => ['title' => 'Microsoft Excel 95+'],
                'mime'            => 'application/vnd.ms-excel',
                'config'          => [
                    'worksheet' => 'Worksheet',
                    'cssFile'   => ''
                ]
            ],
            GridView::PDF   => [
                'label'           => 'PDF',
                'icon'            => ' fa fa-file-pdf-o',
                'iconOptions'     => ['class' => 'text-danger'],
                'showHeader'      => TRUE,
                'showPageSummary' => TRUE,
                'showFooter'      => TRUE,
                'showCaption'     => TRUE,
                'filename'        => $exportFilename,
                'alertMsg'        => 'The PDF export file will be generated for download.',
                'options'         => ['title' => 'Portable Document Format'],
                'mime'            => 'application/pdf',
                'config'          => [
                    'mode'          => 'c',
                    'format'        => 'A4-L',
                    'destination'   => 'D',
                    'marginTop'     => 20,
                    'marginBottom'  => 20,
                    'cssInline'     => '.kv-wrap{padding:20px;}' .
                        '.kv-align-center{text-align:center;}' .
                        '.kv-align-left{text-align:left;}' .
                        '.kv-align-right{text-align:right;}' .
                        '.kv-align-top{vertical-align:top!important;}' .
                        '.kv-align-bottom{vertical-align:bottom!important;}' .
                        '.kv-align-middle{vertical-align:middle!important;}' .
                        '.kv-page-summary{border-top:4px double #ddd;font-weight: bold;}' .
                        '.kv-table-footer{border-top:4px double #ddd;font-weight: bold;}' .
                        '.kv-table-caption{font-size:1.5em;padding:8px;border:1px solid #ddd;border-bottom:none;}',
                    'methods'       => [
                        'SetHeader' => [
                            ['odd' => $ourPdfHeader, 'even' => $ourPdfHeader]
                        ],
                        'SetFooter' => [
                            ['odd' => $ourPdfFooter, 'even' => $ourPdfFooter]
                        ],
                    ],
                    'options'       => [
                        'title'    => 'Summary View',
                        'subject'  => 'PDF export',
                        'keywords' => 'pdf'
                    ],
                    'contentBefore' => '',
                    'contentAfter'  => ''
                ]
            ]
        ];
                $gridColumns = [
                        //['class' => 'yii\grid\SerialColumn'],
                        //['class' => 'kartik\grid\SerialColumn'],
                        //'full_name',
                        [
                            'attribute' => 'card_id',
                            'label' => 'Membership#',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->profile->card_id;
                            },
                        ],            
                        [
                            'attribute' => 'full_name',
                            'label' => 'Full Name',
                            'format' => 'html',
                            'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->profile->full_name;
                            },
                        ],
                        [
                            'attribute' => 'full_name',
                            'label' => 'NRIC / Passport #',
                            'format' => 'html',
                            'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->profile->ic_no;
                            },
                        ],            
                        [
                            'attribute' => 'order_num',
                            'label' => 'Transaction #',
                            'format' => 'html',
                            'headerOptions' => ['width' => '110', 'class' => 'text-center'],
                            'value' => function ($model) {
                                return $model->order_num;
                            },
                        ],
                        //'order_dealer_id',
                        [
                            'attribute' => 'customer_name',
                            'label' => 'Dealer Info',
                            'format' => 'html',
                            'headerOptions' => ['width' => '200'],
                            'value' => function ($model) {
                                return $model->dealerOutlet->customer_name;
                            },
                        ],
                        [
                            'attribute' => 'dealer_invoice_no',
                            'label' => 'Dealer Invoice #',
                            'format' => 'html',
                            'headerOptions' => ['width' => '200'],
                            'value' => function ($model) {
                                return $model->dealer_invoice_no;
                            },
                        ],            
                        [
                            'attribute' => 'dealer_invoice_price',
                            'label' => 'Dealer Invoice (RM)',
                            'format' => 'html',
                            'headerOptions' => ['width' => '200'],
                            'value' => function ($model) {
                                return $model->dealer_invoice_price;
                            },
                        ],            
                        [
                            'attribute' => 'item_id',
                            'label' => 'Product Description',
                            'format' => 'html',
                            'headerOptions' => ['width' => '200'],
                            /*'value' => function($model) {
                                return implode(\yii\helpers\ArrayHelper::map($model->orderItems, 'item_id', 'product_list_id'));
                            },*/
                            'value' => function ($model) {
                                $str = '';
                                foreach($model->orderItems as $name) {
                                    $str .= ' ';
                                    //$str .= $name->id.'';
                                    $str .= $name->product_list_id.'<br>';
                                }
                                return $str;
                            },        
                            /*'value' => function ($model) {
                                return $model->orderItems->item_id;
                            },*/
                        ],
                        //'order_qty',
                        // 'order_total_point',
                        // 'order_total_amount',
                        // 'order_status',
                        //'total_items',
                        [
                            'attribute' => 'total_items',
                            //'label' => 'Total Transactions',
                            'format' => 'html',
                            'headerOptions' => ['width' => '60', 'class' => 'text-center'],
                            'contentOptions' =>['class' => 'text-right'],
                            'value' => function ($model) {
                                return $model->getTotalItems();
                            },
                        ],            
                        [
                            'attribute' => 'order_total_point',
                            'label' => 'Total Point',
                            'format' => 'html',
                            'headerOptions' => ['width' => '70', 'class' => 'text-center'],
                            'contentOptions' =>['class' => 'text-center'],
                            'value' => function ($model) {
                                return $model->order_total_point;
                            },
                        ],
                        [
                            'attribute' => 'order_total_amount',
                            'label' => 'Total Amount RM',
                            'format' => 'html',
                            'headerOptions' => ['width' => '100', 'class' => 'text-center'],
                            'contentOptions' =>['class' => 'text-right'],
                            'value' => function ($model) {
                                return $model->order_total_amount;
                            },
                        ],            
                                    
                                   
                        // 'order_remarks:ntext',
                        // 'order_IP',
                        //'created_datetime',
                        /*[
                            'label' => 'Created Date',
                            'attribute' => 'created_datetime',
                            'headerOptions' => ['width' => '100'],
                            'format' => ['date', 'php:d-m-Y']
                        ],*/
                                    
                        [
                            'attribute' => 'created_datetime',
                            'label' => 'Created Date',
                            'value' => 'created_datetime',
                            //'format' => ['php:D, d-M-Y H:i:s A'],
                            //'format' => 'datetime',
                            'format' =>  ['date', 'php:d-m-Y h.i A'],


                            'options' => ['width' => '150']
                        ],
 

                        //['class' => 'kartik\grid\CheckboxColumn'],            
                    ];
                echo GridView::widget([
                    'dataProvider'=> $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => $gridColumns,
                    'pjax' => true,
                    'bordered' => true,
                    'striped' => false,
                    'condensed' => false,
                    'responsive' => true,
                    'hover' => true,
                    'exportConfig'     => $exportConfig,
                    'floatHeader' => false,
                    //'floatHeaderOptions' => ['scrollingTop' => $scrollingTop],
                    'showPageSummary' => true,
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY,
                        'heading' => '<i class="fa fa-th-list"></i> '.Html::encode($this->title).''
                    ],
                    
                ]);
                ?>
                
<!--            </div>
        </div>
    </div>-->
</div>