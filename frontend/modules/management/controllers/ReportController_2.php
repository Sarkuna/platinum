<?php

namespace app\modules\management\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

use common\models\ReportPainterProfile;
use common\models\ReportPainterProfileSearch;
use common\models\PointOrderSearch;
use common\models\PointOrderItemSearch;
use common\models\RedemptionSearch;
use common\models\PainterProfile;
use common\models\PainterProfileSearch;

class ReportController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $searchModel = new ReportPainterProfileSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=100;
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionTransaction()
    {
        $searchModel = new PointOrderSearch();
        $searchModel->order_status = '17';
        $dataProvider = $searchModel->searchtra(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=100;
        return $this->render('transaction', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionTransactionitem()
    {
        $searchModel = new PointOrderItemSearch();
        //$searchModel->order_status = '17';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=100;
        return $this->render('transaction_item', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionRedemptionsd()
    {
        $searchModel = new RedemptionSearch();
        $searchModel->redemption_status_ray = ['2','19'];
        $dataProvider = $searchModel->searchray(Yii::$app->request->queryParams);

        return $this->render('payment_report', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionPainterprofilesummery()
    {
        $searchModel = new PainterProfileSearch();
        $searchModel->profile_status = ['A'];
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('painter_summary_report', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionRedemptionsummery()
    {
        $searchModel = new RedemptionSearch();
        $searchModel->redemption_status_ray = ['2'];
        $dataProvider = $searchModel->searchray(Yii::$app->request->queryParams);

        return $this->render('redemption_summary_report', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

}
