<?php

namespace app\modules\management\controllers;

use Yii;
use common\models\Campaign;
use common\models\CampaignSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;

use common\models\DealerList;
use common\models\ProductList;
use common\models\CampaignAssignProducts;

/**
 * CampaignController implements the CRUD actions for Campaign model.
 */
class CampaignController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Campaign models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CampaignSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Campaign model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $campaign_assign = new \common\models\CampaignAssignProductsForm();
        $assignproducts = CampaignAssignProducts::find()->where(['campaign_id' => $id])->all();
        return $this->render('view', [
            'assignproducts' => $assignproducts,
            'campaign_assign' => $campaign_assign,
            'model' => $this->findModel($id),
        ]);
    }
    
    public function actionGetitem($id=null,$qty=null,$campaignid=null)
    {
        //$response = null;
        $campaign = $this->findModel($campaignid);
        $qty = str_replace('_', '', $qty);
        if(empty($qty)){
           $qty = '1'; 
        }
        $campaign_start_date = date('Y-m-d', strtotime($campaign->campaign_start_date));
        $campaign_end_date = date('Y-m-d', strtotime($campaign->campaign_end_date));
        $sql = "SELECT * FROM campaign_assign_products WHERE campaign_start_date <= '$campaign_end_date' AND campaign_end_date >= '$campaign_start_date' AND product_list_id = $id";
        $check_conflicts = CampaignAssignProducts::findBySql($sql)->count();

        if($check_conflicts == 0){
            //$chkbarcode = ProductList::find()->where(['product_list_id' => $id])->count();
            $chkbarcode = CampaignAssignProducts::find()->where(['campaign_id' => $campaignid, 'product_list_id' => $id])->count();
            if($chkbarcode == 0){
                //$barcode = BarCodes::find()->where(['bar_code_name' => $id, 'bar_code_status' => 'A'])->one();
                //$bar_code_id = $barcode->bar_code_id;
                $product_list_id = $id;
                $productlist = ProductList::find()->where(['product_list_id' => $product_list_id])->one();

                //$chkcode = PointOrderItem::find()->where(['item_bar_code' => $bar_code_id])->count();

                $Item_status = 'Good';
                $totvalue = $productlist->pack_size * $qty;

                $CampaignAssignProducts = new CampaignAssignProducts();
                $CampaignAssignProducts->campaign_id = $campaignid;
                $CampaignAssignProducts->product_list_id = $id;
                $CampaignAssignProducts->new_points_per_l = $qty;
                $CampaignAssignProducts->campaign_start_date = $campaign->campaign_start_date;
                $CampaignAssignProducts->campaign_end_date = $campaign->campaign_end_date;
                $CampaignAssignProducts->save();

                $response['barcodeid'] = $CampaignAssignProducts->cap_id;
                $response['product_name'] = $productlist->product_name;
                $response['product_description'] = $productlist->product_description;
                $response['pack_size'] = $productlist->pack_size;
                $response['points_per_liter'] = $productlist->points_per_liter;
                $response['new_points_per_liter'] = $qty;
                $response['total_points_awarded'] = $productlist->pack_size * $qty;
                $response['total_value_rm'] = $totvalue * $campaign->campaign_rm_per_point;            
                $response['status']       = $Item_status;
            }else{
               $response = null; 
            }
        }else {
            $response = 'conflicts';
        }
        echo json_encode($response);
        /*echo Json::encode(array(
            'item_price'=>12, 
        ));*/
    }
    
    public function actionExcelimport($campaignid){
        $model = new \common\models\ImportExcelForm();
        $msg = '';


        if ($model->load(Yii::$app->request->post())) {
            $model->excel = UploadedFile::getInstance($model, 'excel');
            if ($model->excel && $model->validate()) {
                $rpath = realpath(Yii::$app->basePath);
                $model->excel->saveAs($rpath.'/web/upload/excelfiles/' . $model->excel->baseName . '.' . $model->excel->extension);

                $inputFile = $rpath.'/web/upload/excelfiles/' . $model->excel->baseName . '.' . $model->excel->extension;
                try{
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
                } catch (Exception $e) {
                    die('Error');
                }

                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                
                //$objPHPExcel = \PHPExcel_IOFactory::load('./test.xlsx');
                $sheetData = $objPHPExcel->getActiveSheet(0)->toArray(null, true, true, true);
                for ($row = 1; $row <= $highestRow; ++ $row) {
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                    if ($row == 1) {
                        continue;
                    }
                    $product_name = $rowData[0][0];
                    $qty = $rowData[0][1];

                    $productLists = \common\models\ProductList::find()->where(['product_description' => $product_name])->one();
                    $count = count($productLists);
                    if($count > 0){
                        $id = $productLists->product_list_id;
                        $campaign = $this->findModel($campaignid);
                        $qty = str_replace('_', '', $qty);
                        if(empty($qty)){
                           $qty = '1'; 
                        }
                        $campaign_start_date = date('Y-m-d', strtotime($campaign->campaign_start_date));
                        $campaign_end_date = date('Y-m-d', strtotime($campaign->campaign_end_date));
                        $sql = "SELECT * FROM campaign_assign_products WHERE campaign_start_date <= '$campaign_end_date' AND campaign_end_date >= '$campaign_start_date' AND product_list_id = $id";
                        $check_conflicts = CampaignAssignProducts::findBySql($sql)->count();

                        if($check_conflicts == 0){
                            //$chkbarcode = ProductList::find()->where(['product_list_id' => $id])->count();
                            
                            $chkbarcode = CampaignAssignProducts::find()->where(['campaign_id' => $campaignid, 'product_list_id' => $id])->count();
                            if($chkbarcode == 0){
                                $product_list_id = $id;
                                $productlist = ProductList::find()->where(['product_list_id' => $product_list_id])->one();

                                $Item_status = 'Good';
                                $totvalue = $productlist->pack_size * $qty;

                                $CampaignAssignProducts = new CampaignAssignProducts();
                                $CampaignAssignProducts->campaign_id = $campaignid;
                                $CampaignAssignProducts->product_list_id = $id;
                                $CampaignAssignProducts->new_points_per_l = $qty;
                                $CampaignAssignProducts->campaign_start_date = $campaign->campaign_start_date;
                                $CampaignAssignProducts->campaign_end_date = $campaign->campaign_end_date;
                                $CampaignAssignProducts->save();

                                $response['barcodeid'] = $CampaignAssignProducts->cap_id;
                                $response['product_name'] = $productlist->product_name;
                                $response['product_description'] = $productlist->product_description;
                                $response['pack_size'] = $productlist->pack_size;
                                $response['points_per_liter'] = $productlist->points_per_liter;
                                $response['new_points_per_liter'] = $qty;
                                $response['total_points_awarded'] = $productlist->pack_size * $qty;
                                $response['total_value_rm'] = $totvalue * $campaign->campaign_rm_per_point;            
                                $response['status']       = $Item_status;
                                $msg .= '<li class="list-group-item"><span class="badge pull-right bg-green"><i class="fa fa-check" aria-hidden="true"></i></span>  Product add <b>('.$product_name.')</b> successfully.</li>';
                            }else{
                               //$response = null;
                               $msg .= '<li class="list-group-item"><span class="badge pull-right bg-yellow"><i class="fa fa-check" aria-hidden="true"></i></span>  Product <b>('.$product_name.')</b> already taken.</li>';
                            }
                        }else {
                            //$response = 'conflicts';
                            $msg .= '<li class="list-group-item"><span class="badge pull-right bg-red"><i class="fa fa-exclamation" aria-hidden="true"></i></span>  Sorry product <b>('.$product_name.')</b> conflicts.</li>';
                        }
                    }else{
                        $msg .= '<li class="list-group-item"><span class="badge pull-right bg-red"><i class="fa fa-ban" aria-hidden="true"></i></span>  Product Not Found this -> <b>('.$product_name.')</b></li>';
                    }
                    
                }
                unlink($inputFile);
                return $this->render('summary', [
                    'msg' => $msg,
                    'campaignid' => $campaignid,
                ]);
            }else{
                print_r($model->getErrors());
            }
        } else {
            //$session['referrer'] = Yii::$app->request->referrer;
            return $this->render('uploadexcel', [
                        'model' => $model,
            ]);
        }
    }
    
    
    public function actionAjaxDelete(){
           $id = $_POST['delete_id'];
           if(!empty($id)){
               $this->findModelproduct($id)->delete();
               echo 'YES';
           }
           
           //echo "YES";
    }

    /**
     * Creates a new Campaign model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Campaign();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->campaign_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Campaign model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->campaign_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Campaign model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Campaign model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Campaign the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Campaign::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function findModelproduct($id)
    {
        if (($model = CampaignAssignProducts::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
