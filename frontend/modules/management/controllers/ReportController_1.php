<?php

namespace app\modules\management\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;

use common\models\ReportPainterProfile;
use common\models\ReportPainterProfileSearch;
use common\models\PointOrderSearch;
use common\models\PointOrderItemSearch;
use common\models\RedemptionSearch;
use common\models\PainterProfile;
use common\models\PainterProfileSearch;

class ReportController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $searchModel = new ReportPainterProfileSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=50;
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionTransaction()
    {
        $searchModel = new PointOrderSearch();
        $searchModel->order_status = '17';
        $dataProvider = $searchModel->searchtra(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=50;
        return $this->render('transaction', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionTransactionSupportingDocumentExcel(){
        $model = new \common\models\Report_Form();
        if ($model->load(Yii::$app->request->post())) {
            $mont = $model->reportdate;
            
            $start_time = date('Y-m-d', strtotime($model->reportdate));
            $end_time = date('Y-m-d', strtotime($model->reportdateto));
            $start_time = $start_time.' 00:00:00';
            $end_time = $end_time.' 23:59:59';
            $rpdate = date('d-m-Y', strtotime($model->reportdate)).'-'.date('d-m-Y', strtotime($model->reportdateto));
            //echo $start_time.'|'.$end_time;
            //die();
            $objPHPExcel = new \PHPExcel();
            $sheet=0;
            $objPHPExcel->setActiveSheetIndex($sheet);

            $query = \common\models\PointOrderItem::find();
            $query->joinWith(['order','profile','productListItem']);
            $query->andWhere(['between', 'point_order.created_datetime', $start_time, $end_time]);
            $query->andWhere(['=','order_status', '17']);
            $data = $query->asArray()->all();
            $items = ArrayHelper::toArray($data,'');

            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);

            $objPHPExcel->getActiveSheet()->setTitle($mont)                     
                ->setCellValue('A1', 'Membership#')
                ->setCellValue('B1', 'Full Name')
                ->setCellValue('C1', 'NRIC / Passport #')
                ->setCellValue('D1', 'Transaction #')
                ->setCellValue('E1', 'Dealer Name')
                ->setCellValue('F1', 'Dealer Address')
                ->setCellValue('G1', 'Dealer Invoice #')
                ->setCellValue('H1', 'Dealer Invoice (RM)')
                ->setCellValue('I1', 'Product Name')
                ->setCellValue('J1', 'Product Description')
                ->setCellValue('K1', 'Qty')
                ->setCellValue('L1', 'Total Points')
                ->setCellValue('M1', 'Total RM award')
                ->setCellValue('N1', 'Transaction Date')
                ->setCellValue('O1', 'Last Modify Date')    
                ->setCellValue('P1', 'Redemption #');

            $row=2;
            //set_time_limit(30);
            //for ($i = 0; $i < 1000; $i++) {
                foreach ($items as $item){
                    $userID = $item['painter_login_id'];
                    $dealerID = $item['order']['order_dealer_id'];
                    $orderID = $item['order']['order_id'];
                    $productID = $item['product_list_id'];
                    //$profile = \app\modules\painter\models\PainterProfile::find()->where(['user_id' => $userID,])->one();
                    $dealerOutlet = \common\models\DealerList::find()->select(['customer_name','address'])->where(['id' => $dealerID])->one();
                    //$dealerOutletitems = ArrayHelper::toArray($dealerOutlet,'');

                    //$productListItem = \common\models\ProductList::find()->where(['product_list_id' => $productID])->one();
                    $rd = \common\models\Redemption::find()->where(['LIKE', 'orderID', $orderID])->one();
                    if(count($rd) > 0){
                        $rid = $rd->redemption_invoice_no;
                    }else{
                        $rid = 'N/A';
                    }
                    //echo $item['order']['order_num'].'<br>';
                    $ic = $item['profile']['ic_no'].' ';
                    $objPHPExcel->getActiveSheet()->setCellValue('A'.$row,$item['profile']['card_id']); 
                    $objPHPExcel->getActiveSheet()->setCellValue('B'.$row,$item['profile']['full_name']);
                    $objPHPExcel->getActiveSheet()->setCellValue('C'.$row,$ic);
                    $objPHPExcel->getActiveSheet()->setCellValue('D'.$row,$item['order']['order_num']);
                    $objPHPExcel->getActiveSheet()->setCellValue('E'.$row,$dealerOutlet->customer_name);
                    $objPHPExcel->getActiveSheet()->setCellValue('F'.$row,$dealerOutlet->address);
                    $objPHPExcel->getActiveSheet()->setCellValue('G'.$row,$item['order']['dealer_invoice_no']);
                    $objPHPExcel->getActiveSheet()->setCellValue('H'.$row,$item['order']['dealer_invoice_price']);
                    $objPHPExcel->getActiveSheet()->setCellValue('I'.$row,$item['productListItem']['product_name']);
                    $objPHPExcel->getActiveSheet()->setCellValue('J'.$row,$item['productListItem']['product_description']);
                    $objPHPExcel->getActiveSheet()->setCellValue('K'.$row,$item['total_qty']);
                    $objPHPExcel->getActiveSheet()->setCellValue('L'.$row,$item['total_qty_point']);
                    $objPHPExcel->getActiveSheet()->setCellValue('M'.$row,$item['total_qty_value']);
                    $objPHPExcel->getActiveSheet()->setCellValue('N'.$row,date('d-M-y H:i:s', strtotime($item['order']['created_datetime'])));
                    $objPHPExcel->getActiveSheet()->setCellValue('O'.$row,date('d-M-y H:i:s', strtotime($item['order']['updated_datetime'])));                    
                    $objPHPExcel->getActiveSheet()->setCellValue('P'.$row,$rid);
                    $row++ ;
                }
            //echo '<pre>';
            //print_r($objPHPExcel);
            //die();
            header('Content-Type: application/vnd.ms-excel');
            //$filename = "Monthly_Report_for_".date('Y-m', strtotime($model->reportdate)).".xls";
            $filename = "Report_for_".$rpdate.".xls";            
            header('Content-Disposition: attachment;filename='.$filename .' ');
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save('php://output');
        }else{
            return $this->render('_form_report', [
                'model' => $model,
            ]);
        }        
    }
    public function actionTransactionitem()
    {
        $searchModel = new PointOrderItemSearch();
        //$searchModel->order_status = '17';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=50;
        return $this->render('transaction_item', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionRedemptionsd()
    {
        $searchModel = new RedemptionSearch();
        $searchModel->redemption_status_ray = ['2','19'];
        
        $dataProvider = $searchModel->searchray(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=50;
        
        return $this->render('payment_report', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionPainterprofilesummery()
    {
        $searchModel = new PainterProfileSearch();
        $searchModel->profile_status = ['A'];
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=50;
        return $this->render('painter_summary_report', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionPainterprofilelogin()
    {
        $searchModel = new PainterProfileSearch();
        //$searchModel->user_type = ['P'];
        
        $dataProvider = $searchModel->searchemails(Yii::$app->request->queryParams);
        
        $dataProvider->pagination->pageSize=50;
        return $this->render('painter_summary_report_log', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionRedemptionsummery()
    {
        $searchModel = new RedemptionSearch();
        $searchModel->redemption_status_ray = ['2'];
        //$dataProvider->pagination->pageSize=100;
        $dataProvider = $searchModel->searchray(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=50;
        //$dataProvider->pagination  = false;

        return $this->render('redemption_summary_report', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionBulkpayment()
    {
        $searchModel = new RedemptionSearch();
        $searchModel->redemption_status_ray = ['2'];
        //$dataProvider->pagination->pageSize=100;
        $dataProvider = $searchModel->searchray(Yii::$app->request->queryParams);
        //$dataProvider->pagination->pageSize=100;
        $dataProvider->pagination  = false;
        
        //$filename = 'Data-'.Date('YmdGis').'-Mahasiswa.txt';
        //header("Content-type: application/vnd-ms-excel");
        //header("Content-Disposition: attachment; filename=".$filename);
        /*echo 'HAPST00565391REWARDSSOL   REWARDS SOLUTION SDN BHD                512343627558                                    000000000055314                                                      S';
        foreach($dataProvider as $data){
            
            $name = $data->profile->full_name;
            echo 'D        '.$name.'                         640608107515                        0120041860070005253       000000000006498';
        }
        echo 'T000000000000002000000000055314';*/

        return $this->render('bulkpayment', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionSummaryViewExcel()
    {
        $query = ReportPainterProfile::find();
        $query->joinWith(['user']);
        $data = $query->asArray()->all();
        $items = ArrayHelper::toArray($data,'');
        $bal = '0.00';
        $footer_totalitems = 0;
        $footer_totalrmawarded = 0;
        $footer_Balance = '0.00';
        $filename = Date('YmdGis').'_Summary_View.xls';
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=".$filename);
        header("Pragma: no-cache"); 
        header("Expires: 0");
        
        echo '<table border="1" width="100%">';
        echo '<thead><tr>
            <th>Full Name</th><th>NRIC/PP Number</th><th>Total # of Transactions</th><th>Total Items</th><th>Total Points Awarded</th><th>Total RM Awarded</th><th>Total # of Redemptions</th><th>Total Points Redeemed</th><th>Total RM paid</th><th>Balance Total Points</th><th>Balance RM to be paid</th>
        </tr></thead>';
        echo '<tbody>';
        foreach ($items as $item){
            $userID = $item['user_id'];
            $totalnotransactions = \common\models\PointOrder::find()->where(['painter_login_id' => $userID, 'order_status' => '17'])->count();
            $totalitems = \common\models\PointOrder::find()->where(['painter_login_id' => $userID, 'order_status' => '17'])->sum('order_qty');
            $totalpointsawarded = \common\models\PointOrder::find()->where(['painter_login_id' => $userID, 'order_status' => '17'])->sum('order_total_point');
            $totalrmawarded = \common\models\PointOrder::find()->where(['painter_login_id' => $userID, 'order_status' => '17'])->sum('order_total_amount');
            $total_no_of_redemptions = \common\models\Redemption::find()->where(['painterID' => $userID, 'redemption_status_ray' => '19'])->count();
            $total_points_redeemed = \common\models\Redemption::find()->where(['painterID' => $userID, 'redemption_status_ray' => '19'])->sum('req_points');
            $total_rm_paid = \common\models\Redemption::find()->where(['painterID' => $userID, 'redemption_status_ray' => '19'])->sum('req_amount');
            $vbalance_total_points = $totalpointsawarded - $total_points_redeemed;
            
            $bal = $totalrmawarded - $total_rm_paid;
            $ic = strval($item['ic_no']);
            //$ic = "$ic";
            //echo '<pre>';
            echo '<tr>
            <td>'.$item['full_name'].'</td>
            <td style="text-align: left;">'.$ic.'&nbsp;</td>
            <td>'.$totalnotransactions.'</td>
            <td>'.$totalitems.'</td>
            <td>'.$totalpointsawarded.'</td>
            <td>'.$totalrmawarded.'</td>
            <td>'.$total_no_of_redemptions.'</td>
            <td>'.$total_points_redeemed.'</td>
            <td>'.$total_rm_paid.'</td>
            <td>'.$vbalance_total_points.'</td>
            <td>'.$bal.'</td>
            </tr>';
            $footer_totalitems += $totalitems;
            $footer_totalrmawarded += $totalrmawarded;
            $footer_Balance += $bal;
            //die();  
        }
        echo '</tbody>';
        echo '<tbody><tr><td></td><td></td><td></td><td>'.$footer_totalitems.'</td><td></td><td>'.$footer_totalrmawarded.'</td><td></td><td></td><td></td><td></td><td>'.$footer_Balance.'</td></tr></tbody>';
        echo '</table>'; 
    }
    
    public function actionPainterAccountManagementExcel()
    {
        $query = PainterProfile::find()->where(['profile_status' => 'A']);
        $query->joinWith(['user']);
        $data = $query->asArray()->all();
        $items = ArrayHelper::toArray($data,'');
        
        $filename = Date('YmdGis').'_Painter_Account_Management_Summary_Report.xls';
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=".$filename);
        header("Pragma: no-cache"); 
        header("Expires: 0");

        echo '<table border="1" width="100%">';
        echo '<thead><tr>
                <th>Membership</th>
                <th>Full Name</th>
                <th>NRIC/Passport #</th>
                <th>Mobile No</th><th>Email ID</th>
                <th>Company Name</th>
                <th>No. of Employees</th>
                <th>No. of Painting Jobs</th>
                <th>Registered At Dealer Outlet</th>
                <th>Dealer Address</th>
                <th>Bank Name</th>
                <th>Name of Account Holder</th>
                <th>Account Number</th>
                <th>Account Verify</th>
        </tr></thead>';
        echo '<tbody>';
        foreach ($items as $item){
            $userID = $item['user_id'];
            $company = \app\modules\painter\models\CompanyInformation::find()->where(['user_id' => $userID])->one();
            $dealerOutlet = \common\models\DealerList::find()->where(['id' => $company->dealer_outlet])->one();
            $bank = \app\modules\painter\models\BankingInformation::find()->where(['user_id' => $userID])->one();
            $banks = \common\models\Banks::find()->where(['id' => $bank->bank_name])->one();
            $bankname = $banks->bank_name;
            $ic = strval($item['ic_no']);
            //$ic = "$ic";
            //echo '<pre>';
            echo '<tr>
            <td>'.$item['card_id'].'&nbsp;</td>
            <td>'.$item['full_name'].'</td>    
            <td style="text-align: left;">'.$ic.'&nbsp;</td>
            <td>'.$item['mobile'].'&nbsp;</td>
            <td>'.$item['user']['email'].'&nbsp;</td>      
            <td>'.$company['company_name'].'</td>
            <td>'.$company->no_painters.'</td>
            <td>'.$company->painter_sites.'</td>
            <td>'.$dealerOutlet->customer_name.'</td>
            <td>'.$dealerOutlet->address.'</td>
            <td>'.$bankname.'</td>
            <td>'.$bank->account_name.'</td>
            <td>'.$bank->account_number.'</td>
            <td>'.$bank->account_no_verification.'</td>    
            </tr>';
            
        }
        echo '</tbody>';
        //echo '<tbody><tr><td></td><td></td><td></td><td>'.$footer_totalitems.'</td><td></td><td>'.$footer_totalrmawarded.'</td><td></td><td></td><td></td><td></td><td>'.$footer_Balance.'</td></tr></tbody>';
        echo '</table>';         
    }
    
    public function actionTransactionSupportingDocumentExcel123()
    {
        $query = \common\models\PointOrderItem::find();
        $query->joinWith(['order']);
        $query->andWhere(['=','order_status', '17']);
        $data = $query->asArray()->all();
        $items = ArrayHelper::toArray($data,'');
        /*echo '<pre>';
        print_r($items);
        die();*/
        $filename = Date('YmdGis').'_Transaction_Supporting_Document_Report.xls';
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=".$filename);
        header("Pragma: no-cache"); 
        header("Expires: 0");
        
        echo '<table border="1" width="100%">';
        echo '<thead><tr>
                <th>Membership#</th>
                <th>Full Name</th>
                <th>NRIC / Passport #</th>
                <th>Transaction #</th>
                <th>Dealer Name</th>
                <th>Dealer Address</th>
                <th>Dealer Invoice #</th>
                <th>Dealer Invoice (RM)</th>
                <th>Product Name</th>
                <th>Product Description</th>
                <th>Qty</th>
                <th>Total Points</th>
                <th>Total RM award</th>
                <th>Transaction Date</th>
                <th>Redemption #</th>
        </tr></thead>';
        echo '<tbody>';
        foreach ($items as $item){
            $userID = $item['painter_login_id'];
            $dealerID = $item['order']['order_dealer_id'];
            $orderID = $item['order']['order_id'];
            //echo $orderID;
            //die();
            //$item['point_order_id']
            $productID = $item['product_list_id'];
            $profile = \app\modules\painter\models\PainterProfile::find()->where(['user_id' => $userID,])->one();
            $dealerOutlet = \common\models\DealerList::find()->where(['id' => $dealerID])->one();
            $productListItem = \common\models\ProductList::find()->where(['product_list_id' => $productID])->one();
            $rd = \common\models\Redemption::find()->where(['LIKE', 'orderID', $orderID])->one();
            /*foreach($rd as $r){
                $rid = '<td>'.$orderID.'-'.$r->redemption_invoice_no.'</td>';
            }*/
            
            //foreach($rd as $r){
            if(count($rd) > 0){
                $rid = '<td>'.$rd->redemption_invoice_no.'</td>';
            }else{
                $rid = '<td>N/A</td>';
            }
            //}

            //$ic = "$ic";
            //echo '<pre>';
            echo '<tr>
            <td>'.$profile->card_id.'&nbsp;</td>
            <td>'.$profile->full_name.'</td>  
            <td style="text-align: left;">'.$profile->ic_no.'&nbsp;</td>
            <td>'.$item['order']['order_num'].'</td>
            <td>'.$dealerOutlet->customer_name.'</td>
            <td>'.$dealerOutlet->address.'</td>
            <td>'.$item['order']['dealer_invoice_no'].'</td>
            <td>'.$item['order']['dealer_invoice_price'].'</td>
            <td>'.$productListItem->product_name.'</td>
            <td>'.$productListItem->product_description.'</td>
            <td>'.$item['total_qty'].'</td>
            <td>'.$item['total_qty_point'].'</td>
            <td>'.$item['total_qty_value'].'</td>
            <td>'.date('d-M-y H:i:s', strtotime($item['order']['created_datetime'])).'</td>
            '.$rid.' 
            </tr>';
            //$footer_totalitems += $totalitems;
            //$footer_totalrmawarded += $totalrmawarded;
            //$footer_Balance += $bal;
            //die();  
        }
        echo '</tbody>';
        echo '</table>';
        //$filename = Date('YmdGis').'_Painter_Account_Management_Summary_Report.xls';
        
    }
    
    public function actionRedemptionSupportingDocumentExcel()
    {
        //'2','19'
        $query = \common\models\Redemption::find()->where(['or','redemption_status_ray=2','redemption_status_ray=19']);
        $query->joinWith(['profile','bank']);
        $data = $query->asArray()->all();
        $items = ArrayHelper::toArray($data,'');
        
        $filename = Date('YmdGis').'_Redemption_Supporting_Document_Report.xls';
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=".$filename);
        header("Pragma: no-cache"); 
        header("Expires: 0");
        
        echo '<table border="1" width="100%">';
        echo '<thead><tr>
                <th>Membership #</th>
                <th>Full Name</th>
                <th>NRIC/Passport #</th>
                <th>Bank Acc #</th>
                <th>Bank Acc Holder Name</th>
                <th>Bank Name</th>
                <th>Request Date</th>
                <th>Redemption #</th>
                <th>Points Redeemed</th>
                <th>RM Redeemed</th>
                <th>Paid Date</th>
                <th>RM Paid</th>
        </tr></thead>';
        echo '<tbody>';
        foreach ($items as $item){
            $banks = \common\models\Banks::find()->where(['id' => $item['bank']['bank_name']])->one();
            $bankname = $banks->bank_name;
            $paiddate = $item['redemption_status_ray_date'];
            if (empty($paiddate)) {
                $raydate = "N/A";
            } else {
                $raydate = date('d-M-y', strtotime($paiddate));
            }
            if ($item['redemption_status_ray'] == '2') {
                $raypaid = '0.00';
            } else {
                $raypaid = $item['req_amount'];
            }
            echo '<tr>
            <td>'.$item['profile']['card_id'].'&nbsp;</td>
            <td>'.$item['profile']['full_name'].'</td>  
            <td style="text-align: left;">'.$item['profile']['ic_no'].'&nbsp;</td>
            <td>'.$item['bank']['account_number'].'&nbsp;</td>
            <td>'.$item['bank']['account_name'].'</td>
            <td>'.$bankname.'</td>
            <td>'.date('d-M-y H:i:s', strtotime($item['redemption_created_datetime'])).'</td>
            <td>'.$item['redemption_invoice_no'].'</td>
            <td>'.$item['req_points'].'</td>
            <td>'.$item['req_amount'].'</td>
            <td>'.$raydate.'</td>
            <td>'.$raypaid.'</td>
            </tr>';
        }
        echo '</tbody>';
        echo '</table>';
    }
    
    public function actionRedemptionSummaryExcel()
    {
        $query = \common\models\Redemption::find()->where(['redemption_status_ray' => '2']);
        $query->joinWith(['profile','bank']);
        $data = $query->asArray()->all();
        $items = ArrayHelper::toArray($data,'');

        $filename = Date('YmdGis').'_Redemption_Summary_Report.xls';
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=".$filename);
        header("Pragma: no-cache"); 
        header("Expires: 0");
        
        echo '<table border="1" width="100%">';
        echo '<thead><tr>
                <th>Redemption #</th>
                <th>Membership #</th>
                <th>Painter Name</th>
                <th>NRIC/Passport #</th>
                <th>Total Transactions</th>
                <th>Total Points</th>
                <th>Total RM Amount</th>
                <th>Bank Name</th>
                <th>Bank Acc Holder Name</th>
                <th>Bank Acc #</th>
                <th>Account Verified</th>                
                <th>Email</th>                
                <th>Mobile</th>                
                <th>Status</th>
                <th>Request Date</th>
        </tr></thead>';
        echo '<tbody>';
        foreach ($items as $item){
            $total_tr = \common\models\RedemptionItems::find()->where(['redemptionID' => $item['redemptionID']])->count();
            $banks = \common\models\Banks::find()->where(['id' => $item['bank']['bank_name']])->one();
            $bankname = $banks->bank_name;
            $paiddate = $item['redemption_status_ray_date'];
            if (empty($paiddate)) {
                $raydate = "N/A";
            } else {
                $raydate = date('d-M-y', strtotime($paiddate));
            }
            if ($item['redemption_status_ray'] == '2') {
                $raypaid = '0.00';
            } else {
                $raypaid = $item['req_amount'];
            }
            
            $verify = $item['bank']['account_no_verification'];
            if($verify == 'Y'){
                $veryfied = 'Y';
            }else{
                $veryfied = '';
            }
            
            echo '<tr>
            <td>'.$item['redemption_invoice_no'].'</td>    
            <td>'.$item['profile']['card_id'].'&nbsp;</td>
            <td>'.$item['profile']['full_name'].'</td>  
            <td style="text-align: left;">'.$item['profile']['ic_no'].'&nbsp;</td>
            <td>'.$total_tr.'&nbsp;</td>  
            <td>'.$item['req_points'].'</td>
            <td>'.$item['req_amount'].'</td>    
            <td>'.$bankname.'</td>
            <td>'.$item['bank']['account_name'].'</td>
            <td>'.$item['bank']['account_number'].'</td>      
            <td>'.$veryfied.'</td>
            <td>'.$item['profile']['email'].'</td>
            <td>'.$item['profile']['mobile'].'</td>
            <td>Processing</td>    
            <td>'.date('d-M-y H:i:s', strtotime($item['redemption_created_datetime'])).'</td>            
            </tr>';
        }
        echo '</tbody>';
        echo '</table>';
        
    }
    

}
