<?php

namespace app\modules\management\controllers;

use Yii;
use common\models\DealerList;
use common\models\DealerListSearch;
use common\models\Region;
use common\models\State;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DealerlistController implements the CRUD actions for DealerList model.
 */
class DealerlistController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DealerList models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DealerListSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DealerList model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DealerList model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DealerList();

        if ($model->load(Yii::$app->request->post())) {
            $regionID = Region::find()
                ->where(['region_id' => $model->region_id])
                ->one();
            $model->region = $regionID->region_name;
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing DealerList model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $regionID = Region::find()
                ->where(['region_id' => $model->region_id])
                ->one();
            $model->region = $regionID->region_name;
            $model->save();
            \Yii::$app->getSession()->setFlash('success',['title' => 'Edit', 'text' => 'Record has been successfully updated.']);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DealerList model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        //$this->findModel($id)->delete();
        $model = $this->findModel($id);
        $model->status = 'X';
        $model->save();
        \Yii::$app->getSession()->setFlash('success',['title' => 'Delete', 'text' => 'Record has been successfully deleted.']);
        return $this->redirect(['index']);
    }

    /**
     * Finds the DealerList model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DealerList the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DealerList::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionDregion($id){
        $states = State::find()
                ->where(['region_id' => $id])
                ->all();
        if ($states) {
            //echo $sle;
            foreach ($states as $state) {
                echo "<option value='" . $state->state_name . "'>" . $state->state_name . "</option>";
            }
        } else {
            echo "<option>-</option>";
        }
    }
}
