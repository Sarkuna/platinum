<?php

namespace app\modules\management\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use common\models\Redemption;
use common\models\RedemptionSearch;
use common\models\RedemptionHistory;
use common\models\RedemptionRemark;
use common\models\PointOrder;


/**
 * RedemptionController implements the CRUD actions for Redemption model.
 */
class RedemptionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Redemption models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RedemptionSearch();
        //$searchModel->redemption_status = ['1','7','17'];
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionPayment()
    {
        $searchModel = new RedemptionSearch();
        $searchModel->redemption_status_ray = ['2','8','10','19'];
        $dataProvider = $searchModel->searchray(Yii::$app->request->queryParams);

        return $this->render('payment', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionProcessing()
    {
        $bulkredem = new \common\models\RedemptionBulk();
        if ($bulkredem->load(Yii::$app->request->post())) {
            $selection=(array)Yii::$app->request->post('selection');//typecasting
            $redemption_status_ray = $bulkredem->redemption_status_ray;
            $comment = $bulkredem->comment;
            $msg = 1;
            if(count($selection) > 0){
                foreach($selection as $valueid){
                    $model = $this->findModel($valueid);
                    //echo $valueid.'-'.$redemption_status_ray.'-'.$comment.'<br>';
                    $model->redemption_status_ray = $redemption_status_ray;
                    $model->redemption_remarks = $comment;
                    $model->redemption_status_ray_date = date('Y-m-d');
                    if($model->save()){
                        $redemptionhistory = new RedemptionHistory();
                        $redemptionhistory->redemptionID = $valueid;
                        $redemptionhistory->redemption_status = $redemption_status_ray;
                        $redemptionhistory->comment = $comment;
                        $redemptionhistory->date_added = date('Y-m-d H:i:s');
                        $redemptionhistory->save(false);

                        $painterId = $model->painterID;
                        $user = \common\models\User::find()->where(['id' => $painterId])->one();
                        $banks = \common\models\Banks::find()->where(['id' => $model->bank->bank_name])->one();
                        $data = \yii\helpers\Json::encode(array(
                            'redemption_ID' => $model->redemption_invoice_no,
                            'redemption_point' => $model->req_points,
                            'redemption_value' => $model->req_amount,
                            're_paid_date' => date('d-m-Y'),
                            'bank' => $banks->bank_name,
                            'account_holder_name' => $model->bank->account_name,
                            'bank_account_no' => $model->bank->account_number,
                        ));
                        if (!empty($user->email) && $model->redemption_status_ray == '19') {
                            $subject = '';                    
                            Yii::$app->ici->sendEmailpainter($user->id, $user->email, Yii::$app->params['email.template.code.successful.redemption.payout'], $data, $subject);
                        }

                        $painterinfo = \common\models\PainterProfile::find()
                        ->where(['user_id' => $user->id])
                        ->one();
                        if(!empty($painterinfo->mobile) && $model->redemption_status_ray == '19'){
                            $mobile = $painterinfo->mobile;
                            $result = Yii::$app->ici->sendSMSpainter($user->id, $mobile, Yii::$app->params['email.template.code.successful.redemption.payout'], $data);
                        }                        
                        //return $this->redirect(Yii::$app->request->referrer);
                    }else{
                        print_r($model->getErrors());
                    }
                    $msg++;
                }// End Selection colum key
                if($msg > 1){
                    \Yii::$app->getSession()->setFlash('success',['title' => 'Action', 'text' => 'Bulk Action has been updated!']);
                    return $this->redirect(['processing']);
                }
            }else{ // End Selection Count
                return $this->redirect(['processing']);
            }
        }else{
            $searchModel = new RedemptionSearch();
            $searchModel->redemption_status_ray = ['2'];
            $dataProvider = $searchModel->searchray(Yii::$app->request->queryParams);

            return $this->render('processing', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'bulkredem' => $bulkredem,
            ]);
        }
    }
    
    public function actionDenied()
    {
        $searchModel = new RedemptionSearch();
        $searchModel->redemption_status_ray = ['8'];
        $dataProvider = $searchModel->searchray(Yii::$app->request->queryParams);

        return $this->render('denied', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionFailed()
    {
        $searchModel = new RedemptionSearch();
        $searchModel->redemption_status_ray = ['10'];
        $dataProvider = $searchModel->searchray(Yii::$app->request->queryParams);

        return $this->render('failed', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionPaid()
    {
        $searchModel = new RedemptionSearch();
        $searchModel->redemption_status_ray = ['19'];
        $dataProvider = $searchModel->searchray(Yii::$app->request->queryParams);

        return $this->render('paid', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Redemption model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }
    
    public function actionInvoiceapprove($id)
    {
        $model = $this->findModel($id);
        $newcomment = new RedemptionRemark();
        if ($model->load(Yii::$app->request->post())) {
            $newcomment->load(Yii::$app->request->post());
            $model->redemption_remarks = $newcomment->comment;
            if($model->redemption_status == '17'){
                $model->redemption_status_ray = '2';
            }
            $model->redemption_status_date = date('Y-m-d');
            if($model->save()){
                $redemptionhistory = new RedemptionHistory();
                $redemptionhistory->redemptionID = $id;
                $redemptionhistory->redemption_status = $model->redemption_status;
                $redemptionhistory->comment = $newcomment->comment;
                $redemptionhistory->date_added = date('Y-m-d H:i:s');
                $redemptionhistory->save(false);
                
                $painterId = $model->painterID;
                $user = \common\models\User::find()->where(['id' => $painterId])->one();
                $data = \yii\helpers\Json::encode(array(
                    'redemption_ID' => $model->redemption_invoice_no,
                    'redemption_point' => $model->req_points,
                    'redemption_value' => $model->req_amount,
                ));
                if (!empty($user->email) && $model->redemption_status == '17') {
                    $subject = '';
                    Yii::$app->ici->sendEmailpainter($user->id, $user->email, Yii::$app->params['email.template.code.successful.redemption.approve'], $data, $subject);
                }
                
                $painterinfo = \common\models\PainterProfile::find()
                ->where(['user_id' => $user->id])
                ->one();
                
                if(!empty($painterinfo->mobile) && $model->redemption_status == '17'){
                    $mobile = $painterinfo->mobile;
                    $result = Yii::$app->ici->sendSMSpainter($user->id, $mobile, Yii::$app->params['email.template.code.successful.redemption.approve'], $data);
                }
                \Yii::$app->getSession()->setFlash('success',['title' => 'Painter Transaction Management', 'text' => 'Action has been updated!']);
                return $this->redirect(['index']); 
            }else{
                print_r($model->getErrors());
            }
            return $this->redirect(['view', 'id' => $model->redemptionID]);
        } else {
            return $this->render('invoiceapprove', [
                'model' => $model,
                'newcomment' => $newcomment,
            ]);
        }
    }
    
    public function actionMakepayment($id)
    {
        $model = $this->findModel($id);
        $newcomment = new RedemptionRemark();
        if ($model->load(Yii::$app->request->post())) {
            $newcomment->load(Yii::$app->request->post());
            $model->redemption_remarks = $newcomment->comment;
            $model->redemption_status_ray_date = date('Y-m-d');
            if($model->save()){
                $redemptionhistory = new RedemptionHistory();
                $redemptionhistory->redemptionID = $id;
                $redemptionhistory->redemption_status = $model->redemption_status;
                $redemptionhistory->comment = $newcomment->comment;
                $redemptionhistory->date_added = date('Y-m-d H:i:s');
                $redemptionhistory->save(false);
                
                $painterId = $model->painterID;
                $user = \common\models\User::find()->where(['id' => $painterId])->one();
                $banks = \common\models\Banks::find()->where(['id' => $model->bank->bank_name])->one();
                $data = \yii\helpers\Json::encode(array(
                    'redemption_ID' => $model->redemption_invoice_no,
                    'redemption_point' => $model->req_points,
                    'redemption_value' => $model->req_amount,
                    're_paid_date' => date('d-m-Y'),
                    'bank' => $banks->bank_name,
                    'account_holder_name' => $model->bank->account_name,
                    'bank_account_no' => $model->bank->account_number,
                ));
                if (!empty($user->email) && $model->redemption_status_ray == '19') {
                    $subject = '';                    
                    Yii::$app->ici->sendEmailpainter($user->id, $user->email, Yii::$app->params['email.template.code.successful.redemption.payout'], $data, $subject);
                }
                
                $painterinfo = \common\models\PainterProfile::find()
                ->where(['user_id' => $user->id])
                ->one();
                if(!empty($painterinfo->mobile) && $model->redemption_status_ray == '19'){
                    $mobile = $painterinfo->mobile;
                    $result = Yii::$app->ici->sendSMSpainter($user->id, $mobile, Yii::$app->params['email.template.code.successful.redemption.payout'], $data);
                    /*echo '<pre>';
                    print_r($result);
                    die();*/
                }
                \Yii::$app->getSession()->setFlash('success',['title' => 'Action', 'text' => 'Action has been updated!']);
                return $this->redirect(['processing']);
                //return $this->redirect(Yii::$app->request->referrer);
            }else{
                print_r($model->getErrors());
            }
            return $this->redirect(['view', 'id' => $model->redemptionID]);
        } else {
            return $this->render('invoicepay', [
                'model' => $model,
                'newcomment' => $newcomment,
            ]);
        }
    }
    
    public function actionMakepaymentbulk(){
        $bulkredem = new \common\models\RedemptionBulk();
        $action=Yii::$app->request->post('action');
        $selection=(array)Yii::$app->request->post('selection');//typecasting
        $redemption_status_ray = $bulkredem->redemption_status_ray;
        $comment = Yii::$app->request->post('RedemptionBulk[comment]');
        echo $redemption_status_ray.$comment;
        //
        //print_r($selection);
        if(count($selection) > 0){
            foreach($selection as $valueid){
                echo $valueid.$redemption_status_ray.$comment.'<br>';
            }
        }
        /*$painterid = Yii::$app->request->post('painterid');
        $selection_orderid = implode(",", $selection);*/
        die();
        
    }
    
    public function actionOnBehalf()
    {
        $model = new \common\models\Redemption();
        //'=','painter_login_id', Yii::$app->user->id
        if ($model->load(Yii::$app->request->post())) {
            $model->painterID = Yii::$app->user->id;            
            $model->redemption_status = '1';
            $model->redemption_status_ray = '1';
            $chkinvoiceid = \common\models\Redemption::find()->count();
            if($chkinvoiceid > 0){
                $newinvoiceid = \common\models\Redemption::find()->orderBy(['redemption_invoice_no' => SORT_DESC,])->one();
                $incinvoice = str_replace(Yii::$app->params['invoice.prefix'],"",$newinvoiceid->redemption_invoice_no);
                $incinvoice = $incinvoice + 1;
            }else{
                $incinvoice = Yii::$app->params['invoice.prefix.dft'];
            }
            $model->redemption_invoice_no = Yii::$app->params['invoice.prefix'].$incinvoice;
            $model->redemption_IP = $model->getRealIp();
            if($model->save()) {
                $orderids = explode(',', $model->orderID);
                foreach($orderids as $value){
                    $total_per_point = PointOrderItem::find()
                            ->where("painter_login_id = " . Yii::$app->user->id . " AND point_order_id = " . $value . " AND Item_status = 'G'")
                            ->sum('total_qty_point');

                    $total_per_price = PointOrderItem::find()
                            ->where("painter_login_id = " . Yii::$app->user->id . " AND point_order_id = " . $value . " AND Item_status = 'G'")
                            ->sum('total_qty_value');
                    $redemptionitem = new RedemptionItems();
                    $redemptionitem->redemptionID = $model->redemptionID;
                    $redemptionitem->order_id = $value;
                    $redemptionitem->req_per_points = $total_per_point;
                    $redemptionitem->req_per_amount = $total_per_price;
                    $redemptionitem->save(false); 
                    unset($redemptionitem);
                }
                \Yii::$app->getSession()->setFlash('success',['title' => 'Redemption Request', 'text' => 'Action successful!']);
                return $this->redirect(['redemption']);
            }else{
                print_r($model->getErrors());
            }
        }else{            
            /*$myorders = (new \yii\db\Query())->from('point_order po')
            ->join('JOIN', 'redemption_items ri', 'po.order_id = ri.order_id')
            ->where(['po.painter_login_id' => Yii::$app->user->getId(), 'po.order_status' => '17'])
            ->all();*/

            $myorders = PointOrder::find()
                //->where("painter_login_id = " . Yii::$app->user->id. " AND order_status = 17 ")
                ->where("order_status = 17 AND redemption = 'N'")
                ->orderBy('order_status ASC')    
                ->all(); 
            return $this->render('onbehalf', [
                'model' => $model,
                'myorders' => $myorders,
            ]);
        }
        /*return $this->render('onbehalf', [
                //'model' => $model,
            ]);*/
    }

    /**
     * Creates a new Redemption model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Redemption();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->redemptionID]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Redemption model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->redemptionID]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Redemption model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Redemption model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Redemption the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Redemption::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}