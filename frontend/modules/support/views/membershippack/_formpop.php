<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\MembershipPack */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Send Membership Pack';
?>


<div class="col-xs-12 col-lg-12">
    <div class="membership-pack-form">
        <?php
        $form = ActiveForm::begin([
                'id' => 'stu-master-update',
                //'enableAjaxValidation' => true,
                'enableClientValidation' => true,
                'fieldConfig' => [
                    'template' => "{label}{input}{error}",
                ],
            ]);
        ?>
        <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
            <div class="col-xs-12 col-sm-4 col-lg-4 no-padding">
                <?php echo $form->field($model, 'pack_status')->dropDownList(['' => '', 'N' => 'No', 'Y' => 'Yes']); ?>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
            <?= $form->field($model, 'remark')->textarea(array('rows'=>3,'cols'=>5)); ?>
        </div> 

    


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Send' : 'Send', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
        
    </div>    
</div>
