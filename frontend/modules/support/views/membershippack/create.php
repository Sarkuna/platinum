<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MembershipPack */

$this->title = 'Create Membership Pack';
$this->params['breadcrumbs'][] = ['label' => 'Membership Packs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="membership-pack-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
