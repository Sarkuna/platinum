<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MembershipPack */

$this->title = 'Update Membership Pack: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Membership Packs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="membership-pack-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
