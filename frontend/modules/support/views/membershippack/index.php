<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MembershipPackSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Membership Packs';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="col-xs-12">
    <div class="box">
        <div class="box-header with-border">
            <div class="col-xs-12">
                <div class="col-lg-4 col-sm-4 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-th-list"></i> <?= Html::encode($this->title) ?></h3></div>
                <div class="col-xs-6"></div>
                <div class="col-lg-2 col-sm-2 col-xs-12 no-padding" style="padding-top: 20px !important;">
                    <div class="col-xs-12 no-padding">
                        <?php if (Yii::$app->user->can("/support/membershippack/create")) { ?>
                            <?= Html::a('Create Membership Pack', ['create'], ['class' => 'btn btn-success', 'data-toggle' => 'modal', 'data-target' => '#myModal']) ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-body table-responsive">
            <div class="membership-pack-index">
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        //'id',
                        [
                            'label' => 'Name',
                            'attribute' => 'userPainter',
                            'value' => 'userPainter.full_name',
                        ],
                        [
                            'label' => 'NRIC/PP Number',
                            'attribute' => 'ic_no',
                            'value' => 'userPainter.ic_no',
                        ],
                        [
                            'label' => 'Email',
                            'attribute' => 'email',
                            'value' => 'email',
                        ],
                        
                        [
                            'label' => 'Mobile',
                            'attribute' => 'mobile',
                            'value' => 'userPainter.mobile',
                        ],
                        //'userPainter.ic_no',
                        //'userPainter.email',
                        //'userPainter.mobile',
                        //'user_id',
                        //'remark',
                        [
                            'label' => 'Status',
                            'attribute' => 'pack_status',
                            'format' => 'html',
                            'value' => function ($model) {
                                $mstaus = $model->membershipPack['pack_status'];
                                $returnValue = "";
                                if ($mstaus == "N") {
                                    $returnValue = "Not Send";
                                } else if ($mstaus == "Y") {
                                    $returnValue = "Send";
                                }
                                return $returnValue;
                                //return $model->membershipPack(getStatusDescription());
                            },
                            'filter' => array("N" => "Not Send", "Y" => "Send"),
                        ],
                        //'IP',
                        //'created_datetime',
                        // 'created_by',
                        //'membershipPack.mp_updated_datetime',
                        [
                            'label' => 'Send Date',
                            'attribute' => 'membershipPack.mp_updated_datetime',
                            'format' => ['date', 'php:d-m-Y']
                        ],             
                        //'mp_updated_datetime', 
                        /*[
                            'label' => 'Send Date',
                            'attribute' => 'mp_updated_datetime',
                            'format' => 'html',
                            'value' => function ($model) {
                                $returnValue = "";
                                $returnValue = date('d-m-Y h:i A', strtotime($model->membershipPack['mp_updated_datetime']));
                                return $returnValue;
                                //return $model->membershipPack(getStatusDescription());
                            }
                        ],  */          
                        //'updated_by',
                        //'membershipPack.id',
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{update}', //{view} {delete}
                            'buttons' => [
                                'update' => function ($url, $model) {
                                    //$url = ['id' => '66'];
                                    $url = 'update?id=' . $model->membershipPack['id'];
                                    //$returnValue = Html::a('<i class="fa fa-pencil-square-o"></i> '.Yii::t('app', 'Edit'), '#', ['class' => 'btn btn-primary btn-sm', 'onclick' => "updateGuard(".$this->id.");return false;"]);
                                    // return $returnValue;
                                    $mstaus = $model->membershipPack['pack_status'];
                                    if ($mstaus == "N") {
                                        return (Html::a('<i class="glyphicon glyphicon-briefcase"></i>', $url, ['title' => Yii::t('app', 'Update'), 'onclick' => "updateGuard(" . $model->membershipPack['id'] . ");return false;"]));
                                    }
                                }/* ,
                                      'view' => function ($url, $model) {
                                      return (Html::a('<span class="glyphicon glyphicon-search"></span>', $url, ['title' => Yii::t('app', 'Update'),]));
                                      },
                                      'delete' => function ($url, $model) {
                                      return (Html::a('<span class="glyphicon glyphicon-remove"></span>', $url, ['title' => Yii::t('app', 'Delete'), 'data' => ['confirm' => 'Are you sure you want to delete this item?', 'method' => 'post'],]));
                                      } */
                                    ],
                                //'visible' => $visible,
                                ],
                            //['class' => 'yii\grid\ActionColumn'],
                            /* [
                              'attribute' => 'membershipPack.action',
                              'format' => 'raw',
                              'value' => function ($model) {
                              return $model->membershipPack.getActions();
                              },
                              'options' => ['width' => '100']
                              ], */
                        ],
                    ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>

<?php
	yii\bootstrap\Modal::begin([
		'id' => 'guardModal',
		'header' => "<h4 class='modal-title'><i class='fa fa-edit'></i> Update </h4>",
	]);
 	yii\bootstrap\Modal::end(); 
?>

<script>
/***
  * Start Update Gardian Jquery
***/
function updateGuard(id) {
	$.ajax({
	  type:'GET',
	  url:'<?= Url::toRoute(["membershippack/update"]) ?>',
	  data: { id : id},
	  success: function(data)
		   {
		       $(".modal-content").addClass("row");
		       $('.modal-body').html(data);
		       $('#guardModal').modal();

		   }
	});
}
</script>