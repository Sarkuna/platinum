<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Terms and Conditions';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-md-12">
    <div class="box box-default">
        <div class="box-header with-border">
            <i class="fa fa-handshake-o"></i>
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            <h3>Terms and Conditions of Dulux Painter's Club Loyalty Program "Loyalty Program"</h3>
            <br>
            <ol>
                <li>To enter the Loyalty Program, you must complete fully the Dulux Painter’s Club Application form and submit the form back to Akzo Nobel Paints (Malaysia) Sdn Bhd through Akzo Nobel Paints (Malaysia) Sdn Bhd authorised representatives or by depositing forms at participating dealers.</li>
                <li>By submitting the completed Dulux Painter’s Club Application form, you agree to comply with these terms & conditions.</li>
                <li>Entry is open only to Malaysian citizens aged 18 and over, who conduct painting as part of their business, at the date of registration. Employees of Akzo Nobel Paints (Malaysia) Sdn Bhd are ineligible to participate in the Loyalty Program, as are members of their families or households or any other person connected with the running of the Loyalty Program. No third party or joint submissions will be accepted.
</li><li>Akzo Nobel Paints (Malaysia) Sdn Bhd will verify each application form and reserves the right, with or without cause, to exclude any Participants in this Loyalty Program where there has been a violation of any of these terms & conditions.
</li><li>Upon approval of application for membership into the Dulux Painter’s Club Loyalty Program, a membership pack shall be sent to each member. The membership pack will contain a membership card that will uniquely identify the member and will be used by the member to participate in any offer or event.
</li><li>Loyalty Program members must notify Akzo Nobel Paints (Malaysia) Sdn Bhd promptly if there is a change in their particulars.</li>
<h4 class="text-bold">Dulux Painter’s Club Loyalty Program Mechanics</h4>
<li>Dulux Painter's Club Loyalty Program membership card is applicable in Malaysia only.
</li><li>Dulux Painter's Club Loyalty Program membership card is not transferable, refundable or exchangeable for cash.
</li><li>To qualify for points awarding and points redemption as specified in the Dulux Painter's Club Loyalty Program, Participants must submit 1 copy of each Tax Invoice along with written Membership ID as proof of purchase to Akzo Nobel Paints (Malaysia) Sdn Bhd authorized representatives.
</li><li>Akzo Nobel Paints (Malaysia) Sdn Bhd assumes no responsibility for lost, unauthorised, misdirected, illegible, falsified, delayed or incomplete proof of purchases, all of which are invalid or void, or for difficulties experienced in submitting a proof of purchase, including as a result of any technical delay, failure or defect.
</li><li>Akzo Nobel Paints (Malaysia) Sdn Bhd reserves the right to vary the offers and the terms of the membership without prior notice.
</li><li>Akzo Nobel Paints (Malaysia) Sdn Bhd decision on all matters relating to Dulux Painter's Club Loyalty Program is final and no correspondence will be entertained</li>
<h4 class="text-bold">General</h4>
            <li>To the fullest extent permitted by law, the Promoter excludes: 
                <ul>
                    <li>all conditions, warranties and other terms which might otherwise be implied; and</li>
                    <li>any liability for any direct, indirect or consequential loss or damage incurred by any Participant in connection with this Loyalty Program.  This shall not be deemed to exclude or restrict liability for death or personal injury resulting from the negligence of Akzo Nobel Paints (Malaysia) Sdn Bhd, or its employees or agents.</li>
                </ul>
            </li>
            <li>Akzo Nobel Paints (Malaysia) Sdn Bhd is committed to protecting the privacy of Participants.  By participating in this Loyalty Program, Participants acknowledge and agree that certain personal data relating to them (including their name and geographical location) may be disclosed to third parties. Any information collected in connection with this Competition will only be used by the Akzo Nobel Paints (Malaysia) Sdn Bhd or their agents for the purposes of marketing or administering this Loyalty Program and future promotions or publicity material relating to this Loyalty Program.</li>
            <li>Akzo Nobel Paints (Malaysia) Sdn Bhd reserves the right to withdraw, delay or amend this Loyalty Program in whole or in part,  without prior notice or compensation, and will not be in breach of these terms or liable for any delay or failure in performing its obligations due to such circumstances
</li><li>If any court or competent authority finds that any provision of these terms and conditions (or part of any provision) is invalid, illegal or unenforceable, that provision or part-provision shall, to the extent required, be deemed to be deleted, and the validity and enforceability of the other provisions of these terms and conditions shall not be affected.
</li><li>These terms and conditions are governed by the laws of Malaysia and are subject to the exclusive jurisdiction of the Malaysia courts. 
</li><li>For all queries or issues with this competition, please contact: customercare.my@akzonobel.com or call 1800 88 9338 (Mon-Fri, 9am-5pm)
</li>
            </ol>


        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div>

