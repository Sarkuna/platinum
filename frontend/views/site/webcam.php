<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Webcam';
$this->params['breadcrumbs'][] = $this->title;
?>
	

<script type="text/javascript" src="/themes/adminlte/webcamjs/webcam.js"></script>

                
<div class="bs-example">
    <!-- Button HTML (to Trigger Modal) -->
    <a href="#myModal" class="btn btn-lg btn-primary" data-toggle="modal">Launch Demo Modal</a>
    
    <!-- Modal HTML -->
    
</div>  


	
	<h1>WebcamJS Test Page</h1>
	<h3>Demonstrates all features at once</h3>
        <input id="mydata" type="text" name="mydata" value=""/>
	<div style="margin-top:5px; margin-bottom:20px;">Captures large 480x480 cropped image while displaying live 240x240 preview, flipped horizontally (mirrored), and allows preview before save.</div>
	<div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <input id="mydata" type="text" name="mydata" value=""/>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Confirmation</h4>
                </div>
                <div class="modal-body">
                    <div id="my_camera"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onClick="save_photo()">Save changes</button>
                </div>
            </div>
        </div>
    </div>
	<div id="my_photo_booth">
		<div id="my_camera1"></div>
		
		
		<!-- First, include the Webcam.js JavaScript Library -->

		
		<!-- Configure a few settings and attach camera -->
		<script language="JavaScript">
			Webcam.set({
				// live preview size
				width: 700,
				height: 420,
				
				// device capture size
				dest_width: 640,
				dest_height: 480,
				
				// final cropped size
				crop_width: 480,
				crop_height: 480,
				
				// format and quality
				image_format: 'jpeg',
				jpeg_quality: 90,
				
				// flip horizontal (mirror mode)
				flip_horiz: true
			});
			
			$('#myModal').on('show.bs.modal', function () {				
			  Webcam.attach( '#my_camera' );
			})
			
			$("#myModal").on("hidden.bs.modal", function () {
				Webcam.reset();
			});
		</script>
		
		<!-- A button for taking snaps -->

	</div>
	
	<div id="results" style="display:none">
		<!-- Your captured image will appear here... -->
	</div>
	
	<!-- Code to handle taking the snapshot and displaying it locally -->
	<script language="JavaScript">
		// preload shutter audio clip
		var shutter = new Audio();
		shutter.autoplay = false;
		shutter.src = navigator.userAgent.match(/Firefox/) ? 'shutter.ogg' : 'shutter.mp3';
		

		
		function save_photo() {
			// actually snap photo (from preview freeze) and display it
			Webcam.snap( function(data_uri) {
				//try { shutter.currentTime = 0; } catch(e) {;} // fails in IE
			//shutter.play();
                        

                            var raw_image_data = data_uri.replace(/^data\:image\/\w+\;base64\,/, '');
                            document.getElementById('mydata').value = raw_image_data;
                            
                            var username = raw_image_data;
                                
                                $.ajax({
                                    url: '<?php echo \Yii::$app->getUrlManager()->createUrl('/site/mycam') ?>',
                                    type: 'POST',
                                     data: { username: username },
                                     success: function(data) {
                                         alert(data);

                                     }
                                 });


			// freeze camera so user can preview current frame
			Webcam.freeze();
				document.getElementById('results').innerHTML = 
					'<h2>Here is your large, cropped image:</h2>' + 
					'<img src="'+data_uri+'"/><br/></br>' + 
					'<a href="'+data_uri+'" target="_blank">Open image in new window...</a>';
				
				// shut down camera, stop capturing
				//Webcam.reset();
				
				// show results, hide photo booth
				document.getElementById('results').style.display = '';
				document.getElementById('my_photo_booth').style.display = 'none';
				
			} );
			$('#myModal').modal('toggle');
				Webcam.reset();
		}
	</script>