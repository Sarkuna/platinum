<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', "My Profile"); 
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-12">
        <section class="content-header">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header">	
                        <i class="fa fa-street-view"></i> <?= Html::encode($this->title) ?>
                        <div class="pull-right">

                        </div>
                    </h2>
                </div><!-- /.col -->
            </div>
        </section>

        <section class="content edusec-user-profile">
            <div class="col-lg-4 table-responsive edusec-pf-border no-padding" style="margin-bottom:15px">
                <div class="col-md-12 text-center">
                    <img class="img-circle edusec-img-disp" src="/upload/profile/icon-user-default.jpg" alt="No Image">		<div class="photo-edit">
                        <a class="photo-edit-icon" href="/edusec/index.php?r=student%2Fstu-master%2Fstu-photo&amp;sid=15" title="Change Profile Picture" data-toggle="modal" data-target="#photoup"><i class="fa fa-pencil"></i></a>
                    </div>
                </div>
                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <th>Full Name</th>
                            <td><?= Html::encode($model->profile_full_name) ?></td>
                        </tr>
                        <tr>
                            <th>Nick Name</th>
                            <td><?= Html::encode($model->profile_nickname) ?></td>
                        </tr>
                        <tr>
                            <th>Email ID</th>
                            <td><?= $user->email ?></td>
                        </tr>
                        <tr>
                            <th>Mobile No</th>
                            <td><?= $model->profile_mobile ?></td>
                        </tr>
                        <tr>
                            <th>Status</th>
                            <td>
                                <?php
                                if($user->status == 'P'){
                                    echo '<span class="label label-warning">Pending</span>';
                                }else if($user->status == 'D'){
                                    echo '<span class="label label-danger">Deleted</span>';
                                }else if($user->status == 'A'){
                                    echo '<span class="label label-success">Active</span>';
                                }
                                ?>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="col-lg-8 profile-data">
                <ul class="nav nav-tabs responsive" id = "profileTab">
                    <li class="active" id = "personal-tab"><a href="#personal" data-toggle="tab"><i class="fa fa-street-view"></i> <?php echo Yii::t('app', 'Personal Details'); ?></a></li>
                </ul>
                <div id='content' class="tab-content responsive">
                    <div class="tab-pane active" id="personal">
                        <?= $this->render('_tab_personal', ['model' => $model, 'user' => $user]) ?>
                    </div>
                </div>
            </div>
        </section>        
    </div>
</div>