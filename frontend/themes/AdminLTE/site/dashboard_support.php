<?php
use yii\helpers\Html;
use yii\helpers\Url;

use app\components\PainterOverviewWidget;
use app\components\ProductOverviewWidget;
use app\components\RedemptionOverviewWidget;
use app\components\TopCountBoxWidget;
?>
<section class="content">
    <div class="callout callout-info msg-of-day">
        <h4><i class="fa fa-bullhorn"></i> <?php echo Yii::t('app', 'Message of day box') ?></h4>
        <p><marquee onmouseout="this.setAttribute('scrollamount', 6, 0);" onmouseover="this.setAttribute('scrollamount', 0, 0);" scrollamount="6" behavior="scroll" direction="left"><?= Yii::$app->params['message.marque'] ?></marquee></p>
    </div>
    

    <div class="row">
        <div class="col-sm-6 col-xs-12">
            <?= PainterOverviewWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
        </div>
        <div class="col-sm-6 col-xs-12">
            <?= ProductOverviewWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
        </div>
    </div>
</section>
