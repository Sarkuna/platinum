<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LoginAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'themes/adminlte/bootstrap/css/bootstrap.min.css',
        'themes/adminlte/plugins/font-awesome/css/font-awesome.min.css',
        //'//code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css',
        'themes/adminlte/dist/css/AdminLTE2.css',
        //'themes/adminlte/dist/css/skins/_all-skins.min.css',
        //'themes/adminlte/plugins/iCheck/flat/blue.css',
        'themes/adminlte/plugins/iCheck/all.css',
        'themes/adminlte/css/login_custom.css',
    ];
    /*public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];*/    
    public $js = [
        'themes/adminlte/bootstrap/js/bootstrap.min.js',
        'themes/adminlte/plugins/iCheck/icheck.min.js',
    ];
    public $depends = [
        //'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
	//'yii\bootstrap\BootstrapPluginAsset',
    ];
}
