<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class DataTableAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'themes/adminlte/bootstrap/css/bootstrap.min.css',
        'themes/adminlte/plugins/font-awesome/css/font-awesome.min.css',
        //'//code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css',
        'themes/adminlte/plugins/datatables/dataTables.bootstrap.css',
        'themes/adminlte/dist/css/AdminLTE.min.css',
        'themes/adminlte/dist/css/skins/_all-skins.min.css',
        //'themes/adminlte/plugins/iCheck/flat/blue.css',
        //'themes/adminlte/plugins/iCheck/all.css',
        'themes/adminlte/css/custom.css',
        'themes/adminlte/css/jquery.fancybox.css',
        //'themes/adminlte/css/miedskin.css',
    ];
    public $js = [
        //'themes/adminlte/bootstrap/js/bootstrap.min.js',        
        'themes/adminlte/dist/js/jquery-ui.min.js',
        'themes/adminlte/dist/js/bootstrap-select.min.js',
        //'//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js',
        //'https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js',
        //'themes/adminlte/plugins/sparkline/jquery.sparkline.min.js',
        //'themes/adminlte/plugins/slimScroll/jquery.slimscroll.min.js',
        //'themes/adminlte/plugins/fastclick/fastclick.min.js',
        //'themes/adminlte/plugins/iCheck/icheck.min.js',
        
        'themes/adminlte/js/responsive-tabs.js',
        'themes/adminlte/js/AdminLTEOptions.js',
        //'themes/adminlte/js/auto.js',
        'themes/adminlte/plugins/slimScroll/jquery.slimscroll.min.js', 
        'themes/adminlte/dist/js/app.min.js',
        'themes/adminlte/js/jquery.fancybox.js',
        'themes/adminlte/js/EduSecUserProfile.js',
        'themes/adminlte/plugins/datatables/jquery.dataTables.min.js',
        'themes/adminlte/plugins/datatables/dataTables.bootstrap.min.js',
        //'themes/adminlte/dist/js/pages/dashboard.js',
        //'themes/adminlte/js/custom.js',                
    ];
    public $jsOptions = [
    	//'position' => \yii\web\View::POS_HEAD
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}