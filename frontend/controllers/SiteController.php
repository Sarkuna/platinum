<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Json;

use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'request-password-reset', 'reset-password','about','webcam','mycam','contactus','termsandconditions','test'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'changepassword', 'login', 'terms', 'viewapplication', 'viewapplication2', 'change-password','contactus','termsandconditions'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    /*public function actionIndex()
    {
        return $this->render('index');
    }*/
    
    public function actionIndex() {

        $session = Yii::$app->session;

        if (!Yii::$app->user->isGuest) {

            //if (($session['currentPersona'] == "owner") || ($session['currentPersona'] == "tenant")) {
            if (($session['currentRole'] == Yii::$app->params['role.type.administrator'])) {
                return $this->render('dashboard_administrator');
            } else if (($session['currentRole'] == Yii::$app->params['role.type.management'])) {
                return $this->render('dashboard_management');
            } else if (($session['currentRole'] == Yii::$app->params['role.type.support'])) {
                return $this->render('dashboard_support');
            } else if (($session['currentRole'] == Yii::$app->params['role.type.painter'])) {
                return $this->render('dashboard_painter');
            }
        } else {
            return $this->redirect(['home']);
        }
        //return $this->render('index');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        $this->layout = 'loginlayout';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            Yii::$app->ici->addtolog('success', Yii::$app->user->id);
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionContactus()
    {
        //echo 'test';
        return $this->render('contactus');
    }
    
    public function actionTermsandconditions()
    {
        //echo 'test';
        return $this->render('termsandconditions');
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('test');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    /*public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }*/

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $this->layout = 'loginlayout';
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        $this->layout = 'loginlayout';
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
    
    public function actionWebcam(){
        //echo 'test';
        $img = '<img src="/upload/documents/camera757.jpg" class="user-image" alt="User Image">';
        return $this->render('webcam', [
                //'model' => $model,
                'img' => $img,
            ]);
    }
    
    public function actionTest(){
        echo 'test live test 12334567';
        $message = "hishihan";
        $message = html_entity_decode($message, ENT_QUOTES, 'utf-8'); 
        $message = urlencode($message);
        //$destination = $myuser->cust_mobile;
        //$new = substr($myuser->mobile, 0, -3) . 'xxx';
        //echo $short.'<br>';
        $mobile = "60111889597";

                    
        $username = urlencode(Yii::$app->params['sms.username']);
        $password = urlencode(Yii::$app->params['sms.password']);
        $sender_id = urlencode("66300");
        $type = '1';
        
        $link = "https://www.isms.com.my/isms_send_all.php";
        $link .= "?un=$username&pwd=$password&dstno=$mobile&msg=$message&type=$type&sendid=$sender_id";
        
 
        //$result = Yii::$app->residenz->ismscURL($fp);
        $http = curl_init($link);
        curl_setopt($http, CURLOPT_RETURNTRANSFER, TRUE);
        $http_result = curl_exec($http);
        $http_status = curl_getinfo($http, CURLINFO_HTTP_CODE);
        curl_close($http);

        return $http_result;
        die();
    }
    public function actionMycam(){
        //echo 'test';
        $encoded_data = $_POST['username'];        
        $image_data = base64_decode( $encoded_data );
        $path = Yii::getAlias('@frontend') .'/web/upload/documents/';
        $file_name = $path;
        $image_name = substr(uniqid(rand(1,6)), 0, 8).'.jpg';
        $file = $file_name . $image_name;

        $success = file_put_contents($file, $image_data);
        
        if ($success) {
            //echo "Successfully uploaded"; 
            $response['image_name'] = $image_name;
        } else {
            //echo "Not uploaded";
            $response = null;
        }
        echo json_encode($response);
    }
    
    
}