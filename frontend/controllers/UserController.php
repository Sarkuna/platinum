<?php

namespace frontend\controllers;

use Yii;
use common\models\User;
use common\models\UserSearch;
use common\models\ProfileUser;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionChange()
    {
	$model=$this->findModel(Yii::$app->user->id);
	$model->scenario = 'change';

	if(isset($_POST['User']))
	{
		$model->attributes = $_POST['User'];
		$user = User::findOne(Yii::$app->user->id);
                $user->setPassword($model->new_pass);
                $user->auth_key = Yii::$app->security->generateRandomString();
		//$model->password_hash = md5($model->new_pass.$model->new_pass);
		if($user->save()){
                    //Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');                    
                    \Yii::$app->getSession()->setFlash('success',['title' => 'Change Password', 'text' => 'Your password has been changed successfully']);
                    return $this->goHome();
                    //return $this->redirect(['/site/index']);
                }else{
                    print_r($user->getErrors());
                }
	}

	return $this->render('password_changeform',[
		'model'=>$model,
	]);
     }
     
     public function actionProfile(){
        $model=$this->findProfile(Yii::$app->user->id);
        $userid = $model->user_id;
        $user = \common\models\User::find()->where(['id' => $userid])->one();
        return $this->render('profile_user',[
            'model' => $model,
            'user' => $user,
	]);
     }
     
     public function actionProfileupdate($id){
        $model=$this->findProfile($id);
        $userid = $model->user_id;
        $user = \common\models\User::find()->where(['id' => $userid])->one();
        return $this->render('update_profile',[
            'model' => $model,
            'user' => $user,
	]);
     }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function findProfile($id)
    {
        $id = '1';
        if (($model = ProfileUser::findOne(['profile_user_id' => $id, 'user_id' => Yii::$app->user->id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
