<?php
namespace app\components;

use Yii;
use yii\base\Widget;
//use yii\helpers\Html;

class AlertWidget extends Widget
{
    public $path;

    public function init()
    {
        parent::init();
    }

    public function run()
    {        
        return $this->render('alertwidget');
        
    }
}