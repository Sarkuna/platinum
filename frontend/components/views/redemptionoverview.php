<?php
    $session = Yii::$app->session;

    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\grid\GridView;
?>

<?php
$copunt = 0;
$redemptionoverviews = $dataProvider->getModels();
//echo '<pre>';
//print_r($announcements);
//die();
$count = count($redemptionoverviews);

?>

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-mail-forward"></i>Redemption Overview</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="table-responsive">
            <table class="table no-margin">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Redemption #</th>
                        <th>Painter Name</th>
                        <th>RM Value</th>
                        <th>Status</th>                        
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if ($count > 0) {
                        foreach ($redemptionoverviews as $redemptionoverview) {
                            $ahrf = '<a href="/management/redemption/invoiceapprove?id='.$redemptionoverview->redemptionID.'"><span class="label label-warning">'.$redemptionoverview->orderStatus->name.'</span></a>';
                            echo '<tr>';
                            echo '<td width="90">' . date('d-m-Y', strtotime($redemptionoverview->redemption_created_datetime)) . '</td>';
                            echo '<td>' . $redemptionoverview->redemption_invoice_no . '</td>';
                            echo '<td>' . $redemptionoverview->profile->full_name . '</td>';
                            echo '<td class="text-right">' . $redemptionoverview->req_amount . '</td>';
                            echo '<td>' . $ahrf . '</td>';
                            echo '</tr>';
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div><!-- /.table-responsive -->
    </div><!-- /.box-body -->
    <div class="box-footer clearfix">
        <a href="/management/redemption" class="btn btn-sm btn-default btn-flat pull-right">View All</a>
    </div><!-- /.box-footer -->
</div><!-- /.box -->