<?php

// define "stack_top_left' stack

\raoul2000\widget\pnotify\PNotify::registerStack(
        [
    'stack_top_left' => [
        'dir1' => 'right',
        'dir2' => 'right',
        'push' => 'top'
    ]
        ], $this
);

// display a notification using the "stack_top_left" stack.
// Note that you must use yii\web\JsExpression for the "stack" plugin option value. 
//$counter = 0;                    
foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
    //$counter++;
    \raoul2000\widget\pnotify\PNotify::widget([
        'pluginOptions' => [
            'title' => $message['title'],
            'text' => $message['text'],
            'type' => $message['type'],
            //'icon' => 'glyphicon glyphicon-envelope',
            'stack' => new yii\web\JsExpression('stack_top_left'),
            'addclass' => 'stack-topleft',
        /* 'desktop' => [
          'desktop' => true
          ],
          'buttons' => [
          'closer_hover' => false
          ] */
        ]
    ]);
}
?> 