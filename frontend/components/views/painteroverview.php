<?php
    $session = Yii::$app->session;

    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\grid\GridView;
?>

<?php
$copunt = 0;
$painteroverviews = $dataProvider->getModels();
//echo '<pre>';
//print_r($announcements);
//die();
$count = count($painteroverviews);

?>

<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-calendar-o"></i> Painter Registration Overview</h3>
    </div>
    <div class="box-body" id="holidayList">
        <div class="table-responsive">
            <table class="table no-margin">
                <tbody><tr>
                        <th>Submitted Date</th>
                        <th>Membership #</th>
                        <th>Painter Name</th>
                        <th>Status</th>
                    </tr>
                    <?php
                    if ($count > 0) {
                        foreach ($painteroverviews as $painteroverview) {
                            echo '<tr>';
                            echo '<td width="100">' . date('d-m-Y', strtotime($painteroverview->created_datetime)) . '</td>';
                            echo '<td width="100">' . $painteroverview->card_id . '</td>';
                            echo '<td>' . $painteroverview->full_name . '</td>';
                            echo '<td>';
                            if ($painteroverview->profile_status == 'P') {
                                echo '<a href="/painter/painterprofile/viewapprove?id='.$painteroverview->id.'"><span class="label label-warning">Pending</span></a>';
                            } else if ($painteroverview->profile_status == 'R') {
                                echo '<a href="javascript:;" onclick="viewremark(' . $painteroverview->id . ');return false;"><span class="label label-info">Review</span></a>';
                            } else if ($painteroverview->profile_status == 'D') {
                                echo '<span class="label label-danger">Decline</span>';
                            } else if ($painteroverview->profile_status == 'A') {
                                echo '<span class="label label-success">Active</span>';
                            }
                            echo '</td>';
                            echo '</tr>';
                        }
                    } else {
                        echo '<tr><td colspan="3" align="center">' . Yii::$app->params['nomsg'] . '</td></tr>';
                    }
                    ?>

                </tbody></table>
        </div><!---/. end-responsive-div--->  
    </div><!---/. box-body--->
    <div class="box-footer clearfix">
        <a href="/painter/painterprofile/" class="btn btn-sm btn-default btn-flat pull-right">View All</a>
    </div>
</div><!---/. box---> 