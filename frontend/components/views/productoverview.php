<?php
    $session = Yii::$app->session;

    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\grid\GridView;
?>

<?php
$copunt = 0;
$productoverviews = $dataProvider->getModels();
//echo '<pre>';
//print_r($announcements);
//die();
$count = count($productoverviews);

?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-rub"></i>Painter Transactions Overview</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="table-responsive">
            <table class="table no-margin">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Transaction #</th>
                        <th>Painter Name</th>
                        <th>Points</th>
                        <th>Status</th>                        
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if ($count > 0) {
                        foreach ($productoverviews as $productoverview) {
                            $ahrf = '<a href="management/pointorder/view?id='.$productoverview->order_id.'"><span class="label label-warning">'.$productoverview->orderStatus->name.'</span></a>';
                            echo '<tr>';
                            echo '<td width="100">' . date('d-m-Y', strtotime($productoverview->created_datetime)) . '</td>';
                            echo '<td>' . $productoverview->order_num . '</td>';
                            echo '<td>' . $productoverview->profile->full_name . '</td>';
                            echo '<td class="text-right">' . $productoverview->order_total_point . '</td>';
                            echo '<td>' . $ahrf . '</td>';
                            echo '</tr>';
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div><!-- /.table-responsive -->
    </div><!-- /.box-body -->
    <div class="box-footer clearfix">
        <a href="/management/pointorder/findpainter" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a>
        <a href="/management/pointorder/index" class="btn btn-sm btn-default btn-flat pull-right">View All</a>
    </div><!-- /.box-footer -->
</div><!-- /.box -->